using Drafts.Database;

namespace @namespace {

	public partial class @class : DatabaseBase<@class> {
		@access
	}

@enums

}