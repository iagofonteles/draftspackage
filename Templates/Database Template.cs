using System;
using System.Collections.Generic;
using UnityEngine;
using Drafts;
using Drafts.Database;
using System.Linq;

namespace @namespace {

	public partial class @class : DatabaseBase<@class> {
		public static @class DB => Instance;
@getters

#if UNITY_EDITOR
		[SerializeField, Button] bool _GenerateAssets;
#endif
	}
}