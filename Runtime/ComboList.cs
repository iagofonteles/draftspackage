﻿using System;
using System.Collections.Generic;

namespace Drafts.Battle {

	public class ComboList<P, R> : List<ComboList<P,R>.Pattern> {

		public struct Pattern {
			/// <summary>Pattern name for easier management (optional).</summary>
			public string name;
			/// <summary>Return this value when match.</summary>
			public R result { get; private set; }
			/// <summary>Sequence to match.</summary>
			public P[] pattern { get; private set; }

			public Pattern(string name, R result, params P[] pattern) {
				this.name = name;
				this.result = result;
				this.pattern = pattern;
			}

			/// <summary>Check if the pattern matches end resturn the resulting power id. The [current_pattern] must start with the combo pattern.</summary>
			public bool SimpleMatch(List<P> current_pattern, out R result) {
				result = default;
				if (pattern.Length > current_pattern.Count) return false;
				for (int i = 0; i < pattern.Length; i++)
					if (!pattern[i].Equals(current_pattern[i])) return false;
				result = this.result;
				return true;
			}

			/// <summary>Check if the pattern matches end resturn the resulting power id. can match at any position of [current_pattern] not only from the beginning.</summary>
			[Obsolete("Not really obsolete. just not implemented yet")]public bool Match(P[] current_pattern, out R result) {
				throw new NotImplementedException();
			}
		}

		/// <summary>Check if the pattern matches end resturn the resulting power id. The [current_pattern] must start with the combo pattern.</summary>
		public bool SimpleMatch(List<P> current_pattern, out R result) {
			result = default;
			for (int i = 0; i < Count; i++)
				if (this[i].SimpleMatch(current_pattern, out result))
					return true;
			return false;
		}
		/// <summary></summary>
		public void Add(string name, R result, params P[] pattern) => Add(new Pattern(name, result, pattern));
		/// <summary></summary>
		public int Remove(string name) => RemoveAll(p => p.name == name);
	}
}
