﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Drafts.IndexedProperties;
using UnityEngine;
using System.Linq;

namespace Drafts
{
    /// <summary>Shotcut for PlayerPrefs class. Also allow to separate preferences for each user.<br />
    /// Use Prefs.I/S/B/F [key] or [player_id, key], for int,string,bool,float values, respectively.</summary>
    public static class Prefs {
        public static bool Once(string setting) => Once(userId, setting);
        public static bool Once(int user_id, string setting) {
            if (!B[user_id, setting]) {
                B[user_id, setting] = true;
                return true;
            }
            return false;
        }

        /// <summary>Use this if you dont want to share the saved settings between users</summary>
        public static int userId = -1;

        /// <summary>Get/Set int values.</summary>
        public static _GetSetBoth<string, int, string, int> I = new _GetSetBoth<string, int, string, int>(
            (s) => PlayerPrefs.GetInt(s + userId), (s, v) => PlayerPrefs.SetInt(s + userId, v),
            (i, s) => PlayerPrefs.GetInt(s + i), (i, s, v) => PlayerPrefs.SetInt(s + i, v));

        /// <summary>Get/Set string values.</summary>
        public static _GetSetBoth<string, int, string, string> S = new _GetSetBoth<string, int, string, string>(
            (s) => PlayerPrefs.GetString(s + userId), (s, v) => PlayerPrefs.SetString(s + userId, v),
            (i, s) => PlayerPrefs.GetString(s + i), (i, s, v) => PlayerPrefs.SetString(s + i, v));

        /// <summary>Get/Set bool values.</summary>
        public static _GetSetBoth<string, int, string, bool> B = new _GetSetBoth<string, int, string, bool>(
            (s) => PlayerPrefs.GetInt(s + userId) != 0, (s, v) => PlayerPrefs.SetInt(s + userId, v ? 1 : 0),
            (i, s) => PlayerPrefs.GetInt(s + i) != 0, (i, s, v) => PlayerPrefs.SetInt(s + i, v ? 1 : 0));

        /// <summary>Get/Set float values.</summary>
        public static _GetSetBoth<string, int, string, float> F = new _GetSetBoth<string, int, string, float>(
            (s) => PlayerPrefs.GetFloat(s + userId), (s, v) => PlayerPrefs.SetFloat(s + userId, v),
            (i, s) => PlayerPrefs.GetFloat(s + i), (i, s, v) => PlayerPrefs.SetFloat(s + i, v));

        /// <summary>Get/Set Integer values.</summary>
        public static bool Exists(string s) => PlayerPrefs.HasKey(s + userId);

        /// <summary>Get/Set Integer values.</summary>
        public static bool Exists(int id, string s) => PlayerPrefs.HasKey(s + id);
    }

    public static class Configs {
        static Dictionary<string, object> values = new Dictionary<string, object>();

#if UNITY_WEBGL
        static public _GetSet<string, int> I = new _GetSet<string, int>((s) => Prefs.I[-999, s], (s, v) =>  Prefs.I[-999, s] = v);
        static public _GetSet<string, string> S = new _GetSet<string, string>((s) => Prefs.S[-999, s], (s, v) => Prefs.S[-999, s] = v);
        static public _GetSet<string, float> F = new _GetSet<string, float>((s) => Prefs.F[-999, s], (s, v) => Prefs.F[-999, s] = v);
        static public _GetSet<string, bool> B = new _GetSet<string, bool>((s) => Prefs.B[-999, s], (s, v) => Prefs.B[-999, s] = v);
#else
        static public _GetSet<string, int> I = new _GetSet<string, int>((s) => values.TryGetValue(s, out var v) ? (int)v : 0, (s, v) => { modified = true; values[s] = v; });
        static public _GetSet<string, string> S = new _GetSet<string, string>((s) => values.TryGetValue(s, out var v) ? (string)v : null, (s, v) => { modified = true; values[s] = v; });
        static public _GetSet<string, float> F = new _GetSet<string, float>((s) => values.TryGetValue(s, out var v) ? (float)v : 0, (s, v) => { modified = true; values[s] = v; });
        static public _GetSet<string, bool> B = new _GetSet<string, bool>((s) => values.TryGetValue(s, out var v) ? (bool)v : false, (s, v) => { modified = true; values[s] = v; });
        static public _GetSet<string, DateTime> D = new _GetSet<string, DateTime>((s) => values.TryGetValue(s, out var v) ? (DateTime)v : default, (s, v) => { modified = true; values[s] = v; });
        static public _GetSet<string, object> O = new _GetSet<string, object>((s) => values.TryGetValue(s, out var v) ? v : null, (s, v) => { modified = true; values[s] = v; });
#endif

        static bool modified = true;


        public static void Save(string path = "myconfigs.conf") {
#if UNITY_WEBGL
            return;
#endif
            if (!modified) return;
            path = Application.persistentDataPath + "/" + path;
            var file = new FileStream(path, FileMode.Create);
            var bf = new BinaryFormatter();
            bf.Serialize(file, values.Keys.ToArray());
            bf.Serialize(file, values.Values.ToArray());
            
            file.Close();
            modified = false;
        }
        public static void Load(string path = "myconfigs.conf") {
#if UNITY_WEBGL
            return;
#endif
            path = Application.persistentDataPath + "/" + path;
            if (!File.Exists(path)) return;
            var file = new FileStream(path, FileMode.Open);
            var bf = new BinaryFormatter();

            values.Clear();
            if (file.Length > 0) {
                var k = (string[])bf.Deserialize(file);
                var v = (object[])bf.Deserialize(file);
                for (int i = 0; i < k.Length; i++) values.Add(k[i], v[i]);
            }
            file.Close();
            modified = true;
        }
        public static void Clear() { modified = true; values.Clear(); }
        /*
        [MenuItem("Tools/Configs Clear")]
        public static void EditorDeleteConfigs() {
            var path = Application.persistentDataPath + "/myconfigs.conf";
            if(File.Exists(path)) File.Delete(path);
        }*/
    }
}

namespace Drafts.IndexedProperties
{
    public readonly struct _GetSet<Index, Type>
    {
        readonly Func<Index, Type> Get; readonly Action<Index, Type> Set;
        public Type this[Index i] { get => Get(i); set => Set(i, value); }
        public _GetSet(Func<Index, Type> get, Action<Index, Type> set) { Get = get; Set = set; }
    }
    public readonly struct _GetSet2<I1, I2, Type>
    {
        readonly Func<I1, I2, Type> Get; readonly Action<I1, I2, Type> Set;
        public Type this[I1 i1, I2 i2] { get => Get(i1, i2); set => Set(i1, i2, value); }
        public _GetSet2(Func<I1, I2, Type> get, Action<I1, I2, Type> set) { Get = get; Set = set; }
    }
    public readonly struct _GetSetBoth<I1, I2, I3, Type>
    {
        readonly Func<I1, Type> Get; readonly Action<I1, Type> Set;
        readonly Func<I2, I3, Type> Get2; readonly Action<I2, I3, Type> Set2;
        public Type this[I1 i1] { get => Get(i1); set => Set(i1, value); }
        public Type this[I2 i2, I3 i3] { get => Get2(i2, i3); set => Set2(i2, i3, value); }
        public _GetSetBoth(Func<I1, Type> get, Action<I1, Type> set) { Get = get; Set = set; Get2 = null; Set2 = null; }
        public _GetSetBoth(Func<I2, I3, Type> get2, Action<I2, I3, Type> set2) { Get2 = get2; Set2 = set2; Get = null; Set = null; }
        public _GetSetBoth(Func<I1, Type> get, Action<I1, Type> set, Func<I2, I3, Type> get2 = null, Action<I2, I3, Type> set2 = null) { Get = get; Set = set; Get2 = get2; Set2 = set2; }
    }
}
