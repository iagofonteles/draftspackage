﻿using System.Text.RegularExpressions;

namespace Drafts {
	namespace RegexExtra {

		public static class MyRegex {
			public static bool OnlyLetters(this string s) => Regex.IsMatch(s, @"^[a-zA-Z]+$");
			public static bool OnlyNumbers(this string s) => Regex.IsMatch(s, @"^[0-9]+$");
			public static bool OnlyLettersAndNumbers(this string s) => Regex.IsMatch(s, @"^[a-zA-Z0-9]+$");
			public static bool OnlyLettersAndNumbersUnderscore(this string s) => Regex.IsMatch(s, @"^[a-zA-Z0-9_]+$");
		}
	}
}
