using Drafts.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

#if INPUT_SYSTEM_ENABLED
using UnityEngine.InputSystem;
#endif

public class MouseEventTrigger : MonoBehaviour {

	public enum Button { Left, Right, Middle }
	public enum Event { Press, Release }

	public Button _button = Button.Right;
	public Event _event = Event.Release;

	public UnityEvent OnTrigger;

	// Start is called before the first frame update
	void Awake() {
		var et = gameObject.GetOrAdd<EventTrigger>();
#if INPUT_SYSTEM_ENABLED
		var btn = new[] { Mouse.current.leftButton, Mouse.current.rightButton, Mouse.current.middleButton }[(int)_button];
		switch(_event) {
			case Event.Press: et.AddListener(EventTriggerType.PointerDown, e => { if(btn.isPressed) OnTrigger.Invoke(); }); break;
			case Event.Release: et.AddListener(EventTriggerType.PointerUp, e => { if(btn.wasReleasedThisFrame) OnTrigger.Invoke(); }); break;
		}
#else
		var btn = (int)_button;
		switch(_event) {
			case Event.Press: et.AddListener(EventTriggerType.PointerDown, e => { if(Input.GetMouseButton(btn)) OnTrigger.Invoke(); }); break;
			case Event.Release: et.AddListener(EventTriggerType.PointerUp, e => { if(Input.GetMouseButtonUp(btn)) OnTrigger.Invoke(); }); break;
		}
#endif
	}

}
