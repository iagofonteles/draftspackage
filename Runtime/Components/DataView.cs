﻿using Drafts.Extensions;
using Drafts.Patterns;
using UnityEngine;

namespace Drafts.Hidden {
	public abstract class DataView : MonoBehaviour { }
}

namespace Drafts.Components {

	/// <summary>Use this to sync a UI element with some data structure using events from the data structure.
	/// Can also be used to avoid destroiyng UI elements and instead</summary>
	/// <typeparam name="T">The data structure to sync with.</typeparam>
	public abstract class DataView<T> : Hidden.DataView {
		[Header("Data Instance")]
		[SerializeField] T data;

		public enum NullMode { None, DeactivateGameObject, DeactivateCanvasGroup, DestroyGameObject, CallClearMethod }
		[SerializeField] protected NullMode nullMode = NullMode.DeactivateGameObject;

		private void OnDestroy() { if(data != null) Unsubscribe(data); }

		public static implicit operator T(DataView<T> listener) => listener.Data;

		/// <summary>Call Subscribe on assign. Hide and Show canvas.</summary>
		public T Data {
			get => data;
			set {
				if(data != null) {
					if(data.Equals(value)) return;
					Unsubscribe(data);
				} else if(value == null) return;

				data = value;
				if(value != null) {
					Subscribe(value);
					Repaint();
				}

				if(!Application.isPlaying && nullMode != NullMode.CallClearMethod) return; // allow preview in editor then set data to null

				switch(nullMode) {
					case NullMode.DeactivateGameObject: gameObject.SetActive(value != null); break;
					case NullMode.DeactivateCanvasGroup: GetComponent<CanvasGroup>().SetActive(value != null); break;
					case NullMode.DestroyGameObject: if(value == null) Destroy(gameObject); break;
					case NullMode.CallClearMethod: if(value == null) Clear(); break;
				}
			}
		}

		/// <summary>Subscribe to Data events to sync UI.</summary>
		protected virtual void Subscribe(T data) { }
		/// <summary>Subscribe to Data events to sync UI.</summary>
		public void Subscribe(Hidden.DataView ui) => Data = (T)ui.GetType().GetProperty("Data").GetValue(ui);

		/// <summary>Unsubscribe to Data events and hide UI.</summary>
		protected virtual void Unsubscribe(T data) { }
		/// <summary>Unsubscribe to Data events and hide UI.</summary>
		public void Unsubscribe() => Data = default;

		/// <summary>Called when Data == null and nullMode set to CallClearMethod.</summary>
		protected virtual void Clear() { }
		/// <summary>Repaint UI, if you dont use callbacks, call this instead of assign the same data again.</summary>
		public virtual void Repaint() { }

		//private void OnValidate() { var d = data; Data = default; Data = d; }

		/// <summary>To be used with null mode destroy.</summary>
		public DataView<T> Duplicate(T data) {
			var ret = this.Duplicate();
			ret.Data = data;
			return ret;
		}

		/// <summary>To be used with null mode deactivate gameobject.</summary>
		public DataView<T> Recycle(T data) {
			var parent = transform.parent;

			for(int i = 1; i < parent.childCount; i++) {
				var slot = parent.GetChild(i).GetComponent<DataView<T>>();
				if(slot.Data == null) {
					slot.Data = data;
					slot.transform.SetSiblingIndex(parent.childCount);
					return slot;
				}
			}
			return Duplicate(data);
		}

		public void DestroyAllCopies() => transform.parent.DestroyChildren(1);
		public void TrashAllCopies() {
			for(int i = 1; i < transform.parent.childCount; i++)
				transform.parent.GetChild(i).GetComponent<DataView<T>>().Data = default;
		}
	}
}
