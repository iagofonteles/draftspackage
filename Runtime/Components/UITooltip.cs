using Drafts.Patterns;
using System;
using UnityEngine;
using UObj = UnityEngine.Object;

#if DRAFTS_USE_TMPRO
using TMPro;
#else
using UnityEngine.UI;
#endif

namespace Drafts.Components {
	
	public class UITooltip : MonoBehaviour {
		public static UITooltipCanvas Instance => UITooltipCanvas.Instance;

		public string text;
		public UObj data;
		public RectTransform target;

		public void ShowThis() { if(data) Show(data); else Show(text); }
		public static void Show(object data) => UITooltipCanvas.Show(data);
		public static void Clear() => UITooltipCanvas.Clear();
	}

	public class UITooltipCanvas : LazyMonoSingleton<UITooltipCanvas> {

#if DRAFTS_USE_TMPRO
		[SerializeField] TextMeshProUGUI text = default;
#else
		[SerializeField] Text text = default;
#endif
		[SerializeField] DataView<object> dataUI = default;
		[SerializeField] TextAnchor anchor = default;
		
		bool followMouse;
		Func<Vector2> targetPosition;

		void Update() => transform.position = followMouse ? GetMousePosition() : targetPosition();

		public static void Clear() => Instance.gameObject.SetActive(false);

		public static void Show(UObj data) => Show(data as object);
		public static void Show(object data) {
			Instance.gameObject.SetActive(true);
			if(Instance.dataUI) Instance.dataUI.Data = data;
			else Instance.text.text = data as string;
		}

		Func<Vector2> GetReposition() {
			var rect = GetComponent<RectTransform>();
			switch(anchor) {
				case TextAnchor.UpperLeft:
					break;
				case TextAnchor.UpperCenter:
					break;
				case TextAnchor.UpperRight:
					break;
				case TextAnchor.MiddleLeft:
					break;
				case TextAnchor.MiddleCenter:
					break;
				case TextAnchor.MiddleRight:
					break;
				case TextAnchor.LowerLeft:
					break;
				case TextAnchor.LowerCenter:
					break;
				case TextAnchor.LowerRight:
					break;
				default:
					break;
			}
			return null;
		}

#if ENABLE_INPUT_SYSTEM
		Vector2 GetMousePosition() => UnityEngine.InputSystem.Mouse.current.position.ReadValue();
#else
		Vector2 GetMousePosition() => Input.mousePosition;
#endif
	}
}
