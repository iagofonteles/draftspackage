using Drafts.Extensions;
using Drafts.Patterns;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Drafts.Components {

	public class SelectableContainer : MonoMultiton<SelectableContainer> {

		[SerializeField] bool focusOnSelect = default;
		public bool rememberSelection;
		public DraftsEvent<bool> OnFocusChanged;
		public UnityEvent<bool> OnSelectedChanged;
		[SerializeField] bool focus;
		public GameObject LastSelected { get; set; }
		Button button;

		// Start is called before the first frame update
		void Start() {
			gameObject.GetOrAdd<EventTrigger>().AddListener(EventTriggerType.Select, _ => OnSelect());
			gameObject.GetOrAdd<EventTrigger>().AddListener(EventTriggerType.Select, _ => OnSelectedChanged.Invoke(true));
			gameObject.GetOrAdd<EventTrigger>().AddListener(EventTriggerType.Deselect, _ => OnSelectedChanged.Invoke(false));
			button = gameObject.GetOrAdd<Button>();
			OnFocusChanged.AddListener(f => button.enabled = !f);
		}

		public bool Focus {
			get => focus;
			set {
				if(value == true) {
					var obj = rememberSelection && LastSelected ? LastSelected : GetComponentInChildren<Selectable>().gameObject;
					Debug.Log(obj, obj);
					if(obj) EventSystem.current.SetSelectedGameObject(obj);
					focus = obj;
				} else {
					focus = false;
				}
				OnFocusChanged.Invoke(focus);
			}
		}

		void OnSelect() {
			foreach(var c in Instances) Focus = false;
			if(focusOnSelect) Focus = true;
		}
	}

}