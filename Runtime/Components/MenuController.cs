﻿using UnityEngine;
using System.Collections.Generic;

namespace Drafts {

	/// <summary>
	/// Used to activate and deactivate Transforms, mostly used for UI Menus.
	/// After Open() a page, you can use Back() to open the previously page, if any.
	/// Use OnEnable and OnDisable to update info while closing/opening pages or implement IPage interface.
	/// Use BackTo() to dig the trace for a specific page.
	/// You can use refer to the static instance if there is only one controller in the scene (it's not a singleton, though).
	/// Note: Sound volume is controlled by the Audio.VolumeSFX.
	/// </summary>
	public class MenuController : MonoBehaviour {
		public static MenuController instance;
		
		public Transform startPage;
		public AudioClip pageSound;
		Transform currentPage;
		readonly Stack<Transform> backTrace = new Stack<Transform>();
		
#pragma warning disable IDE0051 // Remover membros privados não utilizados
		void Awake() {
#pragma warning restore IDE0051 // Remover membros privados não utilizados
			instance = this;
			if(startPage)
				Open(startPage, false);
		}

		void OpenDontTrace(Transform page, bool sound) {
			if(sound && pageSound)
				AudioSource.PlayClipAtPoint(pageSound, transform.position, Audio.VolumeSFX);

			//currentPage?.GetComponent<IPage>()?.Close();
			currentPage?.gameObject.SetActive(false);
			currentPage = page;
			currentPage.gameObject.SetActive(true);
			//currentPage.GetComponent<IPage>()?.Open();
		}

		/// <summary>Open the child with given name.</summary>
		public void Open(string name) => Open(name, true);
		/// <summary>Open the child with given name.</summary>
		public void Open(Transform page) => Open(page, true);
		/// <summary>Open the child with given name.</summary>
		public void Open(string name, bool sound) {
			foreach(Transform t in transform)
				if(t.name == name) { Open(t, sound); break; }
		}
		/// <summary>Open specific transform.</summary>
		public void Open(Transform page, bool sound) {
			if(currentPage && currentPage != page) backTrace.Push(currentPage);
			OpenDontTrace(page, sound);
		}

		/// <summary>Back to previous menu.</summary>
		public void Back(bool sound = true) {
			if(backTrace.Count > 0)
				OpenDontTrace(backTrace.Pop(), sound);
		}
		/// <summary>Back to previous menu, erasing back trace until find the given page.</summary>
		public void BackTo(Transform page) => BackTo(page, true);
		/// <summary>Back to previous menu, erasing back trace until find the given page.</summary>
		public void BackTo(string page) => BackTo(page, true);
		/// <summary>Back to previous menu, erasing back trace until find the given page. Can be used to reset the trace when no such page exists.</summary>
		public void BackTo(Transform page, bool sound) {
			while(backTrace.Count > 0) {
				var t = backTrace.Pop();
				if(t == page) {
					OpenDontTrace(t, sound);
					break;
				}
			}
		}
		/// <summary>Back to previous menu, erasing back trace until find the given page. Can be used to reset the trace when no such page exists.</summary>
		public void BackTo(string page, bool sound) {
			while(backTrace.Count > 0) {
				var t = backTrace.Pop();
				if(t.name == page) {
					OpenDontTrace(t, sound);
					break;
				}
			}
		}

		// <summary>In case you need the Open() and Close() functions for the page.</summary>
		//public interface IPage { void Open(); void Close(); }
	}

	

}