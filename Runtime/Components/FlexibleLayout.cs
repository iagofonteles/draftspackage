using Drafts.Extensions;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Drafts.Components {

	public class ScreenListener : MonoBehaviour {

		/// <summary>Gives the new screen size.</summary>
		public event Action<Vector2Int> OnResize;

		public static void AddListener(Action<Vector2Int> callback) => Instance.OnResize += callback;
		public static void RemoveListener(Action<Vector2Int> callback) => Instance.OnResize -= callback;

		static ScreenListener instance;
		public static ScreenListener Instance => instance
			?? (instance = new GameObject("Screen Resize Listener").AddComponent<ScreenListener>());

		static ScreenListener() => SceneManager.sceneLoaded += (a, b) =>
			Instance.OnResize(new Vector2Int(Screen.width, Screen.height));

		Vector2 previousSize = Vector2.zero;

		void Update() {
			if(previousSize.x != Screen.width || previousSize.y != Screen.height) {
				var newSize = new Vector2Int(Screen.width, Screen.height);
				OnResize?.Invoke(newSize);
				previousSize = newSize;
			}
		}
	}

	[ExecuteAlways, RequireComponent(typeof(RectTransform), typeof(CanvasGroup))]
	public class FlexibleLayout : MonoBehaviour {
		public enum Mode { Both, Portrait, Landscape }
		public Mode ShowIn;

		public RectTransform portraitPosition;
		public RectTransform landscapePosition;

#if UNITY_EDITOR
		private void Update() => Check();
		private void OnValidate() => Check();
#endif
		void Start() { if(Application.isPlaying) ScreenListener.Instance.OnResize += Check; }
		void OnDestroy() { if(Application.isPlaying) ScreenListener.Instance.OnResize -= Check; }

		void Check(Vector2Int screenSize) => Check();
		void Check() {
			var portrait = Screen.width < Screen.height;
			var tgt = portrait ? portraitPosition : landscapePosition;
			if(tgt) ChangeTo(tgt);
			GetComponent<CanvasGroup>().SetActive(ShowIn == Mode.Both
				|| (ShowIn == Mode.Portrait && portrait)
				|| (ShowIn == Mode.Landscape && !portrait));

		}

		void ChangeTo(RectTransform target) {
			if(target.parent != transform.parent) {
				transform.SetParent(target.parent);
				transform.SetSiblingIndex(target.GetSiblingIndex());
			}
			GetComponent<RectTransform>().CopyPosition(target);
		}
	}
}
