﻿using UnityEngine;
using Drafts;
using UnityEngine.Events;

[AddComponentMenu("Drafts/Components/Triggers/On Trigger")]
public class TriggerOnTrigger : MonoBehaviour {
	public string targetTag;
	public bool use2D;

	[Conditional("use2D", true)]
	public UnityEvent<Collider> onTriggerEnter, onTriggerStay, onTriggerExit;
	[Conditional("use2D", false)]
	public UnityEvent<Collider2D> onTriggerEnter2D, onTriggerStay2D, onTriggerExit2D;

	private void OnTriggerEnter(Collider col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerEnter.Invoke(col); }
	private void OnTriggerStay(Collider col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerStay.Invoke(col); }
	private void OnTriggerExit(Collider col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerExit.Invoke(col); }

	private void OnTriggerEnter2D(Collider2D col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerEnter2D.Invoke(col); }
	private void OnTriggerStay2D(Collider2D col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerStay2D.Invoke(col); }
	private void OnTriggerExit2D(Collider2D col) { if(targetTag == "" || col.CompareTag(targetTag)) onTriggerExit2D.Invoke(col); }
}