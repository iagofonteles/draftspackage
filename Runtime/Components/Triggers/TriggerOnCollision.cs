﻿using UnityEngine;
using UnityEngine.Events;

namespace Drafts.Components {
	[AddComponentMenu("Drafts/Components/Triggers/On Collision")]
	public class TriggerOnCollision : MonoBehaviour {
		public string targetTag;
		public bool use2D;

		[Conditional("use2D", true)]
		public UnityEvent<Collision> onCollisionEnter, onCollisionStay, onCollisionExit;
		[Conditional("use2D", false)]
		public UnityEvent<Collision2D> onCollisionEnter2D, onCollisionStay2D, onCollisionExit2D;

		private void OnCollisionEnter(Collision col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionEnter.Invoke(col); }
		private void OnCollisionStay(Collision col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionStay.Invoke(col); }
		private void OnCollisionExit(Collision col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionExit.Invoke(col); }

		private void OnCollisionEnter2D(Collision2D col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionEnter2D.Invoke(col); }
		private void OnCollisionStay2D(Collision2D col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionStay2D.Invoke(col); }
		private void OnCollisionExit2D(Collision2D col) { if(targetTag == "" || col.collider.CompareTag(targetTag)) onCollisionExit2D.Invoke(col); }
	}

}