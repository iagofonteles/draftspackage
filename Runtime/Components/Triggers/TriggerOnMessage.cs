﻿using UnityEngine;
using UnityEngine.Events;

namespace Drafts.Components {
	[AddComponentMenu("Drafts/Components/Triggers/On Message")]
	public class TriggerOnMessage : MonoBehaviour {
		public UnityEvent onStart;
		public UnityEvent onAwake;
		public UnityEvent onDestroy;
		public UnityEvent onEnable;
		public UnityEvent onDisable;

		[Tooltip("When childCount == 0")]
		public UnityEvent onTransformEmpty;
		public UnityEvent<int> onChildrenChanged;

		private void Awake() => onAwake.Invoke();
		private void Start() => onStart.Invoke();
		private void OnDestroy() => onDestroy.Invoke();
		private void OnEnable() => onEnable.Invoke();
		private void OnDisable() => onDisable.Invoke();
		
		private void OnTransformChildrenChanged() {
			if(transform.childCount == 0) onTransformEmpty.Invoke();
			onChildrenChanged.Invoke(transform.childCount);
		}
	}
}
