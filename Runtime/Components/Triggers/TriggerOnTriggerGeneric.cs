﻿using UnityEngine;
using UnityEngine.Events;
using Drafts;

public class TriggerOnTrigger<T> : MonoBehaviour {
	public string targetTag;
	public bool use2D;

	[Conditional("use2D", true)]
	public UnityEvent<T> onTriggerEnter, onTriggerStay, onTriggerExit;
	[Conditional("use2D", false)]
	public UnityEvent<T> onTriggerEnter2D, onTriggerStay2D, onTriggerExit2D;

	private void OnTriggerEnter(Collider col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerEnter.Invoke(c); }
	private void OnTriggerStay(Collider col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerStay.Invoke(c); }
	private void OnTriggerExit(Collider col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerExit.Invoke(c); }

	private void OnTriggerEnter2D(Collider2D col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerEnter2D.Invoke(c); }
	private void OnTriggerStay2D(Collider2D col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerStay2D.Invoke(c); }
	private void OnTriggerExit2D(Collider2D col) { if((targetTag == "" || col.CompareTag(targetTag)) && col.TryGetComponent<T>(out var c)) onTriggerExit2D.Invoke(c); }
}