﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Drafts.Components {
	[ExecuteInEditMode]
	public class Anima_SortingGroup : AnimaModule {
		public SortingGroup target;
		public int depthA, depthB;
		override public void Anim(float f) => target.sortingOrder = (int)Mathf.Lerp(depthA, depthB, f);
	}
}