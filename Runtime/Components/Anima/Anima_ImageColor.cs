﻿using UnityEngine;
using UnityEngine.UI;

namespace Drafts.Components {
	[ExecuteInEditMode]
	public class Anima_ImageColor : AnimaModule {
		public Image target;
		public Color colorA, colorB;
		override public void Anim(float f) => target.color = Color.Lerp(colorA, colorB, f);
	}
}