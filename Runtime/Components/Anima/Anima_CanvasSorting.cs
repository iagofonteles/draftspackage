﻿using UnityEngine;

namespace Drafts.Components {
	[ExecuteInEditMode]
	public class Anima_CanvasSorting : AnimaModule {
		public Canvas target;
		public int depthA, depthB;
		override public void Anim(float f) => target.sortingOrder = (int)Mathf.Lerp(depthA, depthB, f);
	}
}