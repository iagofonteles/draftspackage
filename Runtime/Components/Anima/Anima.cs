﻿using Drafts.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Drafts.Components {

	[ExecuteInEditMode]
	public class Anima : MonoBehaviour {

		[Header("Animation Parameters")]
		public float duration = .5f;
		public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);
		public bool loop = false;
		public bool playOnStart = false;

		internal protected virtual void Funcao() { }

		public bool isPlaying = false;
		[SerializeField] float time = 0;
		/// <summary>Current animation time.</summary>
		public float Time { get => time; private set => time = value; }
		/// <summary>Normalized time of the animation.</summary>
		public float Progress => Time / duration;
		/// <summary>Evaluation from the animation curve.</summary>
		public float Evaluate => curve.Evaluate(Progress);

		float step = 0;
		bool EndReached => Time >= duration || Time <= 0;

		/// <summary>Set false to play backwards.</summary>
		public void Play(bool forward = true) { isPlaying = true; step = forward ? 1 : -1; }
		public void Stop() => isPlaying = false;
		public void Continue() => isPlaying = true;
		public void Reset() => Time = step < 0 ? duration : 0;

		protected virtual void Start() {
#if UNITY_EDITOR
			if(Application.isPlaying)
#endif
			if(playOnStart) Play();
			OnUpdate.Invoke(Evaluate);
		}
		protected virtual void Update() {
			if(isPlaying) {
				Time += step * UnityEngine.Time.deltaTime;
				if(EndReached) // animation end
					if(loop) Reset();
					else {
						Time = Mathf.Clamp(Time, 0, duration);
						isPlaying = false;
					}
				OnUpdate.Invoke(Evaluate);
			}
		}

		[System.Serializable]
		public class UpdateEvent : UnityEvent<float> { }
		public UpdateEvent OnUpdate;
		//public event Action<float> OnStart;
		//public event Action<float> OnEnd;

		[SerializeField, Button] bool _ePlay; void ePlay() => Play(true);
		[SerializeField, Button] bool _ePlayR; void ePlayR() => Play(false);
	}

	public abstract class AnimaModule : MonoBehaviour {
		protected Anima anima;
		private void Start() {
			anima = GetComponent<Anima>();
			if(anima) anima.OnUpdate.AddListener(Anim);
		}
		public void Anim() => Anim(anima.Evaluate);
		public abstract void Anim(float f);
	}
}