﻿using UnityEngine;

namespace Drafts.Components {
	[ExecuteInEditMode]
	public class Anima_FillRect : AnimaModule {
		public RectTransform target, rectA, rectB;
		override public void Anim(float f) => MyMath.Lerp(target, rectA, rectB, f);
	}
}