﻿using Drafts.Extensions;
using UnityEngine;

namespace Drafts.Components {
	[ExecuteInEditMode]
	public class Anima_CanvasGroup : AnimaModule {
		public CanvasGroup target;

		public bool changeAlpha;
		public float alphaA, alphaB;

		public bool changeInteraction, reverseInteraction;
		public bool changeRayblock, reverseRayblock;

		override public void Anim(float f) {
			target.alpha = Mathf.Lerp(alphaA, alphaB, f);
			if(changeInteraction)
				target.interactable = Mathf.RoundToInt(f).ToBool() ^ reverseRayblock;
			if(changeRayblock)
				target.blocksRaycasts = Mathf.RoundToInt(f).ToBool() ^ reverseRayblock;
		}
	}
}