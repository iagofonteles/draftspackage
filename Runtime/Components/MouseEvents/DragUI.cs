using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Drafts.Components {

	[RequireComponent(typeof(CanvasGroup))]
	public class DragUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

		[SerializeField] bool returnToPosition = true;
		[SerializeField] bool useDragAlpha = default;
		[Range(0, 1), Conditional("useDragAlpha")] public float dragAlpha = .75f;

		public UnityEvent OnDragBegin;
		public UnityEvent<Vector2> OnDragUpdate;
		public UnityEvent<Vector2> OnDragEnd;

		CanvasGroup group;

		float originalAlpha;
		Vector2 originalPosition;
		Vector2 mouseOffset;

		private void Start() {
			group = GetComponent<CanvasGroup>();
		}

		public void OnEndDrag(PointerEventData eventData) {
			var delta = (Vector2)transform.position - originalPosition;
			if(returnToPosition) transform.position = originalPosition;
			group.alpha = originalAlpha;
			group.blocksRaycasts = true;
			OnDragEnd.Invoke(delta);
		}

		public void OnBeginDrag(PointerEventData eventData) {
			originalPosition = transform.position;
			originalAlpha = group.alpha;
			if(useDragAlpha) group.alpha = dragAlpha;
			group.blocksRaycasts = false;

			mouseOffset = originalPosition - eventData.position;
			OnDragBegin.Invoke();
		}

		public void OnDrag(PointerEventData eventData) {
			transform.position = eventData.position + mouseOffset;
			OnDragUpdate.Invoke(eventData.delta);
		}

	}
}
