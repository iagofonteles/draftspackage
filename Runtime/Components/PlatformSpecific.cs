﻿using UnityEngine;

#pragma warning disable IDE0044 // readonly
#pragma warning disable IDE0051 // Remover membros privados não utilizados

namespace Drafts {
	[System.Flags]
	public enum BuildPlatform {
		PC = 1, MAC = 2, Android = 4, IOS = 8, WebGL = 16,
		Mobile = Android | IOS, Standalone = PC | MAC
	}

	public static class PlatformSpecificExtensionMethod {
		public static bool Evaluate(this BuildPlatform flags) {
			var match = false;
#if UNITY_STANDALONE_WIN
			match = match || (flags & BuildPlatform.PC) > 0;
#endif
#if UNITY_STANDALONE_OSX
			match = match || (flags & BuildPlatform.MAC) > 0;
#endif
#if UNITY_ANDROID
			match = match || (flags & BuildPlatform.Android) > 0;
#endif
#if UNITY_IOS
			match = match || (flags & BuildPlatform.IOS) > 0;
#endif
#if UNITY_WEBGL
			match = match || (flags & BuildPlatform.WebGL) > 0;
#endif
			return match;
		}
	}
}

namespace Drafts.Components {
	[DisallowMultipleComponent, AddComponentMenu("MyEngine/Platform Specific")]
	class PlatformSpecific : MonoBehaviour {

		[SerializeField] BuildPlatform platforms = default;
		[Tooltip("Deactivate on the specified platform?")]
		[SerializeField] bool deactivate = false;

		void Start() {
			var match = platforms.Evaluate();
			gameObject.SetActive(match ^ deactivate);
		}
	}
}
