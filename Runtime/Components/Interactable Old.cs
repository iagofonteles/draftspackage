﻿/*
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MyEngine.Attributes;

namespace MyEngine {
	[Serializable]
	public class Interactable {
		public static Sprite defaultIcon;
		public static float defaultRange = 1;
		static HashSet<Interactable> list = new HashSet<Interactable>();
		[Serializable] public class EventObj : UnityEvent<object> { }

		/// <summary>Optional name.</summary>
		public string name = "";
		/// <summary>Will use this transform.position to determine distance from the actorz.</summary>
		public Transform transform = null;
		///<summary>Only used when transform is null.</summary>
		[Conditional("transform", true)]
		public Vector2 fixed_position = Vector2.zero;
		[Space]
		public Sprite icon = null;
		[Conditional("icon")]
		public Vector2 iconOffset = Vector2.zero;
		public float range = 0;
		public bool automatic = false;
		public bool disableOnTrigger = false;
		[SerializeField] EventObj action = null;
		[HideInInspector] public GameObject gameObject = null;

		public Vector2 Position => transform?.position ?? fixed_position;

		#region Constructors
		public Interactable(Transform follow, EventObj action, float? range = null) { transform = follow; this.action = action; this.range = range ?? defaultRange; }
		public Interactable(Vector2 position, EventObj action, float? range = null) { fixed_position = position; this.action = action; this.range = range ?? defaultRange; }
		public Interactable(Transform transform = null, Vector2? position = null, float? range = null, bool automatic = false,
			Sprite icon = null, Vector2? iconOffset = null, string name = "", EventObj action = null) {
			this.name = name;
			this.transform = transform;
			fixed_position = position ?? Vector2.zero;
			this.icon = icon;
			this.iconOffset = iconOffset ?? Vector2.zero;
			this.range = range ?? defaultRange;
			this.automatic = automatic;
			this.action = action;
		}
		#endregion
		/// <summary>Returns false if the instance is already on the list.</summary>
		public static bool Add(Interactable instance) => list.Add(instance);
		/// <summary>Returns false if the instance was not on the list.</summary>
		public static bool Remove(Interactable instance) => list.Remove(instance);

		static object lastActive;

		/// <summary>Search for interactables in range and execute the all automatic ones. Only the first non-auto is triggered if [trigger] is true</summary>
		/// <param name="fromPoint">Current actor position to check for nearby interactables.</param>
		/// <param name="rangeAdd">If your character is too large, you may add aditional reach to it.</param>
		/// <param name="trigger">Wheter the trigger button was pressed. Will trigger interactable event.</param>
		/// <param name="icon">Needs a SpriteRenderer or Image component to show the icon on top of the interactable.</param>
		/// <param name="parameter">Optional callback parameter.</param>
		/// <returns></returns>
		public static Interactable GetClosest<T> (Vector2 fromPoint, float rangeAdd, bool trigger, T icon, Action<Interactable, T> OnDrawIcon, object parameter = null) where T : Component {
			var minDistance = float.MaxValue;
			Interactable ret = null;
			foreach (var i in list) {
				var distance = Vector2.Distance(i.Position, fromPoint);
				var inRange = distance <= i.range + rangeAdd;
				if (!inRange) continue;
				if (i.automatic) i.Action(parameter);
				else
					if (distance < minDistance) {
					minDistance = distance;
					ret = i;
				}
			}
			var a = lastActive as T;

			// draw icon
			OnDrawIcon(ret, icon);

			if (trigger) ret?.Action(parameter);
			return ret;
		}

		public static void DrawIconDefault(Interactable i, GameObject go) {
			if (go) {
				if (i != null) {
					if (go.TryGetComponent<SpriteRenderer>(out var spr)) spr.sprite = i.icon;
					if (go.TryGetComponent<UnityEngine.UI.Image>(out var img)) img.sprite = i.icon;
					go.transform.position = i.Position + i.iconOffset;
					Debug.Log("interactable icon position: " + i.Position + i.iconOffset);
				}
				go.SetActive(i != null);
			}
		}

		public void Action(object parameter = null) {
			action?.Invoke(parameter);
			if (disableOnTrigger) Remove(this);
		}

		/// <summary>Draw the icon at the offset position.</summary>
		public void DrawIcon(SpriteRenderer renderer) {
			renderer.enabled = true;
			renderer.sprite = icon;
			renderer.transform.position = Position + iconOffset;
		}
	}

	/// <summary>Add this component to scene objects that needs to interact with the player</summary>
	public class InteractableObject : MonoBehaviour {
		public enum PositionMode { Self, Other, Fixed };
		public PositionMode position;
		public Interactable instance = new Interactable();

#pragma warning disable IDE0051 // Remover membros privados não utilizados
		void Start() { Interactable.Add(instance); instance.gameObject = gameObject; }
		void OnDestroy() => Interactable.Remove(instance);
#pragma warning restore IDE0051 // Remover membros privados não utilizados
		public void OnValidate() => instance.transform = position == PositionMode.Fixed
			? null : position == PositionMode.Self ? transform : instance.transform;

		void OnDrawGizmos() {
			if (!instance.icon) return;
			var spr = instance.icon;
			var pos = (Vector2)transform.position + instance.iconOffset;
			var size = new Vector2(spr.rect.width / spr.pixelsPerUnit, spr.rect.height / spr.pixelsPerUnit);
			var screenRect = new Rect(pos.x - size.x / 2, pos.y + size.y / 2, size.x, -size.y);
			var r = new Rect(spr.rect.x / spr.texture.width, spr.rect.y / spr.texture.height,
				spr.rect.width / spr.texture.width, spr.rect.height / spr.texture.height);
			Graphics.DrawTexture(screenRect, spr.texture, r, 0, 0, 0, 0);
		}
	}
}
*/