﻿using Drafts.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Drafts.Components {
	class SelectableExtras : MonoBehaviour {

		static Navigation NavNone = new Navigation() { mode = Navigation.Mode.None };
		static Navigation NavAuto = new Navigation() { mode = Navigation.Mode.Automatic };

		[SerializeField] bool selectOnEnable = false;
		[Tooltip("The previously selected object is stored on enable and re-selected on disable.")]
		[SerializeField, Conditional("selectOnEnable")] bool restoreOnDisable = true;
		[SerializeField] bool selectOnHover = true;
		[SerializeField] SelectableContainer container = default;
		[SerializeField] UnityEvent<bool> OnSelectedChanged = default;

		GameObject previous;

		private void Start() {
			var selectable = gameObject.GetOrAdd<Selectable>();
			var canvasGroup = gameObject.GetOrAdd<CanvasGroup>();
			var et = gameObject.GetOrAdd<EventTrigger>();
			et.AddListener(EventTriggerType.Select, _ => OnSelectedChanged.Invoke(true));
			et.AddListener(EventTriggerType.Deselect, _ => OnSelectedChanged.Invoke(false));
			et.AddListener(EventTriggerType.PointerEnter, _ => { if(canvasGroup.interactable && selectOnHover) Select(); });

			if(container) container.OnFocusChanged.AddListener(f => selectable.navigation = f ? NavAuto : NavNone);
		}

		private void OnEnable() { if(selectOnEnable) Select(); }

		private void OnDisable() {
			if(selectOnEnable && restoreOnDisable && previous)
				EventSystem.current.SetSelectedGameObject(previous);
		}

		public void Select() {
			previous = EventSystem.current.currentSelectedGameObject;
			EventSystem.current.SetSelectedGameObject(gameObject);
		}

	}
}