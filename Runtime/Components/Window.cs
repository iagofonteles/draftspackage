﻿using Drafts.Patterns;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Drafts.Components {

	public interface IWindow {
		/// <summary>Retorna o estado atual da janela.</summary>
		bool IsOpen { get; }
		/// <summary>Retorna true se a janela estava fechada e abriu.</summary>
		bool OpenWindow();
		/// <summary>Retorna true se a janela estava aberta e fechou.</summary>
		bool CloseWindow();
		/// <summary>Retorna true se for pra tocar o som de abrir.</summary>
		bool ToggleWindow();
		/// <summary>To be used in inspector.</summary>
		void SetActive(bool active);
	}

	public abstract class Window : MonoMultiton<Window>, IWindow {
		[SerializeField] GameObject selectOnStart = default;
		/// <summary>Retorna o estado atual da janela.</summary>
		public virtual bool IsOpen => gameObject.activeInHierarchy;
		/// <summary>Retorna true se a janela estava fechada e abriu.</summary>
		public virtual bool OpenWindow() { if(IsOpen) return false; gameObject.SetActive(true); return true; }
		/// <summary>Retorna true se a janela estava aberta e fechou.</summary>
		public virtual bool CloseWindow() { if(!IsOpen) return false; gameObject.SetActive(false); return true; }
		/// <summary>Retorna true se for pra tocar o som de abrir.</summary>
		public virtual bool ToggleWindow() => IsOpen ? !CloseWindow() : OpenWindow();
		/// <summary>To be used in inspector.</summary>
		public virtual void SetActive(bool active) { if(active) CloseWindow(); else OpenWindow(); }

		protected virtual void Start() { if(selectOnStart) EventSystem.current.SetSelectedGameObject(selectOnStart); }
	}
}