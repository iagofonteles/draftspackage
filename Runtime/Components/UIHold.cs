﻿using Drafts.Extensions;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Drafts.Components {
	class UIHold : MonoBehaviour {
		public float delay = default;
		public UnityAction OnHold = default;

		public bool IsHolding { get; private set; }

		EventTrigger trigger;
		Timer timer;

		[SerializeField] bool test = default;
		void Update() {
			test = IsHolding;
			timer.Update();
			if(timer.Time == 0) OnHold = null;
		}

		[SuppressMessage("", "IDE0051")]
		void Start() {
			trigger = gameObject.GetOrAdd<EventTrigger>();
			trigger.AddListener(EventTriggerType.PointerDown, d => { IsHolding = true; timer.Reset(); });
			trigger.AddListener(EventTriggerType.PointerUp, d => IsHolding = false);
			trigger.AddListener(EventTriggerType.PointerExit, d => IsHolding = false);
		}

	}
}
