﻿using UnityEngine;
using UnityEngine.Events;
using Drafts.Extensions;

namespace Drafts {
	class InteractableWithIcon : Interactable {
		public SpriteRenderer icon;
		[Conditional("icon")]
		public Vector2 iconOffset = Vector2.zero;

		new UnityEvent<bool> onRangeStateChange;
		InteractableWithIcon() => base.onRangeStateChange.AddListener(b=>icon.enabled = b);

		void OnDrawGizmosSelected() {
			if (!icon) return;
			var spr = icon.sprite;
			var pos = (Vector2)transform.position + iconOffset;
			var size = new Vector2(spr.rect.width / spr.pixelsPerUnit, spr.rect.height / spr.pixelsPerUnit);
			var screenRect = new Rect(pos.x - size.x / 2, pos.y + size.y / 2, size.x, -size.y);
			var r = new Rect(spr.rect.x / spr.texture.width, spr.rect.y / spr.texture.height,
				spr.rect.width / spr.texture.width, spr.rect.height / spr.texture.height);
			Graphics.DrawTexture(screenRect, spr.texture, r, 0, 0, 0, 0);
		}
	}

	class InteractableOutlineShader : Interactable {
		new public Renderer renderer = default;
		new UnityEvent<bool> onRangeStateChange = default;
		InteractableOutlineShader() => base.onRangeStateChange.AddListener(b => renderer.material.SetInt("outline", b.ToInt()));
	}
}
