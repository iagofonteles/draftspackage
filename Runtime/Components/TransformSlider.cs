using Drafts.Patterns;
using UnityEngine;

namespace Drafts {

	[ExecuteInEditMode]
	public class TransformSlider : MonoBehaviour {

		public Transform Min, Max;
		public DraftsEvent<float> OnValueChanged;

		[SerializeField, Range(0, 1)] float value;
		public float Value { get => value; set { this.value = Mathf.Clamp01(value); OnValidate(); } }
		public void Add(float value) => Value += value;
		private void Start() => OnValidate();

		private void OnValidate() => transform.position = Vector3.Lerp(Min.position, Max.position, Value);

	}
}
