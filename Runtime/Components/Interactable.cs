﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Drafts {
	[Serializable]
	public class Interactable : MonoBehaviour {
		static HashSet<Interactable> list = new HashSet<Interactable>();
		[Serializable] public class EventObj : UnityEvent<GameObject> { }

		public float range = 0;
		public bool automatic = false;

		public enum AfterTrigger { None, Disable, Destroy }
		public AfterTrigger afterTrigger;

		[SerializeField] EventObj action = default;
		public UnityEvent<bool> onRangeStateChange;

#pragma warning disable IDE0051 // Remover membros privados não utilizados
		void OnEnable() => Add(this);
		void OnDisable() => Remove(this);
#pragma warning restore IDE0051 // Remover membros privados não utilizados

		public void Action(GameObject parameter = null) {
			action?.Invoke(parameter);
			if (afterTrigger == AfterTrigger.Disable) enabled = false;
			if (afterTrigger == AfterTrigger.Destroy) Destroy(gameObject);
		}

		/// <summary>Returns false if the instance is already on the list.</summary>
		public static bool Add(Interactable instance) => list.Add(instance);
		/// <summary>Returns false if the instance was not on the list.</summary>
		public static bool Remove(Interactable instance) => list.Remove(instance);

		static Interactable lastActive;

		/// <summary>Search for interactables in range and execute the all automatic ones. Only the first non-auto is triggered if [trigger] is true</summary>
		/// <param name="from">Current actor position to check for nearby interactables.</param>
		/// <param name="rangeAdd">If your character is too large, you may add aditional reach to it.</param>
		/// <param name="trigger">Wheter the trigger button was pressed. Will trigger interactable event.</param>
		/// <param name="parameter">Optional callback parameter.</param>
		/// <returns>The instance found.</returns>
		public static Interactable GetClosest(Vector3 from, float rangeAdd = 0, bool trigger = false, GameObject parameter = null) {
			var minDistance = float.MaxValue;
			Interactable ret = null;
			foreach (var i in list) {
				var distance = Vector2.Distance(i.transform.position, from);
				var inRange = distance <= i.range + rangeAdd;
				if (!inRange) continue;
				if (i.automatic) i.Action(parameter);
				else
					if (distance < minDistance) {
					minDistance = distance;
					ret = i;
				}
			}

			// in range state changed
			if (lastActive != ret) {
				lastActive?.onRangeStateChange?.Invoke(false);
				ret?.onRangeStateChange?.Invoke(true);
				lastActive = ret;
			}

			if (trigger) ret?.Action(parameter);
			return ret;
		}
	}
}
