using UnityEngine;
using UnityEngine.Events;

namespace Drafts.Components {

	public class CallCondition : MonoBehaviour {
		[SerializeField] UnityEvent callback = default;
		public void CallIf(Component component) { if(component) callback.Invoke(); }
		public void CallIfNot(Component component) { if(!component) callback.Invoke(); }
		public void CallIf(bool call) { if(call) callback.Invoke(); }
		public void CallIfNot(bool notCall) { if(!notCall) callback.Invoke(); }
		public void CallAfter(float time) => Invoke(nameof(_Call), time);
		void _Call() => callback.Invoke();
	}
}
