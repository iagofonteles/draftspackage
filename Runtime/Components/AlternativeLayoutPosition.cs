﻿using Drafts.Extensions;
using Drafts.Patterns;
using System;
using UnityEngine;

namespace Drafts.Components {

	/// <summary>If you want you UI to support both Portrait an Landscape, this will help.</summary>
	[DisallowMultipleComponent, RequireComponent(typeof(RectTransform)), ExecuteInEditMode]
	public class AlternativeLayoutPosition : MonoMultiton<AlternativeLayoutPosition> {

		static int current = 0;
		public static int Layout {
			get => current;
			set {
				if(current == value) return;
				current = value;
				OnLayoutChange?.Invoke(value);
			}
		}
		public static void ForceUpdate() => OnLayoutChange.Invoke(current);
		public virtual string[] Names => new string[0];
		public static Action<int> OnLayoutChange;

		[SerializeField] protected RectTransform[] positions;
		RectTransform rectTransform;
		protected void UpdateLayout(int layout) => ChangeTo(positions[layout]);

		private void Start() {
			rectTransform = GetComponent<RectTransform>();
			OnLayoutChange += UpdateLayout;
			UpdateLayout(Layout);
		}
		protected override void OnDestroy() {
			base.OnDestroy();
			OnLayoutChange -= UpdateLayout;
		}

#if UNITY_EDITOR
		private void Update() => UpdateLayout(Layout);
#endif

		void ChangeTo(RectTransform target) {
			if(!target) Debug.Log("target missing", this);
			if(!rectTransform) Debug.Log("missing", rectTransform);

			if(!target) return;
			if(target.parent != transform.parent) {
				transform.SetParent(target.parent);
				transform.SetSiblingIndex(target.GetSiblingIndex());
			}
			rectTransform.CopyPosition(target);
		}
	}
}
