﻿using Drafts.Extensions;
using Drafts.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.Mathf;

#pragma warning disable IDE0051
#pragma warning disable IDE0044
namespace Drafts.Components {

	[ExecuteInEditMode]
	public class UIWindow : MonoMultiton<UIWindow> {
		private static UIWindow _current = null;
		/// <summary>Current active Window</summary>
		public static UIWindow Current {
			get => _current; private set {
				if(_current && _current.rememberLastElement && EventSystem.current)
					_current.firstElement = EventSystem.current.currentSelectedGameObject;
				_current = value;
			}
		}
		private static int _currentLayer = 0;
		/// <summary></summary>
		public static int CurrentLayer {
			get => _currentLayer; private set {
				if(_currentLayer == value) return;

				// change opacity of other layers
				SetLayerOpacity(false);

				// save/load last window on this layer
				if(value > _currentLayer) {
					layerLastWindow.Push(Current);
					Current = null;
					_currentLayer = value;

				} else {
					_currentLayer = value;
					layerLastWindow.Pop().Open();
				}

				// restore opacity
				SetLayerOpacity(true);
			}
		}
		private static Stack<UIWindow> layerLastWindow = new Stack<UIWindow>();
		private static IEnumerable<UIWindow> GetElegibleWindows() => Instances.Where(w => w.isActiveAndEnabled && w.layer == CurrentLayer);

		#region Instance
		[SerializeField] bool rememberLastElement = true;
		[SerializeField] GameObject firstElement = default;
		[SerializeField] bool openOnStart = false;

		[SerializeField] Text titleText = default;
		/// <summary>Window Title text.</summary>
		public string title;
		[SerializeField] int layer;
		CanvasGroup canvasG;

		private void OnValidate() {
			titleText.TrySetText(title);
			if(openOnStart) {
				FindObjectsOfType<UIWindow>().ForEach(u => u.openOnStart = false);
				openOnStart = true;
			}
			CanvasOpacity(true);
			layer = 0;
		}

		private void Start() { if(openOnStart) Open(); canvasG = GetComponent<CanvasGroup>(); }
		protected override void OnDestroy() { base.OnDestroy(); if(Current == this) Current = null; }
		private void OnDisable() { CanvasOpacity(false); if(Current == this) Current = null; }
		private void OnEnable() { CanvasOpacity(true); }
		private void CanvasOpacity(bool opaque) { if(canvasG) canvasG.alpha = opaque ? 1 : .6f; }

		/// <summary>Enable and set as the current active window.</summary>
		public void Open(bool newLayer = false) {
			layer = CurrentLayer + newLayer.ToInt();
			if(newLayer) CurrentLayer++;

			gameObject.SetActive(true);
			enabled = true;
			Current = this;
			EventSystem.current.SetSelectedGameObject(firstElement);
		}

		public void Toggle() {
			if(isActiveAndEnabled) Close();
			else Open();
		}

		/// <summary>Can be used in the inspector.</summary>
		public void Close_() => Close();

		/// <summary>Go back one layer if this was the last window in current layer.
		/// If this is the last window, it doesnt close and returns true.</summary>
		public bool Close() {
			if(CurrentLayer == 0)
				return true;
			gameObject.SetActive(false);

			var e = GetElegibleWindows();
			var isLast = e.Count() == 0;
			if(isLast) CurrentLayer--;
			else e.First().Open();
			return false;
		}

		[SerializeField] bool ForcedClose() => Close();
		#endregion

		#region Static Methods

		/// <summary>Go to the next enabled and active window. Return false if the is no elegible window.</summary>
		public static bool FocusNext(bool reverse = false) {
			var e = GetElegibleWindows().OrderBy(w => w.transform.position.x).ToList();
			var id = e.IndexOf(Current) + (reverse ? -1 : 1);
			id = (int)Repeat(id, e.Count());

			var next = e.ElementAtOrDefault(id);
			if(!next) return false;
			next.Open();
			return true;
		}

		/// <summary>Return true if this is layer 0.</summary>
		public static bool CloseCurrent() => Current.Close();
		/// <summary>Return true if this is layer 0.</summary>
		public static bool CloseLayer() {
			if(CurrentLayer == 0) return true;
			CurrentLayer--;
			return false;
		}

		static void SetLayerOpacity(bool opaque) => GetElegibleWindows().ForEach(w => w.CanvasOpacity(opaque));

		#endregion

		#region Editor Utility
		[ContextMenu("Sequential Loop")]
		void _Sequential() {
			var selectables = transform.GetComponentsInChildren<Selectable>();
			Sequential(selectables.Where(s => s.interactable).ToArray(), true);
		}

		[ContextMenu("Restrain Omnidirectional")]
		void _Omnidirectional() {
			var selectables = transform.GetComponentsInChildren<Selectable>();
			Omnidirectional(selectables.Where(s => s.interactable));
		}

		enum Orientation { right, left, down, up };
		static Orientation GetOrientation(Vector2 direction) =>
			Abs(direction.x) > Abs(direction.y) // horizontal
				? (direction.x > 0 ? Orientation.right : Orientation.left)
				: (direction.y > 0 ? Orientation.up : Orientation.down);

		/// <summary>Works well for grid layouts. Do not loops</summary>
		public static void Omnidirectional(IEnumerable<Selectable> selectables) {
			foreach(var s in selectables) {
				var nearests = selectables.Select(ss => (obj: ss, vector: ss.transform.position - s.transform.position))
					.OrderBy(ss => ss.vector.magnitude).Skip(1).Take(4);

				s.navigation = new Navigation() {
					mode = Navigation.Mode.Explicit,
					selectOnLeft = nearests.FirstOrDefault(n => Abs(n.vector.x) > Abs(n.vector.y) && n.vector.x < 0).obj,
					selectOnRight = nearests.FirstOrDefault(n => Abs(n.vector.x) > Abs(n.vector.y) && n.vector.x > 0).obj,
					selectOnDown = nearests.FirstOrDefault(n => Abs(n.vector.x) < Abs(n.vector.y) && n.vector.y < 0).obj,
					selectOnUp = nearests.FirstOrDefault(n => Abs(n.vector.x) < Abs(n.vector.y) && n.vector.y > 0).obj,
				};

				var selectOnDown = nearests.FirstOrDefault(n => n.vector.x < n.vector.y && n.vector.y < 0).obj;
			}
		}

		/// <summary>Works well for vertical/horizontal layouts. Loops at start and end.</summary>
		public static void Sequential(Selectable[] selectables, bool loop) {
			if(selectables.Length < 2) return;
			var orientation = GetOrientation(selectables[1].transform.position - selectables[0].transform.position);
			Func<Selectable, Selectable, Navigation> GetNav = null;
			switch(orientation) {
				case Orientation.right: GetNav = (next, back) => new Navigation { selectOnRight = next, selectOnLeft = back }; break;
				case Orientation.left: GetNav = (next, back) => new Navigation { selectOnRight = back, selectOnLeft = next }; break;
				case Orientation.up: GetNav = (next, back) => new Navigation { selectOnUp = next, selectOnDown = back }; break;
				case Orientation.down: GetNav = (next, back) => new Navigation { selectOnUp = back, selectOnDown = next }; break;
			}

			if(selectables.Length > 3)
				for(int i = 1; i < selectables.Length - 1; i++)
					selectables[i].navigation = GetNav(selectables[i - 1], selectables[i + 1]);

			var l = selectables.Length - 1; // set first and last
			selectables[0].navigation = GetNav(loop ? selectables[l] : null, selectables[1]);
			selectables[l].navigation = GetNav(selectables[l - 1], loop ? selectables[0] : null);
		}
		#endregion
	}
}
