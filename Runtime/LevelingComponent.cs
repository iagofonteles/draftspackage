using Drafts.Patterns;
using System;
using UnityEngine;

namespace Drafts {

	/// <summary>If using on the inspector, call Start() or else it will not level up.</summary>
	[System.Serializable]
	public class Leveling {

		Func<int, int> requiredXp;
		public IntWatcher maxLevel = new IntWatcher();
		public IntDeltaWatcher level = new IntDeltaWatcher();
		public FloatDeltaWatcher experience = new FloatDeltaWatcher();

		/// <param name="requiredXp">Receives a level and returns the xp need to reach that level.</param>
		public Leveling(int maxLevel, Func<int, int> requiredXp) {
			this.maxLevel.Value = maxLevel;
			this.requiredXp = requiredXp;
			Start();
		}
		public void Start() => experience.OnChanged += OnExperienceChange;

		void OnExperienceChange(float v, float d) {
			var newLvl = 0;
			while(v >= requiredXp(newLvl)) newLvl++;
			level.Value = newLvl;
		}

		/// <summary>Set experience and level to 0.</summary>
		public void Reset() { experience.Value = 0; }
		public void SetLevel(int level) => experience.Value = requiredXp(level);
		public float RequiredXp(int level) => requiredXp(level);

		public string RequiredXpString {
			get {
				var r = "";
				for(int i = 0; i < maxLevel; i++)
					r += $"Level {i}: {requiredXp(i)}\n";
				return r;
			}
		}

#if UNITY_EDITOR
		[SerializeField, Button] bool _AddExperience;
		void AddExperience(int value) => experience.Value += value;
#endif
	}

	namespace Components {
		public class LevelingComponent : MonoBehaviour {
			[SerializeField] Leveling leveling = default;
			public Leveling Leveling => leveling;
			private void Start() => leveling.Start();
			public void AddExperience(int amount) => leveling.experience += amount;
			public void SetExperience(int amount) => leveling.experience += amount;
			public static implicit operator Leveling(LevelingComponent l) => l.leveling;

#if UNITY_EDITOR
			[SerializeField, Button] bool _AddExperience;
			void TestGiveExperience(int value) => leveling.experience.Value += value;
#endif
		}
	}

	public static class CurveUtil {

		public static AnimationCurve SimpleInvert(AnimationCurve curve) {
			var inverse = new AnimationCurve();
			foreach(var key in curve.keys) inverse.AddKey(key.value, key.time);
			return inverse;
		}

		public static AnimationCurve InvertTangents(AnimationCurve curve) {
			var inverse = new AnimationCurve();

			for(int i = 0; i < curve.length; i++) {
				// tangent weights
				float inWeight = (curve.keys[i].inTangent * curve.keys[i].inWeight) / 1;
				float outWeight = (curve.keys[i].outTangent * curve.keys[i].outWeight) / 1;

				inverse.AddKey(new Keyframe(
					curve.keys[i].value, curve.keys[i].time,
					1 / curve.keys[i].inTangent, 1 / curve.keys[i].outTangent,
					inWeight, outWeight)
				);
			}
			return inverse;
		}
	}
}
