﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable IDE0051 // Remover membros privados não utilizados
#pragma warning disable CS0649

namespace Drafts {

	/// <summary>A wrapper to Coroutine that make possible to retrieve the result value.</summary>
	public class Coroutine<T> {
		object result;
		IEnumerator target;
		public T Result => (T)result;
		public Coroutine(IEnumerator task) => target = task;
		public Coroutine Start(MonoBehaviour owner) => owner.StartCoroutine(Run());

		private IEnumerator Run() {
			while(target.MoveNext()) {
				result = target.Current;
				yield return null;
			}
		}
	}

	/// <summary>A wrapper to Coroutine that make possible to retrieve the result value.</summary>
	public class Coroutine<T,U> {
		IEnumerator<T> target;
		public T Result { get; private set; }
		public Coroutine(IEnumerator<T> task) => target = task;
		public Coroutine Start(MonoBehaviour owner) => owner.StartCoroutine(Run());

		private IEnumerator Run() {
			while(target.MoveNext()) {
				Result = target.Current;
				yield return null;
			}
		}
	}

	public static class Validate {
		/// <summary>Resize the array if null or different from [size].</summary>
		public static bool ArraySize<T>(ref T[] array, int size) {
			if (array == null || array.Length != size) { Array.Resize(ref array, size); return true; }
			return false;
		}
	}
	
	/// <summary> </summary>
	[Obsolete]
	public class Flex {
        public int? i; public string s; public float? f; public bool? b;
        public static implicit operator Flex(string v) => new Flex { s = v };
        public static implicit operator Flex(int v) => new Flex { i = v };
        public static implicit operator Flex(float v) => new Flex { f = v };
        public static implicit operator Flex(bool v) => new Flex { b = v };
        public static implicit operator Flex(DateTime v) => new Flex() { s = v.ToString("yyyy-MM-dd HH:mm:ss") };
        public static implicit operator bool(Flex v) => v != null;
        public static implicit operator string(Flex v) => v.ToString();
        public string SQL => i?.ToString() ?? f?.ToString() ?? b?.ToString() ?? "'" + s + "'";
        public override string ToString() => i?.ToString() ?? f?.ToString() ?? b?.ToString() ?? s;
    }

    /// <summary>Like Tuple and KeyValuePair, but is not readonly. Use Pair.New() to implicitly set types.</summary>
	[Obsolete("", true)]
    public class Pair<Key, Value> {
		public Key key;
        public Value value;
        public Pair(Key k, Value v) { key = k; value = v; }
        /// <summary>Implicitly set types. (little less typing)</summary>
        public static Pair<KEY, VALUE> New<KEY, VALUE>(KEY key, VALUE value) => new Pair<KEY, VALUE>(key, value);
    }

	public static class Serializer {
		static BinaryFormatter bf = new BinaryFormatter();
		static FileStream stream;
		public static void Write(string path) => stream = new FileStream(path, FileMode.Create, FileAccess.Write);
		public static void Read(string path) => stream = new FileStream(path, FileMode.Open, FileAccess.Read);
		public static void Serialize(object obj) => bf.Serialize(stream, obj);
		public static T Deserialize<T>() => (T)bf.Deserialize(stream);
		public static void Close() => stream.Close();
		public static byte[] ToArray(object obj) {
			using(MemoryStream s = new MemoryStream()) {
				bf.Serialize(s, obj);
				return s.ToArray();
			}
		}
	}

}
