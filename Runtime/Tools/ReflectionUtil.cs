using Drafts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Drafts {
	public static class ReflectionUtil {

		public static IEnumerable<Assembly> GetAllAssembliesExcludingUnitys() =>
			AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Substring(0, 5) != "Unity");

		/// <summary>BindingFlags: Public, NonPublic, Instance, Static, FlattenHierarchy.</summary>
		public const BindingFlags CommonFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;

		/// <summary>BindingFlags: NonPublic, Instance, Static.</summary>
		public const BindingFlags PrivateFlags = BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

		/// <summary>Find NonPublic Instance methods. Stop looking when base class is U.</summary>
		public static IEnumerable<FieldInfo> FindInheritedPrivateFields<U>(this Type type) {
			IEnumerable<FieldInfo> fields = new FieldInfo[0];
			while(type != null && type != typeof(U)) {
				fields = fields.Concat(type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance));
				type = type.BaseType;
			}
			return fields;
		}
		/// <summary>Find NonPublic Instance methods. Stop looking when base class is U.</summary>
		public static IEnumerable<FieldInfo> FindSerializableFields<U>(this Type type) {
			var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
			bool Elegible(FieldInfo f) => f.IsPublic || f.HasAttribute<SerializeField>();

			IEnumerable<FieldInfo> fields = new FieldInfo[0];

			while(type != null && type != typeof(U)) {
				fields = fields.Concat(type.GetFields(flags).Where(f => Elegible(f)));
				type = type.BaseType;
			}
			return fields;
		}

		/// <summary>Ignore abstract and generic types</summary>
		public static List<Type> FindAllDerivedTypes<T>(Assembly assembly)
			=> FindAllDerivedTypes(new[] { assembly }, typeof(T));

		/// <summary>Ignore abstract and generic types</summary>
		public static List<Type> FindAllDerivedTypes(Assembly assembly, Type baseType)
			=> FindAllDerivedTypes(new[] { assembly }, baseType);

		/// <summary>Ignore abstract and generic types</summary>
		public static List<Type> FindAllDerivedTypes<T>(IEnumerable<Assembly> assemblies)
			=> FindAllDerivedTypes(assemblies, typeof(T));

		/// <summary>Ignore abstract and generic types</summary>
		public static List<Type> FindAllDerivedTypes(IEnumerable<Assembly> assemblies, Type baseType) {
			var derivedType = baseType;
			List<Type> all = new List<Type>();
			assemblies.ForEach(a => all.AddRange(a.GetTypes().Where(t =>
			   t != derivedType && derivedType.IsAssignableFrom(t)
			   && !t.IsAbstract && !t.IsGenericType)));
			return all;
		}

		/// <summary>Get the value of a field, property or parameterless function by name using reflection.</summary>
		public static void SetValue(this MemberInfo memberInfo, object instance, object value) {
			switch(memberInfo.MemberType) {
				case MemberTypes.Field: ((FieldInfo)memberInfo).SetValue(instance, value); break;
				case MemberTypes.Property: ((PropertyInfo)memberInfo).SetValue(instance, value); break;
				default: throw new Exception("Member is neither field, property or method.");
			}
		}
		/// <summary>Get the value of a field, property or parameterless function by name using reflection.</summary>
		public static object GetValue(this MemberInfo memberInfo, object instance) {
			switch(memberInfo.MemberType) {
				case MemberTypes.Field: return ((FieldInfo)memberInfo).GetValue(instance);
				case MemberTypes.Property: return ((PropertyInfo)memberInfo).GetValue(instance);
				case MemberTypes.Method: return ((MethodInfo)memberInfo).Invoke(instance, null);
				default: throw new Exception("Member is neither field, property or method.");
			}
		}
		/// <summary>Get the value of a field, property or parameterless function by name using reflection.</summary>
		public static T GetValue<T>(this MemberInfo memberInfo, object instance)
			=> (T)GetValue(memberInfo, instance);

		public static Type ReturnType(this MemberInfo memberInfo) {
			switch(memberInfo.MemberType) {
				case MemberTypes.Field: return ((FieldInfo)memberInfo).FieldType;
				case MemberTypes.Property: return ((PropertyInfo)memberInfo).PropertyType;
				case MemberTypes.Method: return ((MethodInfo)memberInfo).ReturnType;
				default: throw new Exception("Member is neither field, property or method.");
			}
		}

		public static bool HasAttribute<T>(this MemberInfo memberInfo) where T : Attribute
			=> memberInfo.GetCustomAttribute<T>() != null;

		public static bool HasAttribute<T>(this MemberInfo memberInfo, out T attribute) where T : Attribute {
			attribute = memberInfo.GetCustomAttribute<T>();
			return attribute != null;
		}

		/// <summary>Get the value of a field, property or parameterless function by name using reflection.</summary>
		public static T Reflect<T>(this object obj, string fieldName, BindingFlags flags = CommonFlags) {
			try {
				return obj.GetType().GetMember(fieldName, flags)[0].GetValue<T>(obj);
			} catch {
				Debug.LogError($"Member {fieldName} not found or type not supported in {obj.GetType()}.");
				return default;
			}
		}
		/// <summary>Set the value of a field or property by name using reflection.</summary>
		public static void ReflectSet(this object obj, string fieldName, object value, BindingFlags flags = CommonFlags) {
			try {
				obj.GetType().GetMember(fieldName, flags)[0].SetValue(obj, value);
			} catch {
				Debug.LogError($"Member {fieldName} not found or type not supported in {obj.GetType()}.");
			}
		}
	}
}
