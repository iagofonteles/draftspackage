#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Drafts;
using Drafts.Extensions;
using UnityEditor;
using UnityEngine;
using UObj = UnityEngine.Object;

namespace DraftsEditor {

	public static class EditorUtil {

		public static T[] FindPrefabsOfType<T>(params string[] folders) where T : Component {
			var guids = AssetDatabase.FindAssets("t:GameObject", folders);
			return guids.Select(g => FindAssetByGUID<GameObject>(g).GetComponent<T>()).Where(c => c).ToArray();
		}

		public static Component[] FindPrefabsOfType(Type type, params string[] folders) {
			var guids = AssetDatabase.FindAssets("t:GameObject", folders);
			return guids.Select(g => FindAssetByGUID<GameObject>(g).GetComponent(type)).Where(c => c).ToArray();
		}

		public static T FindAssetByGUID<T>(string guid) where T : UObj => AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guid));

		/// <summary></summary>
		/// <param name="name">enum name</param>
		/// <param name="names">enum elements</param>
		/// <param name="removePrefix">Removes first n characters from the name.</param>
		/// <param name="ident">line identation if needed.</param>
		/// <returns>The complete enum structure.</returns>
		public static string GenerateEnum(string name, IEnumerable<string> names, int removePrefix = 0, string ident = "") {
			var @enum = "";
			foreach(var s in names) {
				if(!s.IsValidVariableName()) throw new Exception($"Invalid enum name: {s}");
				@enum += $"\n{ident}\t@{s.Substring(removePrefix)},";
			}
			return $"{ident}public enum {name} {{ {@enum} \n{ident}}}";
		}

		public static string GetRawTemplate(string name) => AssetDatabase.LoadAssetAtPath<TextAsset>(
			$"Packages/com.iagofonteles.draftsengines/Templates/{name}").text;

		public static string DraftsTemplate(string tempalte, params string[] values) {
			var str = GetRawTemplate(tempalte);
			for(int i = 0; i < values.Length; i++)
				str = str.Replace($"@{i}", values[i]);
			return str;
		}

		public static string DraftsTemplate(string tempalte, params (string, string)[] pairs) {
			var str = GetRawTemplate(tempalte);
			for(int i = 0; i < pairs.Length; i++)
				str = str.Replace(pairs[i].Item1, pairs[i].Item2);
			return str;
		}


		/// <summary>Get a resulting path relative to asset.</summary>
		public static string RelativePath(this UObj asset, string path) {
			var p = AssetDatabase.GetAssetPath(asset);
			p = Path.GetDirectoryName(p);
			return Path.Combine(p, path);
		}

		/// <summary>Get a list of assets of a type in specified folder.</summary>
		public static IEnumerable<T> FindAssetsAtPath<T>(string path) where T : UObj
			=> AssetDatabase.FindAssets($"t:{typeof(T).Name}", new[] { path })
				.Select(s => AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(s)));

		/// <summary>Get a list of assets of a type in specified folder.</summary>
		public static IEnumerable<UObj> FindAssetsAtPath2(string path, Type type)
			=> AssetDatabase.FindAssets($"t:{type.Name}", new[] { path })
				.Select(s => AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(s), type));

		/// <summary>Load all assets of T in a subfolder and put in a list.</summary>
		public static void StoreAssetsInList<T>(this ScriptableObject so, string folder, List<T> list) where T : UObj {
			list.RemoveAll(i => !i);
			var sos = FindAssetsAtPath<T>(RelativePath(so, folder));
			foreach(var o in sos)
				if(list.All(i => i.name != o.name))
					list.Add(o);
		}

		/// <summary>Find SO Classes derived from T that arent in the list yet, create assets in specified folder. Ignore Generic and Abstract classes.</summary>
		public static void CreateSOInstanceForTypeChildren<T>(this UObj so, string folder, List<T> list, params Type[] ignoreTypes) where T : ScriptableObject {
			var assembly = Assembly.GetAssembly(so.GetType());
			var types = ReflectionUtil.FindAllDerivedTypes<T>(assembly).Where(t => !ignoreTypes.Contains(t));
			foreach(var type in types) {
				if(list.Any(i => i.GetType() == type)) continue;
				var _new = ScriptableObject.CreateInstance(type);
				AssetDatabase.CreateAsset(_new, RelativePath(so, $"{folder}/{type.Name}.asset"));
				list.Add((T)_new);
				Debug.Log($"{folder}/ {type.Name}	Created");
			}
		}
		/// <summary>Find Classes derived from T whose names arent in the list yet, create assets in specified folder. Ignore Generic and Abstract classes.
		/// With this, you can divide info on a ScriptableObject and functionality in a normal class creating a link betwenn them.</summary>
		public static void CreateInfoSOForTypeChildren<T, SO>(this UObj so, string folder, List<SO> list, params Type[] ignoreTypes) where SO : ScriptableObject {
			var assembly = Assembly.GetAssembly(so.GetType());
			var types = ReflectionUtil.FindAllDerivedTypes<T>(assembly).Where(t => !ignoreTypes.Contains(t));
			foreach(var type in types) {
				if(list.Any(i => i.name == type.Name)) continue;
				var _new = ScriptableObject.CreateInstance<SO>();
				AssetDatabase.CreateAsset(_new, RelativePath(so, $"{folder}/{type.Name}.asset"));
				list.Add(_new);
				Debug.Log($"{folder}/ {type.Name}	Created");
			}
		}

		public static void CreateEnumForListIndexes<T>(string namespacee, params (string enumName, List<T> list)[] lists) where T : UObj {
			var intro = $"namespace {namespacee} {{";
			foreach(var el in lists) {
				var entrys = el.list.Aggregate("\n", (s, i) => $"{s}\n@{i.name},");
				Debug.Log($"public enum {el.enumName} {{ {entrys} \n }}");
			}
		}

		public static void CreateOrReplaceFile(UObj relativeTo, string path, string content, bool refresh = true) {
			path = relativeTo ? Path.Combine(Path.GetDirectoryName(AssetDatabase.GetAssetPath(relativeTo)), path) : path;
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			File.WriteAllText(path, content);
			if(refresh) AssetDatabase.Refresh();
		}

		/// <summary>Does not overwrite existent files.</summary>
		public static bool CreateFileOnly(UObj relativeTo, string path, string content, bool refresh = true) {
			path = relativeTo ? Path.Combine(Path.GetDirectoryName(AssetDatabase.GetAssetPath(relativeTo)), path) : path;
			if(File.Exists(path)) return false;
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			File.WriteAllText(path, content);
			if(refresh) AssetDatabase.Refresh();
			return true;
		}

		public static void CreateOrReplaceFile(UObj relativeTo, string path, UObj content, bool refresh = true) {
			path = relativeTo ? Path.Combine(Path.GetDirectoryName(AssetDatabase.GetAssetPath(relativeTo)), path) : path;
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			AssetDatabase.CreateAsset(content, path);
			if(refresh) AssetDatabase.Refresh();
		}

		/// <summary>Does not overwrite existent files.</summary>
		public static T CreateFileOnly<T>(UObj relativeTo, string path, T content, bool refresh = true) where T : UObj {
			path = relativeTo ? Path.Combine(Path.GetDirectoryName(AssetDatabase.GetAssetPath(relativeTo)), path) : path;
			if(File.Exists(path)) return AssetDatabase.LoadAssetAtPath<T>(path);
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			AssetDatabase.CreateAsset(content, path);
			if(refresh) AssetDatabase.Refresh();
			return content;
		}
	}
}
#endif
