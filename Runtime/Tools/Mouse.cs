﻿#if !ENABLE_INPUT_SYSTEM

using UnityEngine;
using System.Diagnostics.CodeAnalysis;
using UnityEngine.EventSystems;
using Drafts.Extensions;

namespace Drafts {
	/// <summary>Mouse position delta on the last frame.</summary>
	public class DMouse : MonoBehaviour {

		static DMouse instance;
		static DMouse Instance {
			get {
				if(!instance) instance = new GameObject("Mouse Delta Tracker", typeof(DMouse)).GetComponent<DMouse>();
				return instance;
			}
		}

		// <summary>Mouse position delta in World coordinates.</summary>
		//public static Vector3 WorldDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0) * 10;
		/// <summary>Mouse position delta normalized. 1 is Screen.width/height. This makes resolution independent.</summary>
		public static Vector2 NormalizedScreenDelta => Vector2.Scale(Instance.screenDelta, new Vector2(Screen.width, Screen.height));
		/// <summary>Mouse position delta in Pixels.</summary>
		public static Vector2 ScreenDelta => Instance.screenDelta;

		// <summary>Mouse position delta in World coordinates.</summary>
		//static Vector3 UnscaledWorldDelta => Instance.worldDelta;
		// <summary>Mouse position delta in World coordinates. Scaled by Screen size, this makes resolution independent.</summary>
		//public static Vector3 WorldDelta => new Vector3(Instance.worldDelta.x / Screen.width, Instance.worldDelta.y / Screen.height, 0);
		// <summary>Delta scaled by the main camera orthographic size.</summary>
		//static Vector2 OrthographicDelta => ScreenDelta * Camera.main.orthographicSize / 5;
		// <summary>Delta scaled by the main camera field of view.</summary>
		//static Vector2 PerspectiveDelta => ScreenDelta * Camera.main.fieldOfView;
		// <summary>Delta scaled by either orthographic size or field of view depending on main camera projection mode.</summary>
		//static Vector2 ScaledScreenDelta => Camera.main.orthographic ? OrthographicDelta : PerspectiveDelta;

		Vector2 screenDelta = Vector2.zero;
		Vector2 _lastScreenPos;

		[SuppressMessage("", "IDE0051")]
		void Start() => _lastScreenPos = Input.mousePosition;
		[SuppressMessage("", "IDE0051")]
		void Update() {
			screenDelta = (Vector2)Input.mousePosition - _lastScreenPos;
			_lastScreenPos = Input.mousePosition;
		}

		/// <summary>Ensure there is no UI element under mouse when clicking.</summary>
#if INPUT_SYSTEM_ENABLED
		public static bool ClickOnScene => throw new System.NotImplementedException();
#else
		
		/// <summary>Ensures there is no UI element in the way when clicking.</summary>
		public static bool ClickOnScene(int button) => Input.GetMouseButtonDown(button) && !EventSystem.current.IsPointerOverGameObject();
		/// <summary></summary>
		public static Vector3 WorldPosition(float cameraDistance) => Input.mousePosition.ToWorld(cameraDistance);
		//public static RaycastHit Raycast => 

		/// <summary>Check if clicked. Get the collider with T component attached under mouse. Ensures there is no UI element in the way when clicking.</summary>
		public static bool ClickOnScene<T>(int button, out T component, float maxDistance = Mathf.Infinity) where T : Component {
			var r = ClickOnScene(button, out var go, maxDistance);
			component = go?.GetComponent<T>();
			return r;
		}

		/// <summary>Check if clicked. Get the collider with T component attached under mouse. Ensures there is no UI element in the way when clicking.</summary>
		public static bool ClickOnScene(int button, out GameObject gameObject, float maxDistance = Mathf.Infinity) {
			gameObject = null;
			if(!ClickOnScene(button)) return false;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out var hit, maxDistance))
				gameObject = hit.collider.gameObject;
			return true;
		}
#endif
	}
}
#endif
