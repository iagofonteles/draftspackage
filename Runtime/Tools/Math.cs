﻿using UnityEngine;
using System;
using System.Diagnostics.CodeAnalysis;
using Drafts.Extensions;

//#pragma warning disable CS1591 // XML ausente para membro publico

namespace Drafts {

	public static class MyMath {
		//public static int Repeat(this ref int i, int length, int step = 1) => i = (int)Mathf.Repeat(i + step, length);
		//// <summary>If i passes max, set it to max. Returns i-max.</summary>
		//public static int Crop(this ref int i, int max) { var r = i - max; i = Math.Min(i, max); return r; }

		/// <summary>Return the specified value when f>0, f==0 or f&lt;0.</summary>
		public static T IfSign<T>(this float f, T positive, T zero, T negative) => f > 0 ? positive : f == 0 ? zero : negative;
		/// <summary>Return the specified value when f>0, f==0 or f&lt;0.</summary>
		public static T IfSign<T>(this int f, T positive, T zero, T negative) => f > 0 ? positive : f == 0 ? zero : negative;

		/// <summary>Linearly interpolate c between a and b by t. Only works properly if a, b and c are siblings.</summary>
		public static void LerpLocal(this RectTransform c, RectTransform a, RectTransform b, float t) {
			c.anchorMin = Vector2.Lerp(a.anchorMin, b.anchorMin, t);
			c.anchorMax = Vector2.Lerp(a.anchorMax, b.anchorMax, t);
			c.pivot = Vector2.Lerp(a.pivot, b.pivot, t);
			c.position = Vector2.Lerp(a.position, b.position, t);
			c.sizeDelta = Vector2.Lerp(a.sizeDelta, b.sizeDelta, t);
		}

		/// <summary>Linearly interpolate c between a and b by t.</summary>
		public static void Lerp(this RectTransform c, RectTransform a, RectTransform b, float t) {
			c.anchorMin = c.anchorMax = Vector2.zero;
			c.pivot = Vector2.Lerp(a.pivot, b.pivot, t);
			c.position = Vector2.Lerp(a.position, b.position, t);
			c.sizeDelta = Vector2.Lerp(a.rect.size, b.rect.size, t);
		}

		/// <summary>Three points Lerp, makes an arc. point [c] is the offset, </summary>
		public static Vector3 Lerp(this Vector3 a, Vector3 b, Vector3 c, float t)
			=> Vector3.Lerp(Vector3.Lerp(a, b, t), Vector3.Lerp(a, c, t), t);

		/// <summary></summary>
		public static float InverseLerp(this Vector3 value, Vector3 a, Vector3 b) {
			Vector3 AB = b - a;
			Vector3 AV = value - a;
			return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
		}

		/// <summary>Returns the shortest path from v to the line ab.</summary>
		public static Vector3 NearestPointInLine(this Vector3 v, Vector3 linePointA, Vector3 linePointB) {
			var M = linePointA - linePointB;
			var t0 = Vector3.Dot(M, v - linePointA) / Vector3.Dot(M, M);
			return linePointA + t0 * M;
		}

		/// <summary>Rotates a point around a pivot.</summary>
		public static Vector3 RotateAround(this Vector3 v, Vector3 pivot, Vector3 angle) => RotateAround(v, pivot, Quaternion.Euler(angle));
		/// <summary>Rotates a point around a pivot.</summary>
		public static Vector3 RotateAround(this Vector3 v, Vector3 pivot, Quaternion angle) => angle * (v - pivot) + pivot;

		/// <summary>Rotate a vector around the origin.</summary>
		public static Vector3 Rotate(this Vector3 v, Vector3 angle) => Quaternion.Euler(angle) * v;
		/// <summary>Rotate a vector around the origin.</summary>
		public static Vector3 Rotate(this Vector3 v, Quaternion angle) => angle * v;

		/// <summary>Rotate a vector around the origin.</summary>
		public static Vector2 Rotate(this Vector2 v, float degrees) {
			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
			float tx = v.x;
			float ty = v.y;
			return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
		}

		/// <summary>Move towards a value at given speed.</summary>
		public static void MoveTowards(this Transform t, Vector3 target, float speed) => t.position = Vector3.MoveTowards(t.position, target, speed);
		/// <summary>Move towards a value at given speed.</summary>
		public static float MoveTowards(this float f, float target, float speed) => Mathf.MoveTowards(f, target, speed);

		#region Semi Physics
		/// <summary>Like Lerp, but not linear. Can specify the height</summary>
		/// <param name="height">The height of the arc. When not specified it will look like a circle.</param>
		/// <param name="direction">Direction of the height. Default: Up.</param>
		[Obsolete("Use Lerp(a,b,c,t)"), SuppressMessage("", "CS1573")]
		public static Vector3 ArcLerp(Vector3 a, Vector3 b, float f, float? height = null, Vector3? direction = null)
			=> Vector3.Lerp(a, b, f) + (direction ?? Vector3.up) * Mathf.Sin(f * Mathf.PI) * (height ?? Vector3.Distance(a, b) / 2);

		/// <summary>Used to simply simulate jump/swing/throw motions without physics</summary>
		public static Vector3 JumpTowards(this Vector3 v, Vector3 start, Vector3 end, float linearSpeed) => v.JumpTowards(start, end, Vector3.Distance(start, end) / 2, linearSpeed);
		/// <summary>Used to simply simulate jump/swing/throw motions without physics</summary>
		public static Vector3 JumpTowards(this Vector3 v, Vector3 start, Vector3 end, float height, float linearSpeed) {
			var t = v.NearestPointInLine(start, end);
			var f = Mathf.Min(t.InverseLerp(start, end) + linearSpeed, 1);
			var h = (end - start) / 2;
			h.y += height;
			return Lerp(start, end,  h, f);
		}
		#endregion
	}

}
