﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Drafts {

	[AddComponentMenu("Drafts/Components/Timer")]
	public class TimerComponent : MonoBehaviour {
		[Min(0)] public float timeLimit = 1;
		public bool autoReset = true;
		public bool playOnStart = true;

		public UnityEvent OnTrigger;
		public UnityEvent<float> OnUpdate;
		public UnityEvent OnReset;

		Timer timer;
		private void Start() {
			timer = new Timer(autoReset) {
				onTrigger = OnTrigger.Invoke,
				onUpdate = OnUpdate.Invoke,
				onReset = OnReset.Invoke,
			};
			if(!playOnStart) timer.pause = true;
		}

		void Update() => timer.Update(timeLimit);
		public void Play() => timer.pause = false;
		public void Pause(bool pause) => timer.pause = pause;
		public void Trigger() => timer.Trigger();
		public void Reset() => timer.Reset();
		public static implicit operator Timer(TimerComponent t) => t.timer;
	}

	/// <summary>Based on Time.deltatime. Trigger events as time passes. Good to sync UI with cooldowns.</summary>
	[Serializable]
	public class Timer {
		/// <summary>Current elapsed time.</summary>
		public virtual float Time { get; set; } = 0;
		/// <summary>Auto reset after tirgger.</summary>
		public bool autoReset = true;
		/// <summary>Pause and Resume timer.</summary>
		public bool pause = false;

		/// <summary>Called once reach timeLimit.</summary>
		public Action onTrigger;
		/// <summary>Called on reset.</summary>
		public Action onReset;
		/// <summary>Called on Update</summary>
		public Action<float> onUpdate;

		/// <summary>Overrides WaitWhile and FixedTimeLimit.</summary>
		public float TimeLimit { set => GetTimeLimit = () => value; }
		/// <summary>Overrides TimeLimit and FixedTimeLimit.</summary>
		public Func<bool> WaitWhile { set => GetTimeLimit = () => value() ? float.MaxValue : 0; }
		/// <summary>Overrides WaitWhile and TimeLimit.</summary>
		public Func<float> GetTimeLimit { private get; set; }

		protected bool _triggered = false;

		public Timer() { }
		public Timer(bool autoReset = true, Action onTrigger = null) { this.autoReset = autoReset; this.onTrigger = onTrigger; }

		/// <summary>Not triggered AND not paused.</summary>
		public bool IsPlaying => !_triggered && !pause;
		/// <summary>Already triggered.</summary>
		public bool IsDone => _triggered;
		/// <summary>Pause and Resume timer. Note: you still need to call Update().</summary>
		public void Pause(bool b) => pause = b;
		/// <summary>Set time to 0 and call OnReset.</summary>
		public void Reset(bool resume = false) { _triggered = false; Time = 0; onReset?.Invoke(); if(resume) pause = false; }

		/// <summary>Same as Update(float). Return true when Triggered.</summary>
		public bool this[float t] => _update(t, UnityEngine.Time.deltaTime);

		/// <summary>Return true when triggered.</summary>
		public void Update() => _update(UnityEngine.Time.deltaTime);
		/// <summary>Return true when triggered.</summary>
		public void Update(float timeLimit) => _update(timeLimit, UnityEngine.Time.deltaTime);

		/// <summary>Return true when triggered.</summary>
		public void FixedUpdate() => _update(UnityEngine.Time.fixedDeltaTime);
		/// <summary>Return true when triggered.</summary>
		public void FixedUpdate(float timeLimit) => _update(timeLimit, UnityEngine.Time.fixedDeltaTime);

		protected bool _update(float deltaTime) => _update(GetTimeLimit(), deltaTime);
		protected bool _update(float timeLimit, float deltaTime) {
			if(_triggered || pause) return false;
			var amt = (Time += deltaTime) / timeLimit;
			if(amt < 1) {
				onUpdate?.Invoke(amt);
				return false;
			} else {
				Trigger();
				return true;
			}
		}

		public void Trigger() {
			onUpdate?.Invoke(1);
			onTrigger?.Invoke();
			_triggered = true;
			if(autoReset) Reset();
		}
		public void Cancel() {
			onUpdate?.Invoke(0);
			_triggered = true;
		}
	}

	/// <summary>Based on DateTime. Call Update to triggger events. Good to sync UI with cooldowns.</summary>
	[Serializable]
	public class TimerClock : Timer {
		[SerializeField] DateTime time = DateTime.Now;
		/// <summary>Current elapsed time.</summary>
		public override float Time { get => (float)(DateTime.Now - time).TotalSeconds; set => time = DateTime.Now.AddSeconds(value); }
		public TimerClock(bool autoReset = true, Action onTrigger = null) : base(autoReset, onTrigger) { }
	}
}
