﻿#if DRAFTS_USE_FMOD
using UnityEngine;

namespace Drafts.FMOD {
	public class FMODOneShots : MonoBehaviour {
		[SerializeField] FMODSoundAlias[] sounds;
		public void PlayOneShot(string sound) => sounds.Play(sound, transform.position);
	}
}
#endif