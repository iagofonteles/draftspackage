﻿#if DRAFTS_USE_FMOD
using UnityEngine;
using Drafts.Extensions;

namespace Drafts.FMOD {
	public class FMODEventInstances : MonoBehaviour {
		[SerializeField] FMODEventInstanceAlias[] sounds;
		public void PlaySound(string sound) => sounds.Play(sound, transform.position);
		private void OnDestroy() => sounds.ForEach(s => s.sound.Release());
	}
}
#endif