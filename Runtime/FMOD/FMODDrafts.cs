#if DRAFTS_USE_FMOD
using UnityEngine;
using FMODUnity;
using System;
using System.Linq;
using FMOD.Studio;

namespace Drafts.FMOD {

	/// <summary>Use for one shot sfx.</summary>
	[Serializable]
	public class FMODSound {
		[EventRef] public string sound;
		public void Play(Vector3 position = default) => RuntimeManager.PlayOneShot(sound, position);
	}

	[Serializable]
	public class FMODSoundAlias {
		public string alias;
		[EventRef] public string sound;
		public void Play(Vector3 position = default) => RuntimeManager.PlayOneShot(sound, position);
	}

	/// <summary>Remember to release on destroy.</summary>
	[Serializable]
	public class FMODEvent {
		[EventRef] public string sound;
		EventInstance instance;

		public FMODEvent(string path) => sound = path;

		public void Play(Vector3 position = default) {
			if(!instance.isValid()) instance = RuntimeManager.CreateInstance(sound);
			if(position != default) instance.set3DAttributes(RuntimeUtils.To3DAttributes(position));
			instance.start();
		}

		public void Release() => instance.release();
	}

	[Serializable]
	public class FMODEventInstanceAlias {
		public string alias;
		public FMODEvent sound;
		public void Play(Vector3 position = default) => sound.Play(position);
	}

	public static class FMODAliasExtension {

		public static void Play(this FMODEventInstanceAlias[] alias, string sound, Vector3 position = default)
			=> alias.First(a => a.alias == sound).Play(position);

		public static void Play(this FMODSoundAlias[] alias, string sound, Vector3 position = default)
			=> alias.First(a => a.alias == sound).Play(position);
	}
}
#endif
