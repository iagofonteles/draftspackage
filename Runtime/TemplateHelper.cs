﻿using System;
using UnityEngine;
using Drafts.Extensions;

namespace Drafts {
	public static class TemplateHelper<T> {

		public static Func<T, Transform> Maker<A>(Transform template,
			(A child, Action<A, T> action) action1)
			where A : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2)
			where A : Component where B : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3)
			where A : Component where B : Component
			where C : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C, D>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3,
			(D child, Action<D, T> action) action4)
			where A : Component where B : Component
			where C : Component where D : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);
			var path4 = template.PathTo(action4.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				action4.action(t.Find<D>(path4), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C, D, E>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3,
			(D child, Action<D, T> action) action4,
			(E child, Action<E, T> action) action5)
			where A : Component where B : Component
			where C : Component where D : Component
			where E : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);
			var path4 = template.PathTo(action4.child.transform);
			var path5 = template.PathTo(action5.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				action4.action(t.Find<D>(path4), i);
				action5.action(t.Find<E>(path5), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C, D, E, F>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3,
			(D child, Action<D, T> action) action4,
			(E child, Action<E, T> action) action5,
			(F child, Action<F, T> action) action6)
			where A : Component where B : Component
			where C : Component where D : Component
			where E : Component where F : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);
			var path4 = template.PathTo(action4.child.transform);
			var path5 = template.PathTo(action5.child.transform);
			var path6 = template.PathTo(action6.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				action4.action(t.Find<D>(path4), i);
				action5.action(t.Find<E>(path5), i);
				action6.action(t.Find<F>(path6), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C, D, E, F, G>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3,
			(D child, Action<D, T> action) action4,
			(E child, Action<E, T> action) action5,
			(F child, Action<F, T> action) action6,
			(G child, Action<G, T> action) action7)
			where A : Component where B : Component
			where C : Component where D : Component
			where E : Component where F : Component
			where G : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);
			var path4 = template.PathTo(action4.child.transform);
			var path5 = template.PathTo(action5.child.transform);
			var path6 = template.PathTo(action6.child.transform);
			var path7 = template.PathTo(action7.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				action4.action(t.Find<D>(path4), i);
				action5.action(t.Find<E>(path5), i);
				action6.action(t.Find<F>(path6), i);
				action7.action(t.Find<G>(path7), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

		public static Func<T, Transform> Maker<A, B, C, D, E, F, G, H>(Transform template,
			(A child, Action<A, T> action) action1,
			(B child, Action<B, T> action) action2,
			(C child, Action<C, T> action) action3,
			(D child, Action<D, T> action) action4,
			(E child, Action<E, T> action) action5,
			(F child, Action<F, T> action) action6,
			(G child, Action<G, T> action) action7,
			(H child, Action<H, T> action) action8)
			where A : Component where B : Component
			where C : Component where D : Component
			where E : Component where F : Component
			where G : Component where H : Component {

			var parent = template.parent;
			var path1 = template.PathTo(action1.child.transform);
			var path2 = template.PathTo(action2.child.transform);
			var path3 = template.PathTo(action3.child.transform);
			var path4 = template.PathTo(action4.child.transform);
			var path5 = template.PathTo(action5.child.transform);
			var path6 = template.PathTo(action6.child.transform);
			var path7 = template.PathTo(action7.child.transform);
			var path8 = template.PathTo(action8.child.transform);

			Func<T, Transform> ret = i => {
				//var i = getParameters(id);
				var t = GameObject.Instantiate(template, parent).transform;
				t.gameObject.SetActive(true);
				t.SetParent(parent, false);

				action1.action(t.Find<A>(path1), i);
				action2.action(t.Find<B>(path2), i);
				action3.action(t.Find<C>(path3), i);
				action4.action(t.Find<D>(path4), i);
				action5.action(t.Find<E>(path5), i);
				action6.action(t.Find<F>(path6), i);
				action7.action(t.Find<G>(path7), i);
				action8.action(t.Find<H>(path8), i);
				return t;
			};

			template.gameObject.SetActive(false);
			return ret;
		}

	}
}
