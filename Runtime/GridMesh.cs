﻿using System;
using UnityEngine;

namespace Drafts {
	public class GridMesh {

		public Vector2Int Size { get; private set; }
		public Mesh Mesh { get; private set; }
		public Func<int, int, Vector2> GetUvPosition;
		public Vector2 UvSize;
		Vector2 cellSize;

		public static implicit operator Mesh(GridMesh v) => v.Mesh;
		public int GetVerticeId(int x, int y) => (x * Size.y + y) * 4;

		/// <summary></summary>
		/// <param name="size"></param>
		/// <param name="cellSize">Default: 1</param>
		/// <param name="getUvPosition">Default: tiled (Vector2.zero)</param>
		/// <param name="uvSize">Default: tiled (Vector2.one)</param>
		/// <param name="z"></param>
		public GridMesh(Vector2Int size, Vector2? cellSize = null, Func<int, int, Vector2> getUvPosition = null, Vector2? uvSize = null, int z = 0) {
			Size = size;
			this.cellSize = cellSize ?? Vector2.one;
			GetUvPosition = getUvPosition ?? ((x, y) => Vector2.zero);
			UvSize = uvSize ?? Vector2.one;
			Mesh = new Mesh();
			RecreateMesh(z);
		}

		public void RecreateMesh(int z = 0) {
			int length = Size.x * Size.y;

			Vector3[] vertices = new Vector3[length * 4];
			Vector2[] uvs = new Vector2[length * 4];
			int[] triangles = new int[length * 6];
			Color[] colors = new Color[length * 4];

			for(int x = 0; x < Size.x; x++)
				for(int y = 0; y < Size.y; y++) {

					var index = x * Size.y + y;
					var vertId = index * 4;
					var triId = index * 6;

					var v0 = vertId + 0;
					var v1 = vertId + 1;
					var v2 = vertId + 2;
					var v3 = vertId + 3;

					vertices[v0] = new Vector3(x * cellSize.x, y * cellSize.y, z);
					vertices[v1] = new Vector3(x * cellSize.x, y * cellSize.y + cellSize.y, z);
					vertices[v2] = new Vector3(x * cellSize.x + cellSize.x, y * cellSize.y + cellSize.y, z);
					vertices[v3] = new Vector3(x * cellSize.x + cellSize.x, y * cellSize.y, z);


					triangles[triId + 0] = v0;
					triangles[triId + 1] = v1;
					triangles[triId + 2] = v3;
					triangles[triId + 3] = v3;
					triangles[triId + 4] = v1;
					triangles[triId + 5] = v2;

					var texCoord = GetUvPosition(x, y);
					uvs[v0] = texCoord + UvSize * Vector2.zero;
					uvs[v1] = texCoord + UvSize * Vector2.up;
					uvs[v2] = texCoord + UvSize * Vector2.one;
					uvs[v3] = texCoord + UvSize * Vector2.right;

					colors[v0] = Color.white;
					colors[v1] = Color.white;
					colors[v2] = Color.white;
					colors[v3] = Color.white;
				}

			Mesh.vertices = vertices;
			Mesh.uv = uvs;
			Mesh.triangles = triangles;
			Mesh.colors = colors;
		}

		public void SetUv(int x, int y, Vector2 uvPosition) {
			var vertId = GetVerticeId(x, y);
			var uvs = Mesh.uv;

			uvs[vertId + 0] = uvPosition + UvSize * Vector2.zero;
			uvs[vertId + 1] = uvPosition + UvSize * Vector2.up;
			uvs[vertId + 2] = uvPosition + UvSize * Vector2.one;
			uvs[vertId + 3] = uvPosition + UvSize * Vector2.right;

			Mesh.uv = uvs;
		}
		public void SetUvSilent(int x, int y, Vector2 uvPosition) {
			var vertId = GetVerticeId(x, y);
			Mesh.uv[vertId + 0] = uvPosition + UvSize * Vector2.zero;
			Mesh.uv[vertId + 1] = uvPosition + UvSize * Vector2.up;
			Mesh.uv[vertId + 2] = uvPosition + UvSize * Vector2.one;
			Mesh.uv[vertId + 3] = uvPosition + UvSize * Vector2.right;
		}
		public void ApplyUv() => Mesh.uv = Mesh.uv;

		public void SetColor(int x, int y, Color color) {
			var vertId = GetVerticeId(x, y);
			var colors = Mesh.colors;

			colors[vertId + 0] = color;
			colors[vertId + 1] = color;
			colors[vertId + 2] = color;
			colors[vertId + 3] = color;

			Mesh.colors = colors;
		}

		public void SetColorSilent(int x, int y, Color color) {
			var vertId = GetVerticeId(x, y);
			Mesh.colors[vertId + 0] = color;
			Mesh.colors[vertId + 1] = color;
			Mesh.colors[vertId + 2] = color;
			Mesh.colors[vertId + 3] = color;
		}
		public void ApplyColor() => Mesh.colors = Mesh.colors;

		public void ResetColors(Color color) {
			var colors = new Color[Mesh.vertexCount];
			for(int i = 0; i < Mesh.vertexCount; i++)
				colors[i] = color;
			Mesh.colors = colors;
		}
	}
}
