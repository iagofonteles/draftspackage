﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Drafts {

	[Serializable]
	public class DailyMission {
		[SerializeField] string path;
		[SerializeField] Dictionary<DateTime, Info[]> info;

		public Info this[DateTime date] {
			get => GetCalendar(date)[date.Day - 1];
			set { GetCalendar(date)[date.Day] = value; OnStateChange?.Invoke(); }
		}
		public void Complete() => Complete(DateTime.Today);
		public void Complete(DateTime date) { GetCalendar(date)[date.Day - 1].completed = true; OnStateChange?.Invoke(); }
		public void Reward() => Reward(DateTime.Today);
		public void Reward(DateTime date) { GetCalendar(date)[date.Day - 1].rewarded = true; OnStateChange?.Invoke(); }

		public event Action OnStateChange;

		public DailyMission(string fileName) { path = $"{Application.persistentDataPath}/{fileName}"; Load(); }
		public DailyMission(Dictionary<DateTime,Info[]> info) => this.info = info;

		public void Save() {
			var file = File.OpenWrite(path);
			var bf = new BinaryFormatter();
			bf.Serialize(file, info);
			file.Close();
		}

		public void Load() {
			try {
				var file = File.OpenRead(path);
				var bf = new BinaryFormatter();
				info = (Dictionary<DateTime, Info[]>)bf.Deserialize(file);
				file.Close();
			} catch {
				var date = DateTime.Today;
				var year = date.Year;
				var month = date.Month;

				info = new Dictionary<DateTime, Info[]>();
				info.Add(CurrentMonth, new Info[DateTime.DaysInMonth(year, month)]);
			}
		}

		public void Serialize(Stream stream, BinaryFormatter bf) => bf.Serialize(stream, info);
		public DailyMission Deserialize(Stream stream, BinaryFormatter bf) => new DailyMission((Dictionary<DateTime, Info[]>)bf.Deserialize(stream));

		Info[] GetCalendar(DateTime date) {
			var month = new DateTime(date.Year, date.Month, 1);
			if (info.TryGetValue(month, out var i)) return i;
			var ret = new Info[DateTime.DaysInMonth(date.Year, date.Month)];
			info.Add(month, ret);
			return ret;
		}

		public DayInfo[] GetDaysState() => GetDaysState(DateTime.Today);
		public DayInfo[] GetDaysState(DateTime date) {
			int first = (int)new DateTime(date.Year, date.Month, 1).DayOfWeek;
			int last = DateTime.DaysInMonth(date.Year, date.Month);
			var ret = new DayInfo[first > 4 ? 42 : 35];
			var d = GetCalendar(date);

			for (int i = 0; i < ret.Length; i++) {
				var day = i - first + 1;
				var curr = new DateTime(date.Year, date.Month, Mathf.Clamp(day, 1, last));
				ret[i] = new DayInfo(day, (day < 1 || day > last) ? DayState.None
					: curr > date ? DayState.Locked
					: d[day - 1].rewarded ? DayState.Rewarded
					: d[day - 1].completed ? DayState.Completed
					: curr < date ? DayState.Missed
					: DayState.Current);
			}
			return ret;
		}

		public enum DayState { None, Missed, Current, Completed, Rewarded, Locked }

		DateTime CurrentMonth => new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

		[Serializable] public struct Info { public bool completed; public bool rewarded; }

		public readonly struct DayInfo {
			public readonly int day;
			public readonly DayState state;
			public DayInfo(int day, DayState state) {
				this.day = day;
				this.state = state;
			}
		}
	}
}