﻿using System;
using UnityEngine;

namespace Drafts {
	public class StaticField : PropertyAttribute { }

	public class ReadOnlyAttribute : PropertyAttribute { }

	/// <summary>For ScriptableObjects only, you can edit the object right below the ObjectField and trigger a Validation callback function.
	/// Doesnt work with arrays/lists for now and may not work with multi editing, just remember to save the scene.</summary>
	public class EditableAttribute : PropertyAttribute {
		public string validationMethod;

		public EditableAttribute(string validationMethod = null) {
			this.validationMethod = validationMethod;
		}
	}

	public class ShowClassNameAttribute : PropertyAttribute { }

	/// <summary>Only works with a serialized bool field in the same script.</summary>
	public class ConditionalAttribute : PropertyAttribute {
		public string field;
		public bool inverse;
		public float value;
		public string str;

		public ConditionalAttribute() { }

		public ConditionalAttribute(string field, bool inverse = false) {
			this.field = field;
			this.inverse = inverse;
		}

		public ConditionalAttribute(string field, float value, bool inverse = false) {
			this.field = field;
			this.value = value;
			this.inverse = inverse;
		}

		public ConditionalAttribute(string field, string value, bool inverse = false) {
			this.field = field;
			str = value;
			this.inverse = inverse;
		}

		public bool Validate(bool v) => v ^ inverse;
		public bool Validate(int v) => v == (int)value ^ inverse;
		public bool Validate(float v) => v == value ^ inverse;
		public bool Validate(string v) => v == str ^ inverse;
		public bool Validate(UnityEngine.Object v) => v ^ inverse;
	}

	/// <summary> Note: the field needs to be serializable.</summary>
	public class ButtonAttribute : PropertyAttribute {
		public readonly string text = null;
		public readonly string methodName = null;
		public readonly Func<bool> enabled;

		public ButtonAttribute(string text = null, string methodName = null) : this(text, methodName, true, true) { }
		protected ButtonAttribute(string text, string methodName, bool runtime, bool editor) {
			this.text = text;
			this.methodName = methodName;
			enabled = () => (runtime && Application.isPlaying) || (editor && !Application.isPlaying);
		}
	}

	/// <summary> Note: the field needs to be serializable.</summary>
	public class RuntimeButtonAttribute : ButtonAttribute {
		public RuntimeButtonAttribute(string text = null, string methodName = null) : base(text, methodName, true, false) { }
	}
	/// <summary> Note: the field needs to be serializable.</summary>
	public class EditorButtonAttribute : ButtonAttribute {
		public EditorButtonAttribute(string text = null, string methodName = null) : base(text, methodName, false, true) { }
	}

	public class PropertyFieldAttribute : PropertyAttribute {
		public string name;
		public PropertyFieldAttribute(string name = null) => this.name = name;
	}

	public class AllButtonsAttribute : PropertyAttribute {
		public bool after;
		public AllButtonsAttribute(bool after = false) => this.after = after;
	}

	[AttributeUsage(AttributeTargets.Field)]
	public class InputActionAttribute : PropertyAttribute {
		public string asset;
		public Type type;
		public InputActionAttribute(string asset) => this.asset = asset;
		public InputActionAttribute(Type type) => this.type = type;
	}

	public abstract class DropdownAttribute : PropertyAttribute {
		public bool AllowTyping { get; protected set; }
		public bool FilterDropdown { get; protected set; }
		public abstract string[] GetStrings(object obj);
		public DropdownAttribute(bool allowTyping = false, bool filter = false) {
			AllowTyping = allowTyping;
			FilterDropdown = filter;
		}
	}

	public class EnumDropDown : DropdownAttribute {
		protected Type type;
		public EnumDropDown(Type type, bool allowTyping = false, bool filter = false)
			: base(allowTyping, filter) {
			this.type = type;
		}
		public override string[] GetStrings(object obj) => Enum.GetNames(type);
	}

	/// <summary>Get elements from any static member of a given class that return an array of strings.</summary>
	public class StaticDropdownAttribute : DropdownAttribute {
		Type type;
		string member;
		string[] values;

		public StaticDropdownAttribute(Type type, string member, bool allowTyping = false, bool filter = false)
			: base(allowTyping, filter) {
			this.type = type;
			this.member = member;
		}
		public StaticDropdownAttribute(string[] values, bool allowTyping = true, bool filter = false)
			: base(allowTyping, filter) {
			this.values = values;
		}

		public override string[] GetStrings(object obj) {
			if(values != null) return values;
			return type.GetMember(member, ReflectionUtil.CommonFlags)[0].GetValue<string[]>(null);
		}
	}

	/// <summary>Get elements from any member of the current class, OBS: only work in Monobehaviours.</summary>
	public class LocalDropdownAttribute : DropdownAttribute {
		string member;

		public LocalDropdownAttribute(string member, bool allowTyping = false, bool filter = false)
			: base(allowTyping, filter) {
			this.member = member;
		}

		public override string[] GetStrings(object obj) {
			try {
				return obj.GetType().GetMember(member, ReflectionUtil.CommonFlags)[0].GetValue<string[]>(obj);
			} catch {
				Debug.LogError($"Invalid member {member} in {obj.GetType().Name} for LocalDropdown, must return an array of strings.");
				return new string[0];
			}
		}

	}

	public class PreviewAttribute : PropertyAttribute {
		public float maxHeight;

		public PreviewAttribute(float maxHeight = 128) {
			this.maxHeight = maxHeight;
		}
	}
}
