﻿using UnityEngine;

namespace Drafts.Extensions {

	public static partial class ExtensionMethods {

		/// <summary>Return (a - b).sqrMagnitude < .001f.</summary>
		public static bool Near(this Vector3 a, Vector3 b) => (a - b).sqrMagnitude < .001f;

		// Vector3
		public static Vector3 Change(this Vector3 v, float? x = null, float? y = null, float? z = null) => new Vector3(x ?? v.x, y ?? v.y, z ?? v.z);
		public static Vector3Int Change(this Vector3Int v, int? x = null, int? y = null, int? z = null) => new Vector3Int(x ?? v.x, y ?? v.y, z ?? v.z);

		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3 X(this Vector3 v, float x) => new Vector3(x, v.y, v.z);
		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3 Y(this Vector3 v, float y) => new Vector3(v.x, y, v.z);
		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3 Z(this Vector3 v, float z) => new Vector3(v.x, v.y, z);

		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3Int X(this Vector3Int v, int x) => new Vector3Int(x, v.y, v.z);
		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3Int Y(this Vector3Int v, int y) => new Vector3Int(v.x, y, v.z);
		/// <summary>Return a new vector with one coordinate changed.</summary>
		public static Vector3Int Z(this Vector3Int v, int z) => new Vector3Int(v.x, v.y, z);

		/// <summary>Return a new vector that is floored on each individual axis</summary>
		public static Vector3 Floored(this Vector3 v) => new Vector3((int)v.x, (int)v.y, (int)v.z);
		/// <summary>Return a new vector that is rounded on each individual axis</summary>
		public static Vector3 Rounded(this Vector3 v) => new Vector3(Mathf.Round(v.x), Mathf.Round(v.y), Mathf.Round(v.z));
		/// <summary>Return a new vector that is rounded on each individual axis</summary>
		public static Vector3 Ceiled(this Vector3 v) => new Vector3(Mathf.Ceil(v.x), Mathf.Ceil(v.y), Mathf.Ceil(v.z));

		/// <summary>Return a new vector that is aligned with a grid o size [gridSize]</summary>
		public static Vector2 Aligned(this Vector2 v, Vector2 gridSize) => new Vector2(v.x - v.x % gridSize.x, v.y - v.y % gridSize.y);
		/// <summary>Return a new vector that is aligned with a grid o size [gridSize]</summary>
		public static Vector3 Aligned(this Vector3 v, Vector3 gridSize) => new Vector3(v.x - v.x % gridSize.x, v.y - v.y % gridSize.y, v.z - v.z % gridSize.z);

	}
}
