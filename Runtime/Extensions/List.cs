﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using URand = UnityEngine.Random;

namespace Drafts.Extensions {

	public static partial class ExtensionMethods {

		/// <summary>Remove the first value and return it.</summary>
		public static T Dequeue<T>(this List<T> list) { var v = list[0]; list.RemoveAt(0); return v; }
		/// <summary>Insert value in position 0.</summary>
		public static void Enqueue<T>(this List<T> list, T value) => list.Add(value);

		/// <summary>Add the value at the end.</summary>
		public static void Push<T>(this List<T> list, T value) => list.Add(value);
		/// <summary>Add the values at the end.</summary>
		public static void Push<T>(this List<T> list, IEnumerable<T> values) => list.AddRange(values);

		/// <summary>Remove the value at the end and return it.</summary>
		public static T Pop<T>(this List<T> list) => list.Pop(list.Count - 1);
		/// <summary>Remove the value at the end and return it.</summary>
		public static T PopOrDefault<T>(List<T> list) => list.Count > 0 ? list.Pop() : default;
		/// <summary>Remove the value at index from the list and return it.</summary>
		public static T Pop<T>(this List<T> list, int index) { var v = list[index]; list.RemoveAt(index); return v; }
		/// <summary>Remove a random value from the list and return it.</summary>
		public static T PopRandom<T>(this List<T> list) => list.Pop(URand.Range(0, list.Count));
		/// <summary>Remove [amount] values at the end and return them.</summary>
		public static List<T> PopMany<T>(this List<T> list, int amount) => list.PopMany(list.Count - amount, amount);
		/// <summary>Remove [amount] values at the end and return them.</summary>
		public static List<T> PopMany<T>(this List<T> list, int index, int amount) {
			try {
				var ret = list.GetRange(index, amount);
				list.RemoveRange(index, amount);
				return ret;
			} catch {
				Debug.LogError($"i:{index}, a:{amount}");
				return new List<T>();
			}
		}
		/// <summary>Remove the value at the end and return it. Retun false if list is empty.</summary>
		public static bool TryPop<T>(List<T> list, out T item) {
			if(list.Count > 0) {
				item = list.Pop();
				return true;
			}
			item = default;
			return false;
		}
		/// <summary>Remove the value that matches the predicate and return it. Return default if not found.</summary>
		public static T PopOrDefault<T>(this List<T> list, Func<T, bool> predicate) {
			var value = list.FirstOrDefault(predicate);
			list.Remove(value);
			return value;
		}
		/// <summary>Remove the value that matches the predicate and return it. Throw exception if not found.</summary>
		public static T Pop<T>(this List<T> list, Func<T, bool> predicate) {
			var value = list.First(predicate);
			list.Remove(value);
			return value;
		}

		/// <summary>Get the last [amount] values on the list.</summary>
		public static List<T> Peek<T>(this List<T> list, int amount) => list.GetRange(list.Count - amount, amount);
		/// <summary>Get the last value on the list.</summary>
		public static T Peek<T>(this List<T> list) => list[list.Count - 1];

		/// <summary>Swap values of index n with m.</summary>
		public static void Swap<T>(this List<T> l, int n, int m) { var v = l[n]; l[n] = l[m]; l[m] = v; }
		/// <summary>Swap values of index n with m.</summary>
		public static void Swap<T>(this T[] l, int n, int m) { var v = l[n]; l[n] = l[m]; l[m] = v; }

		/// <summary>Reorganize elements in a random order.</summary>
		public static void Shuffle<T>(this T[] l) { for(var n = 0; n < l.Length; n++) l.Swap(n, URand.Range(0, l.Length)); }
		/// <summary>Reorganize elements in a random order.</summary>
		public static void Shuffle<T>(this List<T> l) { for(var n = 0; n < l.Count; n++) l.Swap(n, URand.Range(0, l.Count)); }
		/// <summary>Add values to the list then reorganize elements in a random order.</summary>
		public static void Shuffle<T>(this List<T> l, IEnumerable<T> values) { l.AddRange(values); l.Shuffle(); }

		/// <summary>Get a random value from the collection</summary>
		public static T Random<T>(this List<T> v) => v[URand.Range(0, v.Count)];
		/// <summary>Get a random value from the collection</summary>
		public static T Random<T>(this T[] v) => v[URand.Range(0, v.Length)];
		/// <summary>Get a random value from the collection</summary>
		public static T RandomOrDefault<T>(this List<T> array) => array.Count == 0 ? default : array[URand.Range(0, array.Count)];
		/// <summary>Get a random value from the collection</summary>
		public static T RandomOrDefault<T>(this T[] array) => array.Length == 0 ? default : array[URand.Range(0, array.Length)];

		/// <summary>Get random values from the array. They are still ordered though.</summary>
		public static T[] TakeRand<T>(this List<T> array, int num) {
			var ret = new T[num];
			var curr = 0;
			for(var i = 0; i < array.Count; i++)
				if(URand.value <= (num - curr) / (float)(array.Count - i)) { ret[curr] = array[i]; curr++; }
			return ret;
		}
		/// <summary>Get random values from the array. They are still ordered though.</summary>
		public static T[] TakeRand<T>(this T[] array, int num) {
			var ret = new T[num];
			var curr = 0;
			for(var i = 0; i < array.Length; i++)
				if(URand.value <= (num - curr) / (float)(array.Length - i)) { ret[curr] = array[i]; curr++; }
			return ret;
		}

		/// <summary>Get a random value from the collection</summary>
		public static void Add<T>(this List<T> list, params T[] values) => list.AddRange(values);

		/// <summary>Return Default value if key does not exists.</summary>
		public static V Get<K, V>(this Dictionary<K, V> c, K key) => c.TryGetValue(key, out var v) ? v : default;

		/// <summary>Initialize with Default value if key does not exists.</summary>
		public static void Init<K, V>(this Dictionary<K, V> c, K key, V value = default) { if(!c.ContainsKey(key)) c[key] = value; }

		/// <summary>Show values on the console.</summary>
		public static string LogString<T, U>(this Dictionary<T, U> v) => v.Aggregate("", (s, p) => s += $"{p.Key}: {p.Value}\n");
		/// <summary>Show values on the console.</summary>
		public static void Log<T, U>(this Dictionary<T, U> v) => Debug.Log(LogString(v));

		/// <summary>Show values on the console.</summary>
		public static string LogString<T>(this IEnumerable<T> v, bool newLine = true) {
			var i = 0;
			return v.Aggregate("", (s, a) => s += $"{i++}: {a}{(newLine ? "\n" : ", ")}");
		}
		/// <summary>Show values on the console.</summary>
		public static string LogString<T>(this IEnumerable<T> v, Func<T, string> select, bool newLine = true) {
			var i = 0;
			return v.Aggregate("", (s, a) => s += $"{i++}: {select(a)}{(newLine ? "\n" : ", ")}");
		}

		/// <summary>Show values on the console.</summary>
		public static void Log<T>(this IEnumerable<T> v) => Debug.Log(LogString(v));
		/// <summary>Show values on the console.</summary>
		public static void Log<T>(this IEnumerable<T> v, Func<T, string> select) => Debug.Log(LogString(v));

		/// <summary>Only add item if equality fails on all current elements</summary>
		public static bool AddIfAll<T>(this List<T> list, T item, Func<T, T, bool> condition) {
			if(list.All(i => condition(i, item))) {
				list.Add(item);
				return true;
			}
			return false;
		}
		/// <summary>Uses object.Equals</summary>
		public static bool AddUnique<T>(this List<T> list, T item) => list.AddIfAll(item, (a, b) => !a.Equals(b));

		public static HashSet<T> Duplicates<T>(List<T> list, Func<T, T, bool> comparator) {
			var ret = new HashSet<T>();
			for(int i = 0; i < list.Count - 1; i++)
				for(int j = i + 1; j < list.Count; j++)
					if(comparator(list[i], list[j])) ret.Add(list[j]);
			return ret;
		}
		public static bool HasDuplicates<T>(List<T> list, Func<T, T, bool> comparator) {
			for(int i = 0; i < list.Count - 1; i++)
				for(int j = i + 1; j < list.Count; j++)
					if(comparator(list[i], list[j])) return true;
			return false;
		}
	}
}
