﻿using System;

namespace Drafts.Calendar {
	public static class Calendar {
		public static System.Globalization.Calendar calendar = System.Globalization.CultureInfo.InvariantCulture.Calendar;
		public static DateTime FirstDayOfFirstWeekOfMonth(this DateTime date) => date.FirstDayOfMonth().FirstDayOfWeek();
		public static DateTime FirstDayOfWeek(this DateTime date) => date.AddDays(-(int)date.DayOfWeek);
		public static DateTime FirstDayOfMonth(this DateTime date) => date.AddDays(1 - date.Day);
		public static DateTime LastDayOfLastWeekOfMonth(this DateTime date) => date.LastDayOfMonth().LastDayOfWeek();
		public static DateTime LastDayOfMonth(this DateTime date) => new DateTime(date.Year, date.Month, date.DaysInMonth());
		public static DateTime LastDayOfWeek(this DateTime date) => date.AddDays(6 - (int)date.DayOfWeek);
		public static int DaysInMonth(this DateTime date) => calendar.GetDaysInMonth(date.Year, date.Month);
		public static string ToSQL(this DateTime date) => date.ToString("u").Substring(0, 19);
	}
}