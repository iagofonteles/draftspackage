﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using static UnityEngine.Mathf;
using System.Collections;
using UObj = UnityEngine.Object;

namespace Drafts.Extensions {

	public static partial class ExtensionMethods {

		#region Template Helpers

		/// <summary>Find transform then get a specific component</summary>
		public static T Find<T>(this Transform t, string name) where T : Component => t?.Find(name)?.GetComponent<T>();
		/// <summary>Find transform then get a specific component recursively</summary>
		public static T FindInChildren<T>(this Transform t, string name) where T : Component => t.GetComponentsInChildren<T>().FirstOrDefault(c => c.name == name);

		/// <summary>Get the relative path from the transform to one of its child. Returns Null if the parameter is not a child of the transform.</summary>
		public static string PathTo(this Transform t, Transform child) {
			if(t == child) return "";
			var path = child.name;
			while(child.parent != null) {
				child = child.parent;
				if(child == t) return path;
				path = child.name + "/" + path;
			}
			return null;
		}

		/// <summary>GetComponents only on the first level of children. Exclude iself. Maybe Faster than GetComponentsInChildren.</summary>
		public static T[] GetComponentsImmediate<T>(this GameObject go, bool includeInactive = true) where T : Component => go.transform.GetComponentsImmediate<T>(includeInactive);
		/// <summary>GetComponents only on the first level of children. Exclude iself. Maybe Faster than GetComponentsInChildren.</summary>
		public static T[] GetComponentsImmediate<T>(this Component comp, bool includeInactive = true) where T : Component {
			var ret = new List<T>();
			for(int i = 0; i < comp.transform.childCount; i++) {
				if(comp.transform.GetChild(i).TryGetComponent<T>(out var n))
					if(includeInactive || n.gameObject.activeSelf) ret.Add(n);
			}
			return ret.ToArray();
		}

		/// <summary>Serach for the first children with that component.</summary>
		public static T FirstInactiveChild<T>(this Transform parent) where T : Component {
			for(int i = 0; i < parent.childCount; i++) {
				if(parent.GetChild(i).TryGetComponent<T>(out var n))
					if(!n.gameObject.activeSelf) return n;
			}
			return null;
		}

		/// <summary>Get all components of a type in the transform children and execute an action acording to its name.</summary>
		public static void SetTemplate<T>(this Transform t, params (string name, Action<T> action)[] settings) where T : Component {
			foreach(var o in t.GetComponentsInChildren<T>(true))
				foreach(var s in settings) if(o.name == s.name) s.action(o);
		}

		/// <summary>WARNING: this may be slow. Get all components of a type in the transform children and execute an action acording to its name.</summary>
		public static void SetTemplateALL(this Transform parent, params (string name, Action<object> action)[] settings) {
			foreach(var c in parent.GetComponentsInChildren<Component>(true))
				foreach(var s in settings) if(c.name == s.name) s.action(c);
		}

		#endregion

		/// <summary>Smooth rotate around Y axis until face targetAngle.</summary>
		public static void SmoothRotateY(this Transform t, float targetAngle, ref float currentSpeed, float smooth) {
			var angle = SmoothDampAngle(t.eulerAngles.y, targetAngle, ref currentSpeed, smooth);
			var rot = t.rotation.eulerAngles.Y(angle);
			t.rotation = Quaternion.Euler(rot);
		}
		/// <summary>Smooth rotate around Y axis until face targetAngle.</summary>
		public static void SmoothRotateY(this Transform t, float targetAngle, float speed) {
			var angle = MoveTowardsAngle(t.eulerAngles.y, targetAngle, speed);
			var rot = t.rotation.eulerAngles.Y(angle);
			t.rotation = Quaternion.Euler(rot);
		}
		/// <summary>Smooth rotate around Y axis until face direction.</summary>
		public static void SmoothFaceDirectionY(this Transform t, Vector3 direction, float speed) {
			var targetAngle = Atan2(direction.x, direction.z) * Rad2Deg;
			t.SmoothRotateY(targetAngle, speed);
		}
		/// <summary>Uses Vector3.MoveTowards and returns Near(a.position, b).</returns>
		public static bool ReachPoint(this Transform a, Vector3 b, float spd) {
			a.MoveTowards(b, spd);
			return Near(a.position, b);
		}
		/// <summary>Yield ReachPoint(a, b, spd * Time.fixedDeltaTime).</returns>
		public static IEnumerator WaitReachPointFixed(this Transform a, Vector3 b, float spd) {
			while(!ReachPoint(a, b, spd * Time.fixedDeltaTime))
				yield return new WaitForFixedUpdate();
		}
		/// <summary>Return the component wich the transform is nearest from [position] using Linq.</summary>
		public static T NearestFrom<T>(this IEnumerable<T> c, Vector3 position) where T : Component
			=> c.OrderBy(a => (a.transform.position - position).sqrMagnitude).FirstOrDefault();

		/// <summary>Align children with a grid. Change achors only.</summary>
		/// <param name="skipCount">Skip first n children.</param>
		public static void SortChildrenInGrid(this Transform rect, int width, int height, int skipCount = 0) {
			var t = rect.GetComponentsImmediate<RectTransform>().Skip(skipCount).ToArray();
			var space = new Vector2(1f / width, 1f / height);
			for(int i = 0; i < t.Length; i++) {
				var x = i % (float)width;
				var y = i / width;

				t[i].anchorMin = new Vector2(x * space.x, 1 - y * space.y - space.y);
				t[i].anchorMax = new Vector2(x * space.x + space.x, 1 - y * space.y);
			}
		}
		/// <summary>Align with a parent grid. Change achors only.</summary>
		public static void SortInParentGrid(this Transform rect, int width, int height, int position) {
			if(rect.TryGetComponent<RectTransform>(out var r)) r.SortInParentGrid(width, height, position);
		}
		/// <summary>Align with a parent grid. Change achors only.</summary>
		public static void SortInParentGrid(this RectTransform rect, int width, int height, int position) {
			var space = new Vector2(1f / width, 1f / height);
			var x = position % (float)width;
			var y = position / width;

			rect.anchorMin = new Vector2(x * space.x, 1 - y * space.y - space.y);
			rect.anchorMax = new Vector2(x * space.x + space.x, 1 - y * space.y);
		}

		/// <summary>Destroy all children of the transform.</summary>
		public static void DestroyChildren(this Transform t, int skip = 0) { for(int i = skip; i < t.childCount; i++) UObj.Destroy(t.GetChild(i).gameObject); }
		/// <summary>Destroy all children of the transform.</summary>
		public static void DeativateChildren(this Transform t, bool deactivate = true, int skip = 0) { for(int i = skip; i < t.childCount; i++) t.GetChild(i).gameObject.SetActive(!deactivate); }
		/// <summary>Destroy all children of the transform.</summary>
		public static void DestroyChildrenImmediate(this Transform t) { while(t.childCount > 0) UObj.DestroyImmediate(t.GetChild(0).gameObject); }

		/// <summary>Clamp transform position.</summary>
		public static void Restrain(this Transform t, Vector2 min, Vector2 max)
			=> t.position = new Vector3(Clamp(t.position.x, min.x, max.x), Clamp(t.position.y, min.y, max.y), t.position.z);
		/// <summary>Clamp transform position.</summary>
		public static void Restrain(this Transform t, Rect r) => Restrain(t, r.min, r.max);
		/// <summary>Clamp transform position.</summary>
		public static void Restrain(this Transform t, Bounds b) => Restrain(t, b.min, b.max);
		/// <summary>Clamp transform position.</summary>
		public static void Restrain(this Transform t, RectTransform r) {
			var c = new Vector3[4];
			r.GetWorldCorners(c);
			Restrain(t, new Vector2(c[0].x, c[0].y), new Vector2(c[2].x, c[2].y));
		}

		/// <summary>Activate the named sibling and deactivate the others.</summary>
		public static void SetActiveSibling(this Transform t, string name) => t.parent.SetActiveChild(name);
		/// <summary>Activate the named child and deactivate the others.</summary>
		public static void SetActiveChild(this Transform t, GameObject child) { foreach(Transform c in t) c.gameObject.SetActive(c.gameObject == child); }
		/// <summary>Activate the named child and deactivate the others.</summary>
		public static void SetActiveChild(this Transform t, Component child) { foreach(Transform c in t) c.gameObject.SetActive(c.gameObject == child.gameObject); }
		/// <summary>Activate the named child and deactivate the others.</summary>
		public static void SetActiveChild(this Transform t, string name) { foreach(Transform c in t) c.gameObject.SetActive(c.name == name); }
		/// <summary>Activate the next child and deactivate the previous.</summary>
		public static GameObject CicleChild(this Transform t, int v = 1) {
			for(var i = 0; i < t.childCount; i++)
				if(t.GetChild(i).gameObject.activeSelf) {
					t.GetChild(i).gameObject.SetActive(false);
					var next = t.GetChild((int)Repeat(i + v, t.childCount)).gameObject;
					next.SetActive(true);
					return next;
				}
			return null;
		}

		/// <summary>Move rect transform by anchors.</summary>
		public static void MoveAnchorsTo(this RectTransform r, Vector4 v, float speed) {
			r.anchorMin = Vector2.MoveTowards(r.anchorMin, new Vector2(v[0], v[1]), speed);
			r.anchorMax = Vector2.MoveTowards(r.anchorMax, new Vector2(v[2], v[3]), speed);
		}
		/// <summary>May need to be in the same parent to work properly</summary>
		public static void CopyPosition(this RectTransform t, RectTransform src) {
			t.anchorMin = src.anchorMin;
			t.anchorMax = src.anchorMax;
			t.anchoredPosition = src.anchoredPosition;
			t.sizeDelta = src.sizeDelta;
			t.pivot = src.pivot;
		}
		/// <summary>Scale only work in the same parent.</summary>
		public static void CopyPositionRaw(this RectTransform target, RectTransform source) {
			target.pivot = source.pivot;
			target.anchorMin = new Vector2(.5f, .5f);
			target.anchorMax = new Vector2(.5f, .5f);
			target.position = source.position;
			target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, source.sizeDelta.y);
			target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, source.sizeDelta.x);
			target.rotation = source.rotation;
			target.localScale = source.localScale;
		}
	}
}
