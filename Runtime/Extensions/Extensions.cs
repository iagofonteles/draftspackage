﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;
using UnityEngine.EventSystems;
using URand = UnityEngine.Random;
using System.Text.RegularExpressions;
using TMPro;

#if ENABLE_AI
using UnityEngine.AI;
#endif

//#pragma warning disable CS1591 // XML ausente para membro publico

namespace Drafts.Extensions {
	/// <summary>Some extensions methods.</summary>
	public static class ExtensionsMethods {

#if ENABLE_AI
		public static bool DestinationReached(this NavMeshAgent agent) => agent.remainingDistance <= agent.stoppingDistance;
#endif

		public static bool AddToPreloadedAssets(this ScriptableObject so) {
#if UNITY_EDITOR
			var preloaded = UnityEditor.PlayerSettings.GetPreloadedAssets().ToList();
			if(preloaded.Any(p => p.GetType() == so.GetType())) return false;
			preloaded.Add(so);
			UnityEditor.PlayerSettings.SetPreloadedAssets(preloaded.ToArray());
			return true;
#else
			return false;
#endif
		}

		// animator
		/// <summary>Get the length of current clip playing.</summary>
		public static float Duration(this Animator a, int layer = 0) => a.GetCurrentAnimatorStateInfo(layer).length;
		public static IEnumerator WaitAnimation(this Animator anim, string state, int layer = 0, float scale = 1) {
			anim.Play(state, layer);
			yield return new WaitForEndOfFrame();
			var time = scale * anim.Duration(layer);

			switch(anim.updateMode) {
				case AnimatorUpdateMode.Normal: yield return new WaitForSeconds(time); break;
				case AnimatorUpdateMode.AnimatePhysics: yield return new WaitForSeconds(time); break;
				case AnimatorUpdateMode.UnscaledTime: yield return new WaitForSecondsRealtime(time); break;
			}
		}

		/// <summary>Set alpha and toggle raycast on this group.</summary>
		public static void SetActive(this CanvasGroup group, bool b) { group.alpha = b.ToInt(); group.blocksRaycasts = b; }

		public static T GetOrAdd<T>(this Component c) where T : Component => c.TryGetComponent<T>(out var n) ? n : c.gameObject.AddComponent<T>();
		public static T GetOrAdd<T>(this GameObject c) where T : Component => c.TryGetComponent<T>(out var n) ? n : c.AddComponent<T>();

		// --- MonoBehaviour

		// EventTrigger
		public static void AddListener(this EventTrigger trigger, EventTriggerType type, Action<BaseEventData> callback) {
			var clbck = new EventTrigger.TriggerEvent();
			clbck.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(callback));
			trigger.triggers.Add(new EventTrigger.Entry() { eventID = type, callback = clbck });
		}

		// color
		/// <summary>Changing one or more parameters of a color.</summary>
		public static void Change(this ref Color c, float? r = null, float? g = null, float? b = null, float? a = null) { c.r = r ?? c.r; c.g = g ?? c.g; c.b = b ?? c.b; c.a = a ?? c.a; }
		/// <summary>Create new color changing one parameter.</summary>
		public static Color R(this Color c, float red) => new Color(red, c.g, c.b, c.a);
		/// <summary>Create new color changing one parameter.</summary>
		public static Color G(this Color c, float green) => new Color(c.r, green, c.b, c.a);
		/// <summary>Create new color changing one parameter.</summary>
		public static Color B(this Color c, float blue) => new Color(c.r, c.g, blue, c.a);
		/// <summary>Create new color changing one parameter.</summary>
		public static Color A(this Color c, float alpha) => new Color(c.r, c.g, c.b, alpha);

		// datetime
		/// <summary>Convert to MySQL format "yyyy-MM-dd HH:mm:ss".</summary>
		public static string SQL(this DateTime d) => "'" + d.ToString("yyyy-MM-dd HH:mm:ss") + "'";
		public static bool TimePassed(this DateTime d, int days = 0, int hours = 0, int years = 0, int months = 0)
			=> (d - DateTime.Now).TotalDays > (days + hours / 24.0 + years * 365 + months * 30.5);

		// optional serializable fields
		public static void TrySetText(this Text text, string value) { if(text) text.text = value; }
		public static void TrySetText(this Text text, object value) { if(text) text.text = value.ToString(); }
		public static void TrySetText(this TextMeshProUGUI text, string value) { if(text) text.text = value; }
		public static void TrySetText(this TextMeshProUGUI text, object value) { if(text) text.text = value.ToString(); }
		public static void TrySetSprite(this Image image, Sprite value) { if(image) image.sprite = value; }
		public static void TryEnable(this Behaviour behaviour, bool enabled) { if(behaviour) behaviour.enabled = enabled; }
		public static void TrySetActive(this GameObject go, bool active) { if(go) go.SetActive(active); }
		public static void TryPlay(this Animator anim, string stateName, int layer = 0) { if(anim && anim.isActiveAndEnabled) anim.Play(stateName, layer); }

		public static Texture2D Base64Texture2D(this string s) {
			var bytes = Convert.FromBase64String(s);
			var tex = new Texture2D(0, 0);
			tex.LoadImage(bytes);
			return tex;
		}
		public static Sprite Base64Sprite(this string s) {
			var tex = s.Base64Texture2D();
			return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one / 2);
		}
		public static string UrlGetParameter(this string url, string parameter) {
			if(string.IsNullOrEmpty(url)) return null;
			var p = UrlGetParameters(url);
			return p.First(s => s.name == parameter).value;
		}
		public static (string name, string value)[] UrlGetParameters(this string url) {
			try {
				var p = url.Split('?');
				if(p.Length < 2 || p[1].Length == 0) return null;
				p = p[1].Split('&');
				var ret = new (string, string)[p.Length];

				for(int i = 0; i < p.Length; i++) {
					var s = p[i].Split('=');
					ret[i] = (s[0], UnityEngine.Networking.UnityWebRequest.UnEscapeURL(s[1]));
				}
				return ret;
			} catch(Exception e) {
				Debug.LogError(e.Message);
				return null;
			}
		}

		/// <summary>Like Substring, but no error when count passes lenght.</summary>
		public static string SubstringCap(this string s, int first, int count) {
			if(string.IsNullOrEmpty(s)) return "";
			first = Mathf.Min(first, s.Length - 1);
			return first + count > s.Length ? s.Substring(first) : s.Substring(first, count);
		}

		public static bool IsValidVariableName(this string s) => !Regex.IsMatch(s, "\\W");

		/// <summary>Double Split string forming a matrix. The first char will be used to break lines and the second to break colums.</summary>
		public static string[][] Matrix(this string str) {
			var row_sep = str[0];
			var colum_sep = str[1];
			str = str.Substring(2);

			var rows = str.Split(row_sep);
			string[][] ret = new string[rows.Length][];
			for(int i = 0; i < rows.Length; i++)
				ret[i] = rows[i].Split(colum_sep);

			return ret;
		}
		/// <summary>Double Split string forming a matrix. The first char will be used to break lines and the second to break colums.</summary>
		public static string[,] Matrix2(this string str) {
			var row_sep = str[0];
			var colum_sep = str[1];
			str = str.Substring(2);

			var rows = str.Split(row_sep);
			var c = rows[0].Split(colum_sep);

			string[,] ret = new string[rows.Length, c.Length];
			for(int i = 0; i < rows.Length; i++) {
				var values = rows[i].Split(colum_sep);
				for(int j = 0; j < c.Length; j++)
					ret[i, j] = values[j];
			}
			return ret;
		}

		/// <summary>Parse or default</summary>
		public static int EnumID<T>(this string s, bool ignoreCase = true) => (int)Enum.Parse(typeof(T), s, ignoreCase);
		/// <summary>Parse or default</summary>
		public static T ToEnum<T>(this string s, bool ignoreCase = true) => (T)Enum.Parse(typeof(T), s, ignoreCase);

		// === ARRAY

		/// <summary>Compare elements on the same position.</summary>
		public static bool ArrayEquals<T>(this T[] a, T[] b) {
			if(a.Length != b.Length) return false;
			for(int i = 0; i < a.Length; i++)
				if(!a[i].Equals(b[i])) return false;
			return true;
		}

		public static bool InBounds<T>(this T[] array, int x) => x >= 0 && x < array.Length;
		public static bool InBounds<T>(this T[,] array, Vector2Int v) => v.x >= 0 && v.y >= 0 && v.x < array.GetLength(0) && v.y < array.GetLength(1);
		public static bool InBounds<T>(this T[,] array, int x, int y) => x >= 0 && y >= 0 && x < array.GetLength(0) && y < array.GetLength(1);

		public static int MaxIndex<T>(this T[] array) => Array.IndexOf(array, array.Max());

		public static bool IsNullOrEmpty<T>(this T[] array) => array == null || array.Length == 0;

		// === ENUMERATOR

		/// <summary>Intend to use with enumerators.</summary>
		public static int EnumRandom<T>() => URand.Range(0, Enum.GetValues(typeof(T)).Length);

		// === IEnumerable

		/// <summary>Same as foreach.</summary>
		public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action) { foreach(var v in ie) action(v); }

		/// <summary>The result of all element multiplied.</summary>
		public static int Multiply(this IEnumerable<int> ie) { var r = 1; foreach(var i in ie) r *= i; return r; }

		/// <summary>The result of all element multiplied.</summary>
		public static float Multiply(this IEnumerable<float> ie) { var r = 1f; foreach(var i in ie) r *= i; return r; }

		/// <summary>Returns a List with the indexes of entries that match the condition.</summary>
		public static List<int> IndexesOf<T>(this IEnumerable<T> array, Predicate<T> predicate) {
			var r = new List<int>(); var i = 0;
			foreach(var a in array) { if(predicate(a)) r.Add(i); i++; }
			return r;
		}
		/// <summary>Returns the index of element.</summary>
		public static int IndexOf<T>(this T[] array, T element) => Array.IndexOf(array, element);
		/// <summary>Returns the index of element.</summary>
		[Obsolete("redundant", true)]
		public static int IndexOf<T>(this IEnumerable<T> array, T element) {
			var i = 0;
			foreach(var a in array) { if(a.Equals(element)) return i; i++; }
			return -1;
		}

		/// <summary>Return the index from sortedValues where value fits >=.</summary>
		public static int RankOf(this IEnumerable<int> sortedValues, int value)
			=> sortedValues.Where(v => value >= v).Count();
		/// <summary>Return the index from sortedValues where value fits >=.</summary>
		public static int RankOf(this IEnumerable<float> sortedValues, float value)
			=> sortedValues.Where(v => value >= v).Count();
		/// <summary>Return the index from sortedValues where value fits >=.</summary>
		public static int RankOf<T>(this IEnumerable<T> sortedValues, float value, Func<T, int> getValue)
			=> sortedValues.Where(v => value >= getValue(v)).Count();

		/// <summary></summary>
		public static int WeightedRandom(this IEnumerable<int> weights) => WeightedIndex(weights, URand.Range(0, weights.Sum()));
		/// <summary>Return the index of . value is between 0 and sum of weights.</summary>
		public static int WeightedIndex(this IEnumerable<int> weights, int value) {
			var sum = 0;
			return weights.Where(v => value > (sum += v)).Count();
		}
		/// <summary></summary>
		public static int WeightedRandom(this IEnumerable<float> weights) => WeightedIndex(weights, URand.Range(0, weights.Sum()));
		/// <summary>Return the index of </summary>
		public static int WeightedIndex(this IEnumerable<float> weights, float value) {
			var sum = 0f;
			return weights.Where(v => value > (sum += v)).Count();
		}
		/// <summary></summary>
		public static int WeightedRandom<T>(this IEnumerable<T> itens, Func<T, float> getWeight) => WeightedIndex(itens, getWeight, URand.Range(0, itens.Sum(getWeight)));
		/// <summary>Return the index of </summary>
		public static int WeightedIndex<T>(this IEnumerable<T> itens, Func<T, float> getWeight, float value) {
			var sum = 0f;
			return itens.Where(v => value > (sum += getWeight(v))).Count();
		}
	}
}