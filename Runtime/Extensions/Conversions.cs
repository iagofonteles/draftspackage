﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Drafts.Extensions {

	public static partial class ExtensionMethods {

		// float
		/// <summary>Returns x0%.</summary>
		public static string Percent(this float f, int decimals = 0) {
			NumberFormatInfo info = new NumberFormatInfo();
			info.PercentDecimalDigits = decimals;
			return f.ToString("P", info);
		}

		/// <summary>Cast seconds to x00:00 format.</summary>
		public static string ClockTime(this float f) {
			var secs = (f % 60).ToString("00");
			var mins = (f / 60).ToString("00");
			return $"{mins}:{secs}";
		}

		// === bool value
		public static int ToInt(this bool b) => b ? 1 : 0;
		public static float ToFloat(this bool b) => b ? 1f : 0f;
		public static bool ToBool(this int b) => b == 0 ? false : true;
		public static bool ToBool(this float b) => b == 0 ? false : true;

		// string
		public static string SplitCamelCase(this string s) {
			if(string.IsNullOrEmpty(s)) return s;
			string camelCase = Regex.Replace(Regex.Replace(s, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"), @"(\p{Ll})(\P{Ll})", "$1 $2");
			return camelCase.Insert(1, camelCase.Substring(0, 1).ToUpper()).Substring(1);
		}
		public static string CamelToSnakeCase(this string s) {
			if(string.IsNullOrEmpty(s)) return s;
			string snakeCase = Regex.Replace(Regex.Replace(s, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1_$2"), @"(\p{Ll})(\P{Ll})", "$1_$2");
			return snakeCase.ToLower();
		}

		public static DateTime ToDateTime(this string s) => DateTime.TryParse(s, out var v) ? v : DateTime.MinValue;
		public static TimeSpan ToTimeSpan(this string s) => TimeSpan.TryParse(s, out var v) ? v : new TimeSpan();

		public static T Parse<T>(this string value) where T : Enum => (T)Enum.Parse(typeof(T), value);
		public static bool TryParse<T>(this string value, out T result) where T : Enum {
			try { result = Parse<T>(value); return true; } catch { result = default; return false; }
		}
	}
}

