﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Drafts.Mission {

	/// <summary>Easily Manage your missions with this class. Can be serialized to save and load.</summary>
	[Serializable]
	public abstract class Mission {
		/// <summary>Wether the reward was given.</summary>
		public bool Rewarded { get; private set; } = false;
		/// <summary>Wether the requeriment was met.</summary>
		public bool Completed => Progress == 1;
		/// <summary>Display name.</summary>
		[SerializeField] public string Name { get; set; }
		/// <summary>Detailed description of the mission.</summary>
		public string Briefing { get; set; }
		/// <summary>Mission objectives.</summary>
		public List<Objective> Objectives { get; protected set; } = new List<Objective>();
		/// <summary>Percentage of the overall mission progress.</summary>
		public float Progress { get; private set; } = 0;
		/// <summary>Progress of all objectives includind optional ones.</summary>
		public float ProgressOptional => Objectives.Sum(o => o.Progress * o.Weight) / Objectives.Sum(o => o.Weight);
		/// <summary>True if any non optional objective has failed.</summary>
		public bool Failed => Objectives.Any(o => !o.Optional & o.Failed);

		[NonSerialized] private bool enabled = false;
		public bool Enabled { get => enabled & !Completed; set => enabled = value & !Completed; }
		/// <summary>Enable mission and register its objectives. Use this after the creation to really start counting progress.</summary>
		public void Enable() { Enabled = true; RegisterAll(); }
		/// <summary>Mark the mission as completed without triggering rewards. Useful when loading saves.</summary>
		public void MarkAsDone() { Progress = 1; Rewarded = true; Enabled = false; RegisterAll(false); }

		/// <summary>Display percentage of the overall mission.</summary>
		public virtual string DisplayProgress => string.Format("{0}%", (int)(Progress * 100));
		/// <summary>Display percentage of the overall mission.</summary>
		public virtual string DisplayCounter => string.Format("({0}/{1})", Objectives.Count(o => !o.Optional & o.Completed), Objectives.Count(o => !o.Optional));
		/// <summary>Display all objectives and their counters as a multiline colored text.</summary>
		public virtual string DisplayObjectives => Objectives.Aggregate("", (s, o) => $"{s}\n>> {o.Display}").Substring(1);

		internal void UpdateProgress() {
			var _last = Completed;
			Progress = Objectives.Sum(o => o.Optional ? 0 : o.Progress * o.Weight) / Objectives.Sum(o => o.Optional ? 0 : o.Weight);
			if (_last != Completed) OnGoalMet(Completed);
		}

		/// <summary>Execute Reward and disable if goals are met. Fails if already completed.</summary>
		public bool GetRewards() {
			if (Rewarded || !Completed)
				return false;
			if (Reward()) {
				Rewarded = true;
				Enabled = false;
				RegisterAll(false);
				return true;
			}
			return false;
		}

		public void Reset() { Objectives.ForEach(o => o.Current = 0); Rewarded = false; Progress = 0; }

		/// <summary></summary>
		public Mission AddObjective(params Objective[] objectives) {
			foreach (var o in objectives) o.Mission = this;
			Objectives.AddRange(objectives);
			UpdateProgress();
			return this;
		}

		protected virtual bool Reward() => true;
		/// <summary>Executes when all objective are completed.</summary>
		protected virtual void OnGoalMet(bool goalMet) { }
		public void Update() => Objectives.ForEach(o => { if (o.Enabled) o.Update(); });
		/// <summary>Trigger Register on all objectives.</summary>
		protected void RegisterAll(bool register = true) => Objectives.ForEach(o=>o.OnEnable(register));

		public Mission(params Objective[] objectives) : this("", "", objectives) { }
		public Mission(string name, params Objective[] objectives) : this(name, "", objectives) { }
		public Mission(string name, string briefing, params Objective[] objectives) {
			Name = name;
			Briefing = briefing;
			AddObjective(objectives);
		}

		public static class Status { public static int Failed = 0, Disabled = 1, Enabled = 2, Completed = 3; }
	}
}
