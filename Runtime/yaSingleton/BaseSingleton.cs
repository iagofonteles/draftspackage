﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Drafts.Extensions;

namespace Drafts.yaSingleton.Internal {
	/// <summary>
	/// Base class for singletons. Contains method stubs and the Create method. Use this to create custom Singleton flavors.
	/// If you're looking to create a singleton, inherit Singleton or LazySingleton.
	/// </summary>
	[ExecuteInEditMode]
	public abstract class BaseSingleton : ScriptableObject, IComparable<BaseSingleton> {
		protected static SingletonUpdater Updater => SingletonUpdater.Instance;
		protected virtual int LoadPriority => 0;

		#region UnityEvents
		protected virtual void Initialize() { }
		protected virtual void Deinitialize() { }
		public virtual void OnFixedUpdate() { }
		public virtual void OnUpdate() { }
		public virtual void OnLateUpdate() { }
		public virtual void OnApplicationFocus(bool hasFocus) { }
		public virtual void OnApplicationPause(bool pauseStatus) { }
		public virtual void OnApplicationQuit() { }
		public virtual void OnDrawGizmos() { }
		public virtual void OnPostRender() { }
		public virtual void OnPreCull() { }
		public virtual void OnPreRender() { }
		#endregion

		#region Coroutines
		/// <summary>Starts a coroutine.</summary>
		public static Coroutine StartCoroutine(IEnumerator routine) => Updater.StartCoroutine(routine);
		/// <summary>Stops the first coroutine named methodName, or the coroutine stored in routine running on this behaviour.</summary>
		public static void StopCoroutine(Coroutine routine) => Updater.StopCoroutine(routine);
		/// <summary>Stops all coroutines running on this behaviour.</summary>
		public static void StopAllCoroutines() => Updater.StopAllCoroutines();
		#endregion

		public static readonly List<BaseSingleton> AllSingletons = new List<BaseSingleton>();
		public static void Store(BaseSingleton[] _new) { foreach(var s in _new) Store(s); }
		public static void Store(BaseSingleton _new) => AllSingletons.AddIfAll(_new, (a, b) => a.GetType() != b.GetType());

		protected static T FindInstance<T>() where T : BaseSingleton {
			var instance = AllSingletons.FirstOrDefault(s => s.GetType() == typeof(T)) as T;
			if(instance) Debug.LogError("Singleton of " + typeof(T).Name + "does not exist");
			return instance;
		}

		protected virtual void Subscribe() {
			Updater.DestroyEvent += Deinitialize;

			Updater.FixedUpdateEvent += OnFixedUpdate;
			Updater.UpdateEvent += OnUpdate;
			Updater.LateUpdateEvent += OnLateUpdate;

			Updater.ApplicationFocusEvent += OnApplicationFocus;
			Updater.ApplicationPauseEvent += OnApplicationPause;
			Updater.ApplicationQuitEvent += OnApplicationQuit;

			Updater.DrawGizmosEvent += OnDrawGizmos;
			Updater.PostRenderEvent += OnPostRender;
			Updater.PreCullEvent += OnPreCull;
			Updater.PreRenderEvent += OnPreRender;
		}
		protected virtual void AssignInstance() { }

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void InitializeSingletons() {
#if !UNITY_EDITOR
			AllSingletons.Clear();
			AllSingletons.AddRange(Resources.FindObjectsOfTypeAll<BaseSingleton>().ToList());
			AllSingletons.Sort();
			Debug.Log($"{AllSingletons.Count} singletons Found.");
#endif
			if(AllSingletons.Count == 0) return;
			SingletonUpdater.Instance = new GameObject("Singleton Updater").AddComponent<SingletonUpdater>();
			DontDestroyOnLoad(SingletonUpdater.Instance.gameObject);
			AllSingletons.ForEach(s => { s.AssignInstance(); s.Subscribe(); s.Initialize(); });
		}

		private void OnEnable() => this.AddToPreloadedAssets();

		public int CompareTo(BaseSingleton b) => b.LoadPriority.CompareTo(LoadPriority);
	}
}