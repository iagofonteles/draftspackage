﻿using System;
using UnityEngine;
using Drafts.yaSingleton.Internal;

namespace Drafts.Patterns {
	/// <summary>
	/// Singleton class. It'll be initialized before the Awake method of all other MonoBehaviours.
	/// Inherit by passing the inherited type (e.g. class GameManager : Singleton&lt;GameManager&gt;)
	/// </summary>
	/// <typeparam name="TSingleton">The Inherited Singleton's Type</typeparam>
	[Serializable, ExecuteInEditMode]
	public abstract class ScriptableSingleton<TSingleton> : BaseSingleton where TSingleton : BaseSingleton {
		private void OnEnable() { Store(this); Instance = this as TSingleton; }
#if UNITY_EDITOR
		static TSingleton _instance;
		public static TSingleton Instance {
			get => _instance ? _instance : _instance = FindInstance<TSingleton>();
			set => _instance = value;
		}
#else
		public static TSingleton Instance { get; private set; }
#endif
		protected override void AssignInstance() => Instance = (TSingleton)(BaseSingleton)this;
	}

	/// <summary>
	/// Singleton class. It'll be lazy-initialized when first accessed.
	/// Inherit by passing the inherited type (e.g. class GameManager : LazySingleton&lt;GameManager&gt;)
	/// </summary>
	/// <typeparam name="TSingleton">The Inherited Singleton's Type</typeparam>
	public abstract class LazyScriptableSingleton<TSingleton> : BaseSingleton where TSingleton : BaseSingleton {
		public static TSingleton Instance => Initializer<TSingleton>.LazyInstance;

		// ReSharper disable once ClassNeverInstantiated.Local
		private class Initializer<T> where T : BaseSingleton {
			static Initializer() { LazyInstance = FindInstance<T>(); }
			internal static readonly T LazyInstance;
		}
	}
}