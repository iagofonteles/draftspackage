using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Drafts.yaSingleton.Internal {
    /// <inheritdoc />
    /// <summary>ScriptableObject that automagically adds itself to Unity's preloaded assets.</summary>
    public abstract class PreloadedScriptableObject : ScriptableObject {

#if UNITY_EDITOR
        [InitializeOnLoadMethod] private static void LoadPreloadedAssetsInEditor() => PlayerSettings.GetPreloadedAssets();
#endif

		protected virtual void OnEnable() {
#if UNITY_EDITOR
            if(EditorApplication.isPlayingOrWillChangePlaymode) return;
#endif
            this.AddToPreloadedAssets();
        }

        protected virtual void OnDisable() {
#if UNITY_EDITOR
            if(EditorApplication.isPlayingOrWillChangePlaymode) return;
#endif
            ScriptableObjectExtensions.RemoveEmptyPreloadedAssets();
        }
    }

    public static class ScriptableObjectExtensions {
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void AddToPreloadedAssets(this ScriptableObject scriptableObject) {
#if UNITY_EDITOR
            var preloadedAssets = PlayerSettings.GetPreloadedAssets().ToList();
            
            if(preloadedAssets.Any(preloadedAsset => preloadedAsset
            && preloadedAsset.GetInstanceID() == scriptableObject.GetInstanceID()))
                // Already being preloaded
                return; 
            
            preloadedAssets.Add(scriptableObject);
            PlayerSettings.SetPreloadedAssets(preloadedAssets.ToArray());
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void RemoveEmptyPreloadedAssets() {
#if UNITY_EDITOR
            var preloadedAssets = PlayerSettings.GetPreloadedAssets().ToList();
            var nonEmptyPreloadedAssets = preloadedAssets.Where(asset => asset).ToArray();
            PlayerSettings.SetPreloadedAssets(nonEmptyPreloadedAssets);
#endif
        }
    }
}