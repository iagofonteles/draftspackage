﻿using UnityEngine;
using System;

namespace Drafts.yaSingleton.Internal {
    /// <summary>
    /// Singleton updater class. Instantiates a single MonoBehaviour and uses it to send Unity's events to all singletons. Has a sexy editor.
    /// </summary>
    public class SingletonUpdater : MonoBehaviour {
        public static SingletonUpdater Instance { get; internal set; }

        //static SingletonUpdater() {
        //    Instance = new GameObject("Singleton Updater").AddComponent<SingletonUpdater>();
        //    DontDestroyOnLoad(Instance.gameObject);
        //    Debug.Log("UPDATER");
        //}

        #region Events
        public event Action StartEvent = () => { };
        public event Action EnableEvent = () => { };
        public event Action DisableEvent = () => { };
        public event Action DestroyEvent = () => { };

        public event Action FixedUpdateEvent = () => { };
        public event Action UpdateEvent = () => { };
        public event Action LateUpdateEvent = () => { };

        public event Action<bool> ApplicationFocusEvent = b => { };
        public event Action<bool> ApplicationPauseEvent = b => { };
        public event Action ApplicationQuitEvent = () => { };

        public event Action DrawGizmosEvent = () => { };
        public event Action GUIEvent = () => { };
        public event Action PostRenderEvent = () => { };
        public event Action PreCullEvent = () => { };
        public event Action PreRenderEvent = () => { };

#pragma warning disable IDE0051 // Remove unused private members
        private void Start() => StartEvent();
        private void OnEnable() => EnableEvent();
        private void OnDisable() => DisableEvent();
        private void OnDestroy() => DestroyEvent();

		private void FixedUpdate() => FixedUpdateEvent();
		private void Update() => UpdateEvent();
        private void LateUpdate() => LateUpdateEvent();

        private void OnApplicationFocus(bool hasFocus) => ApplicationFocusEvent(hasFocus);
        private void OnApplicationPause(bool pauseStatus) => ApplicationPauseEvent(pauseStatus);
        private void OnApplicationQuit() => ApplicationQuitEvent();

        private void OnDrawGizmos() => DrawGizmosEvent();
        private void OnGUI() => GUIEvent();
        private void OnPostRender() => PostRenderEvent();
        private void OnPreCull() => PreCullEvent();
        private void OnPreRender() => PreRenderEvent();
#pragma warning restore IDE0051 // Remove unused private members
        #endregion
    }
}