using Drafts.Patterns;
using UnityEngine;

namespace Drafts.Avatar {

	public class CustomAvatar : MonoBehaviour {
		[Header("Avatar Parts")]
		[SerializeField] GameObject[] parts = default;
		public AvatarSet preset;

		public void ChangeTo(AvatarSet set) {
			for(int i = 0; i < set.parts.Length; i++)
				set.parts[i].SwitchToThis(parts[i]);
		}
	}

	public class AvatarPieceDatabase : ScriptableSingleton<AvatarPieceDatabase> {

	}
}

