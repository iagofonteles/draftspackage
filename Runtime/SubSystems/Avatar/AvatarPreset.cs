﻿using System;
using UnityEngine;

namespace Drafts.Avatar {
	[CreateAssetMenu(menuName = "Drafts/Avatar Preset")]
	public class AvatarPreset : ScriptableObject {
		public AvatarSet avatarSet;
		public static implicit operator AvatarSet(AvatarPreset npc) => npc.avatarSet;
	}

	[Serializable]
	public class AvatarSet {
		public int Length => parts.Length;
		public AvatarPart[] parts;
	}
}

