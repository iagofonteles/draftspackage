﻿using UnityEngine;
using UnityEngine.UI;

namespace Drafts.Avatar {
	[CreateAssetMenu(menuName = "Drafts/Avatar Part")]
	public class AvatarPart : ScriptableObject {
		[SerializeField] int partId = default;
		[SerializeField] string alias = default;
		[SerializeField] Material material = default;
		[SerializeField] Texture2D texture = default;
		[SerializeField] Color color = default;
		[SerializeField] Mesh mesh = default;
		[SerializeField] Sprite sprite = default;

		public int PartId => partId;
		public string Alias => alias;
		public Material Material => material;
		public Texture2D Texture => texture;
		public Color Color => color;
		public Mesh Mesh => mesh;
		public Sprite Sprite => sprite;

		/// <summary>Change Material, Texture and Color of the attached Renderer or Graphic component.</summary>
		public void SwitchToThis(GameObject gameObject) {
			if(gameObject.TryGetComponent<Renderer>(out var renderer)) {
				if(material) renderer.material = new Material(material);
				if(texture) renderer.material.mainTexture = texture;
				renderer.material.color = color;
			}
			if(gameObject.TryGetComponent<Graphic>(out var graphic)) {
				if(material) graphic.material = new Material(material);
				if(texture) graphic.material.mainTexture = texture;
				graphic.material.color = color;
			}
			if(gameObject.TryGetComponent<MeshFilter>(out var filter))
				if(mesh) filter.mesh = mesh;
		}
	}
}

