﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;
using Drafts.Extensions;
using System.Collections.Specialized;
using System.Linq;
using Drafts.Patterns;
using Drafts.Translations;

namespace Drafts {

	public static class WRExtensions {
		/// <summary>length must be even.</summary>
		public static WRForm ToWRForm(this object[] s) {
			var r = new WRForm();
			for(int i = 0; i + 1 < s.Length; i += 2)
				r.Add(s[i].ToString(), s[i + 1]?.ToString());
			return r;
		}
	}

	/// <summary>Alternative to UnityWebRequest. High flexibility and code friendly. Option to assign Logs and Popups on connection failed.<br />
	/// Call Initialize() to configure, remember to use DontDestroyOnLoad on the GameObject that will start Coroutines.</summary>
	[CreateAssetMenu(menuName = "Drafts/Systems/Web Request", fileName = "Config WebRequest")]
	public class WebRequest : ScriptableSingleton<WebRequest> {
		protected override int LoadPriority => 100;

		protected override void Initialize() {
			requestHistory.Clear();
			translation = new Texts();
		}

		[Serializable]
		internal class Texts : TranslatedStrings {
			public TranslatedString no_connection = ("drafts", "webrequest_no_connection", "Verifique se seu aparelho tem acesso á internet.");
			public TranslatedString timeout = ("drafts", "webrequest_timeout", "Tempo limite excedido.");
			public TranslatedString verify_connection = ("drafts", "webrequest_verify_connection", "Erro de conexão, verifique sua internet.");
		}
		internal Texts translation;

		public bool Validate() => translation.Validate();

		/// <summary>Prompts when connection failed. Basically WaitWhile, WaitUntil or MyEngine.UserInput methods.</summary>
		public static Func<CustomYieldInstruction> RetryAsk = null;//() => UserInput.Choice01("Connection Failed.\nRetry?");
		/// <summary>After yield RetryAsk, this should hold the answer.</summary>
		public static Func<bool> RetryResult = null; //() => UserInput.accepted;
		/// <summary></summary>
		public UnityEventString LogError;
		/// <summary>Errors will show queries and page errors. Most for debug, not user friendly.</summary>
		public bool detailedLogs = false;
		/// <summary>Default time limit for requests</summary>
		public int DefaultTimeout = 10;

		[ReadOnly, TextArea(3, 50)] public string lastRequestResponse;
		public List<WRequest> requestHistory = new List<WRequest>();

		/// <summary>Set custom actions for when a reqest fails.</summary>
		public static void SetRetryQuestion(Func<CustomYieldInstruction> retryAsk, Func<bool> retryResult) { RetryAsk = retryAsk; RetryResult = retryResult; }

		public enum Method { GET, POST, PUT, DELETE }
		public static WRequest Get(string url, Dictionary<string, string> headers = null, Action<WRequest> callback = null) => new WRequest(url, null, headers, callback, Method.GET);
		public static WRequest Post(string url, Dictionary<string, string> form, Dictionary<string, string> headers = null, Action<WRequest> callback = null) => new WRequest(url, form, headers, callback, Method.POST);
		public static WRequest Put(string url, Dictionary<string, string> form, Dictionary<string, string> headers = null, Action<WRequest> callback = null) => new WRequest(url, form, headers, callback, Method.PUT);
		public static WRequest Delete(string url, Dictionary<string, string> headers = null, Action<WRequest> callback = null) => new WRequest(url, null, headers, callback, Method.DELETE);
	}

	/// <summary>Web Request Form</summary>
	public class WRForm : Dictionary<string, string> {
		public WRForm() { }
		public WRForm(params object[] obj) {
			for(int i = 0; i + 1 < obj.Length; i += 2)
				Add(obj[i].ToString(), obj[i + 1]?.ToString());
		}
		public void Add(string key, object value) => base.Add(key, value.ToString());
		public static implicit operator WRForm(object[] obj) => new WRForm(obj);
	}

	/// <summary>Returned type from Queries and Page functions. Yield return request.Send() to wait until it is finished,
	/// then check request.Suceed and request.Results. See Data Fetch region for more option on reading returned data.</summary>
	[Serializable]
	public class WRequest {

		[SerializeField, ReadOnly, TextArea(1, 3)] string url;
		[SerializeField, ReadOnly] WebRequest.Method method;
		[SerializeField, ReadOnly, TextArea(1, 20)] string _headers;
		[SerializeField, ReadOnly, TextArea(1, 20)] string _form;
		[SerializeField, ReadOnly] string formString;
		[SerializeField, ReadOnly] bool suceed = false;
		[SerializeField, ReadOnly] string response;
		[SerializeField, ReadOnly] string error;
		[ReadOnly] public bool silent = false;

		Dictionary<string, string> headers;
		Dictionary<string, string> form;

		public bool Suceed { get => suceed; private set => suceed = value; }
		public string Error => error;
		public bool IsDone => Request.isDone;
		public Action<WRequest> callback; // Executed after a sucessfully Send the request.

		public UnityWebRequest Request { get; private set; }
		static WebRequest.Texts translation => WebRequest.Instance.translation;
		/// <summary>Errors ocurred during the web request, mainly conection issues. (Check WebRequest.detailedLog option)</summary>

		public WRequest(string url, Dictionary<string, string> form, Dictionary<string, string> headers,
			Action<WRequest> callback, WebRequest.Method method) {

			this.url = url;
			this.form = form;
			this.headers = headers ?? new Dictionary<string, string>();
			this.callback = callback;
			this.method = method;

			var parse = new NameValueCollection(); //HttpUtility.ParseQueryString("");
			foreach(var p in form) parse.Add(p.Key, p.Value);
			formString = parse.ToString();

			// debug
			if(headers.Count > 0) _headers = headers.Aggregate("", (s, h) => $"{s}\n{h.Key}: {h.Value}").Substring(1);
			if(form.Count > 0) _form = form.Aggregate("", (s, h) => $"{s}\n{h.Key}: {h.Value}").Substring(1);

			WebRequest.Instance.requestHistory.Insert(0, this);
		}

		#region Data Fetch
		/// <summary>Web script result as string.</summary>
		public string Text => Request.downloadHandler.text;
		/// <summary>Web script binary result.</summary>
		public byte[] Data => Request.downloadHandler.data;
		/// <summary>Web script parsed as json.</summary>
		public T JsonParse<T>() {
			if(string.IsNullOrEmpty(Text)) return default;
			try {
				return JsonUtility.FromJson<T>(Text);
			} catch {
				try {
					var str = System.Text.Encoding.UTF8.GetString(Data, 3, Data.Length - 3);
					if(string.IsNullOrEmpty(str)) return default;
					return JsonUtility.FromJson<T>(str);
				} catch {
					Debug.LogWarning("Invalid Json of type " + typeof(T) + ": " + url + "\nText: " + Text);
					return default;
				}
			}
		}
		public void JsonOverwrite<T>(T obj) {
			if(string.IsNullOrEmpty(Text)) return;
			try {
				JsonUtility.FromJsonOverwrite(Text, obj);
			} catch {
				try {
					var str = System.Text.Encoding.UTF8.GetString(Data, 3, Data.Length - 3);
					JsonUtility.FromJsonOverwrite(str, obj);
				} catch {
					Debug.LogWarning("Invalid Json of type " + typeof(T) + ": " + url + "\nText: " + Text);
				}
			}
		}

		/// <summary>Use this when downloading images as binary data.</summary>
		public Texture2D Texture2D { get { var t = new Texture2D(0, 0); t.LoadImage(Data); return t; } }
		/// <summary>Use this when downloading images as binary data.</summary>
		public Sprite Sprite {
			get {
				var tex = Texture2D;
				return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one / 2);
			}
		}
		public Texture2D Base64Texture2D => Text.Base64Texture2D();
		public Sprite Base64Sprite => Text.Base64Sprite();
		#endregion

		/// <summary>Yield this tho wait request completion. Even if forceRetry is true, you still need to check request.Suceed.</summary>
		/// <param name="forceRetry">Automatically retry on Network fail. Will not retry if the issue is in page script.</param>
		/// <param name="timeout">Time limit for the request.</param>
		public Coroutine Send(bool forceRetry = true, int timeout = 0) {
			timeout = timeout == 0 ? WebRequest.Instance.DefaultTimeout : timeout;
			switch(method) {
				case WebRequest.Method.GET: Request = UnityWebRequest.Get(url); break;
				case WebRequest.Method.POST: Request = UnityWebRequest.Post(url, form); break;
				case WebRequest.Method.PUT: Request = UnityWebRequest.Put(url, formString); break;
				case WebRequest.Method.DELETE: Request = UnityWebRequest.Delete(url); break;
			}
			Request.timeout = timeout;
			if(headers != null)
				foreach(var p in headers)
					Request.SetRequestHeader(p.Key, p.Value ?? "");
			error = "";

			return WebRequest.StartCoroutine(_Send(forceRetry, timeout));
		}

		protected virtual IEnumerator _Send(bool forceRetry, int timeout) {
			yield return Request.SendWebRequest();
#if UNITY_2020
			if(Request.result == UnityWebRequest.Result.ConnectionError)
#else
			if(Request.isNetworkError)
#endif
				switch(Request.error) {
					case "Cannot resolve destination host":
						error += translation.no_connection + "\n"; break;
					case "Request timeout":
						error += translation.timeout + "\n"; break;
					default: error += translation.verify_connection + "\n" + (WebRequest.Instance.detailedLogs || Application.isEditor ? Request.error + '\n' : ""); break;
				}
#if UNITY_2020
			if(Request.result == UnityWebRequest.Result.ProtocolError) {
#else
			if(Request.isHttpError) {
#endif
				var detail = string.Format("<b>Post Data:</b> {0}\n{1}\n<b>Response:</b> {2}\n{3}\n", formString, form.LogString(), Request.error, Request.downloadHandler.text);
				error += string.Format("Erro na Página {0}\n{1}", url, WebRequest.Instance.detailedLogs || Application.isEditor ? detail : null);
			}
			if(error != "") { // has error
				error = "[" + DateTime.Now.ToShortTimeString() + "]: " + error;
				WebRequest.Instance.LogError.Invoke(error);
				WebRequest.Instance.lastRequestResponse = error;

#if UNITY_2020
				if(Request.result == UnityWebRequest.Result.ProtocolError) yield break;
#else
				if(Request.isHttpError) yield break;
#endif
				if(forceRetry) yield return Send(forceRetry, timeout);
				else if(!silent && WebRequest.RetryAsk != null) {
					yield return WebRequest.RetryAsk();
					if(WebRequest.RetryResult()) yield return Send(forceRetry, timeout);
				}
			} else {
				Suceed = true;
				var str = "<b>URL</b>: " + url
					+ "\n<b>Form:</b> " + form.LogString()
					+ "\n<b>Response:</b> " + Text
					+ "\n<b>Error:</b> " + error
					+ "\n" + Request.error;

				WebRequest.Instance.lastRequestResponse = str;
				callback?.Invoke(this);
			}
		}
	}
}
