﻿using System;
using UnityEngine;

namespace Drafts.Battle {

	[Serializable]
	/// <summary></summary>
	public abstract class StatBase {
		public abstract int Length { get; }
		public abstract string[] DisplayNames { get; }
		public abstract int[] SortOrder { get; }
		public virtual string GetDescription {
			get {
				var name = DisplayNames;
				var ret = "";

				for(int i = 0; i < Length; i++) {
					if(add[i] != 0) ret += $"\n{name[i]} {add[i]:+0;-#}";
					if(stack[i] != 0) ret += $"\n{name[i]} {stack[i]:+0;-#}%";
					if(mult[i] != 0) ret += $"\n{name[i]} x{1 + mult[i]}";
				}
				return ret.Length > 0 ? ret.Substring(1) : ret;
			}
		}

		public float[] add, stack, mult, total;

		public float this[int stat] => total[stat];

		public StatBase() {
			add = new float[Length];
			stack = new float[Length];
			mult = new float[Length];
			total = new float[Length];
		}
		public StatBase(float[] _base) {
			if(_base.Length != Length) Debug.LogError("wrong stats array size");
			add = _base;
			total = _base;
		}

		/// <summary>Formula: (base + ADD) * mult * (1 + stack)</summary>
		public void Add(int stat, float amount) { add[stat] += amount; Recalculate(stat); }
		/// <summary>Formula: (base + add) * mult * (1 + STACK)</summary>
		public void Stack(int stat, float amount) { stack[stat] += amount; Recalculate(stat); }
		/// <summary>Formula: (base + add) * MULT * (1 + stack)</summary>
		public void Mult(int stat, float amount) { mult[stat] *= amount < 0 ? -1 / (1 + amount) : 1 + amount; Recalculate(stat); }

		public void Recalculate() { for(int i = 0; i < Length; i++) Recalculate(i); }
		protected void Recalculate(int stat) => total[stat] = add[stat] * mult[stat] * (1 + stack[stat]);
	}
}