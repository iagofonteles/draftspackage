﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Drafts.Translations {

	[DisallowMultipleComponent, AddComponentMenu("Drafts/Translation/Translated Text")]
	public class TranslatedText : MonoBehaviour, ITranslatedComponent {

		public TranslatedString String;
		public string alias = default;
		public string key = default;

		Action _update;
		Action Update {
			get {
				if(_update == null) {
					var text = GetComponent<Text>();
					var tmp = GetComponent<TextMeshPro>();
					var ugui = GetComponent<TextMeshProUGUI>();

					if(!text && !tmp && !ugui) Debug.LogException(
						new Exception($"Missing Text, TextMeshPro or TextMeshProUGUI on {name}"), this);

					if(text) _update = () => text.text = String;
					if(tmp) _update = () => tmp.text = String;
					if(ugui) _update = () => ugui.text = String;
				}
				return _update;
			}
		}

		void OnValidate() => Update();
		public void Translate() => Update();
		private void Start() { Translate(); Translation.Instance.OnChanged.AddListener(Translate); }
		private void OnDestroy() => Translation.Instance.OnChanged.RemoveListener(Translate);
	}
}
