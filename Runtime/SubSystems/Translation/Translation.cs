﻿using UnityEngine;
using System.Linq;
using System.IO;
using Drafts.Translations;
using System.Collections.Generic;
using Drafts.Patterns;

namespace Drafts.Translations {

	/// <summary>Mark a Component to be searched for IValidateTranslation fields and call Validate on them.
	/// This is not required but helps searching the project for missing or wrong keys.</summary>
	public interface ITranslatedComponent { }

	/// <summary></summary>
	public interface IValidateTranslation { bool Validate(); }
}

namespace Drafts {

	[CreateAssetMenu(menuName = "Drafts/Systems/Translation", fileName = "Config Translation")]
	public class Translation : ScriptableSingleton<Translation> {
		protected override int LoadPriority => 100;

		internal static HashSet<IValidateTranslation> ValidationList = new HashSet<IValidateTranslation>();

		/// <summary>Where to load files with LoadExtern.</summary>
		public string folder;
		[SerializeField] List<TranslationKeySet> keySets = default;
		[SerializeField] List<TextAsset> translations = default;

		/// <summary>After a new file or language was loaded.</summary>
		[Space] public DraftsEvent OnChanged;

		protected override void Initialize() {
			if(ValidationList.Count > 0) Debug.Log("ValidationList: " + ValidationList.Count);
			ValidationList.RemoveWhere(v => v.Validate());
		}

		/// <summary>Get a specific keyset to later use. Less overhead then Translate.</summary>
		public TranslationKeySet GetKeySet(string alias) => keySets.FirstOrDefault(p => p.alias == alias);

		/// <summary>Get the current translated string in a specific keyset.</summary>
		public static string Translate(string keySet, string key) => Instance.GetKeySet(keySet)[key];

		/// <summary>Load texts from stored files list.</summary>
		public void LoadIntern(string lang, string keySet = null) {
			foreach(var p in keySets) {
				if(keySet != null && p.alias != keySet) continue;
				var file = translations.FirstOrDefault(f => f.name == $"{p.alias}_{lang}");
				if(file) p.LoadTexts(file.text);
			}
			Instance.OnChanged.Invoke();
		}

		/// <summary>Load all files *_[lang].txt</summary>
		public void LoadExtern(string lang) {
			foreach(var p in keySets) {
				var file = File.ReadAllLines($"{folder}/{p.alias}_{lang}.txt");
				p.LoadTexts(file);
			}
			Instance.OnChanged.Invoke();
		}

		public static string[] GetAllKeysWithSet() => Instance.keySets.Aggregate((IEnumerable<string>)new string[0], 
			(all, k) => all.Concat(k.GetKeysWithSet())).ToArray();
		
		public bool AddKeySet(TranslationKeySet set) {
			if(keySets.Any(s => s.alias == set.alias)) {
				Debug.LogError($"A keySet {set.alias} already exists.");
				return false;
			}
			keySets.Add(set);
			return true;
		}

		public TranslationKeySet CreateKeySet(string alias) {
			var set = GetKeySet(alias);
			if(set) return set;
			set = CreateInstance<TranslationKeySet>();
			set.alias = alias;
			keySets.Add(set);
			return set;
		}

	}

}
