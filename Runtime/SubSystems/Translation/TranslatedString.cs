﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Drafts.Translations {

	/// <summary>Declare as many TranslatedString as you want and call Validate().</summary>
	[Serializable]
	public class TranslatedStrings : IValidateTranslation {

		IEnumerable<TranslatedString> GetStrings() {
			try {
				var s = this;
				return GetType().GetFields().Select(f => (TranslatedString)f.GetValue(s));
			} catch {
				throw new FormatException("Only TranslatedString fields are allowed in a TranslationStrings subtype.");
			}
		}

		public bool Validate() => GetStrings().Count(s => !s.Validate()) == 0;

		internal bool IsValid => GetStrings().All(s => s.keySet && s.keyId >= 0);

		internal void CreateAllKeySetsAndKeys() {
			foreach(var s in GetStrings())
				if(!s.Validate()) {
					if(!s.keySet) s.CreateKeySet();
					if(s.keyId < 0) s.keySet.AddKey(s.set);
				}
			Validate();
		}
	}

	/// <summary>Getting the translated text from TranslatedString is O(1)!
	/// Using serialized components is better for initialization thoungh.</summary>
	[Serializable]
	public class TranslatedString : IValidateTranslation {
		//[StaticDropdown(typeof(Translation), "GetAllKeysWithSet", true, true)]
		[SerializeField] internal string keyWithSet;
		[SerializeField] internal string set, key;

		[SerializeField, ReadOnly] internal TranslationKeySet keySet;
		[SerializeField, ReadOnly] internal int keyId;
		public string String => keySet ? keySet[keyId] : null;

		[SerializeField, HideInInspector] string defaultText;
		[SerializeField, HideInInspector] bool lockKey;

		//internal string[] KeysNames => keySet ? keySet.GetKeys() : new string[0];

		/// /// <summary>Separate keySet from key with a slash like 'main/money'.
		/// Store for faster access later.</summary>
		/// <param name="defaultText">Only used for addinng a KeySet for this class.</param>
		public TranslatedString(string set, string key, string defaultText = null) {
			this.defaultText = defaultText;
			this.set = set;
			this.key = key;
			keyWithSet = $"{key}/{set}";
			lockKey = true;
			keySet = null;
			keyId = -3;
			if(Translation.Instance) Validate();
			else Translation.ValidationList.Add(this);
		}
		public TranslatedString(TranslationKeySet keySet, int keyId) {
			set = keySet.alias;
			key = keySet[keyId].alias;
			keyWithSet = $"{key}/{set}";
			this.keySet = keySet;
			this.keyId = keyId;
			lockKey = true;
			defaultText = null;
		}

		public TranslatedString() { }

		public bool Validate() {
			if(!Translation.Instance) throw new Exception("Translation system not found.");
			keySet = Translation.Instance.GetKeySet(set);
			keyId = keySet ? keySet.GetKeyId(key) : -2;
			return keySet && keyId >= 0;
		}

		public static implicit operator string(TranslatedString t) => t?.String;
		public static implicit operator TranslatedString(string set_key_text) {
			var split = set_key_text.Split('/');
			return new TranslatedString(split[0], split[1], string.Join("/", split.Skip(2)));
		}
		public void CreateKeySet() => keySet = Translation.Instance.CreateKeySet(key);

		public static implicit operator TranslatedString((string set, string key) tuple)
			=> new TranslatedString(tuple.set, tuple.key);
		public static implicit operator TranslatedString((string set, string key, string text) tuple)
			=> new TranslatedString(tuple.set, tuple.key, tuple.text);
		public override string ToString() => String;
	}

}
