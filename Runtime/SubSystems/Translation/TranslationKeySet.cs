﻿using Drafts.RegexExtra;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Drafts.Translations {
	[Serializable]
	public class TranslationKey {
		public string alias;
		public int id;
		public string text;
		public static implicit operator string(TranslationKey k) => k?.text;
	}

	[CreateAssetMenu(menuName = "Drafts/Translation KeySet", fileName = "New KeySet")]
	public class TranslationKeySet : ScriptableObject, IEnumerable<TranslationKey> {
		public string alias;
		List<TranslationKey> keys = new List<TranslationKey>();
		public int Count => keys.Count;

		/// <summary>Return the translated string.</summary>
		public TranslationKey this[int id] { get { try { return keys[id]; } catch { return null; } } }
		/// <summary>Return the translated string.</summary>
		public TranslationKey this[string key] => keys[GetKeyId(key)];

		public IEnumerator<TranslationKey> GetEnumerator() => keys.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => keys.GetEnumerator();

		/// <summary>Returns -1 if not found.</summary>
		public int GetKeyId(string key) => keys.FirstOrDefault(l => l.alias == key)?.id ?? -1;
		public string GenerateEmptyKeys() => keys.OrderBy(l => l.alias).Aggregate("", (s, l) => s + $"{l.alias}\t\n");
		public string GenerateFile() => keys.OrderBy(l => l.alias).Aggregate("", (s, l) => s + $"{l.alias}\t{l.text}\n");

		public bool AddKey(string key, string text = "") {
			if(string.IsNullOrEmpty(key)) return false;
			if(keys.All(l => l.alias != key)) {
				keys.Add(new TranslationKey() { id = keys.Count, alias = key, text = text });
				return true;
			}
			return false;
		}

		public void LoadTexts(string file) => LoadTexts(file.Split('\n'));
		public void LoadTexts(string[] file) {
			foreach(var s in file) {
				if(string.IsNullOrWhiteSpace(s)) continue;
				if(!MyRegex.OnlyLetters(s[0].ToString())) continue;
				var skey = s.Substring(0, s.IndexOf("\t"));
				var line = keys.FirstOrDefault(l => l.alias == skey);
				var txt = s.Substring(skey.Length + 1);
				if(line != null && !string.IsNullOrWhiteSpace(txt))
					line.text = txt;
			}
		}
		public void LoadKeys(string file) => LoadKeys(file.Split('\n'));
		public void LoadKeys(string[] file) {
			foreach(var s in file) {
				if(string.IsNullOrWhiteSpace(s)) continue;
				if(!MyRegex.OnlyLetters(s[0].ToString())) continue;
				var tab = s.IndexOf("\t");
				var key = tab < 0 ? s : s.Substring(0, tab);
				if(MyRegex.OnlyLetters(s[0].ToString()) && keys.All(l => l.alias != key))
					keys.Add(new TranslationKey { id = keys.Count, alias = key });
			}
		}
		public List<int> Filter(string filter, bool sort) {
			IEnumerable<TranslationKey> ret;
			if(filter == "") ret = keys;
			else if(filter == " ") ret = keys.Where(l => string.IsNullOrEmpty(l.alias));
			else ret = keys.Where(l => l.alias.Contains(filter));
			if(sort) ret = ret.OrderBy(l => l.alias);
			return ret.Select(l => l.id).ToList();
		}

		[ContextMenu("Clear All")]
		void OnReset() => keys.Clear();

		public string[] GetKeys() => keys.Select(k => k.alias).ToArray();
		public string[] GetKeysWithSet() => keys.Select(k => $"{alias}/{k.alias}").ToArray();

	}
}
