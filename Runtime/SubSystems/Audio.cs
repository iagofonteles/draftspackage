﻿using UnityEngine;
using System;
using System.Linq;
using Drafts.Patterns;

namespace Drafts {

	/// <summary>
	/// Tip: use LoadClips then extend a method like this: <br />
	/// <c>public static void Play(this BgmEnum bgm, float, float) => Audio.PlayBGM((int)bgm, float, float);</c>
	/// </summary>
	[CreateAssetMenu(fileName = "Audio Config", menuName = "Drafts/Systems/Audio System", order = 0)]
	public class Audio : ScriptableSingleton<Audio> {
		protected override int LoadPriority => 100;

		#region Inspector
		[Header("Configuration")]
		[SerializeField, Range(0, 1)] float initialBGMVolume = 1;
		[SerializeField, Range(0, 1)] float initialSFXVolume = 1;
		[SerializeField] AudioClip[] sfxs = default;

		[Space, Header("Runtime")]
		[SerializeField, ReadOnly, Range(0, 1)] float volumeBGM;
		[SerializeField, ReadOnly, Range(0, 1)] float volumeSFX;
		#endregion

#if !DRAFTS_USE_FMOD
		int current_sfxSlot = 0;
#endif
		AudioSource bgm;
		readonly AudioSource[] sfxSlots = new AudioSource[5];

		protected override void Initialize() {
			// save volume on PlayerPrefs
			VolumeBGM = PlayerPrefs.GetFloat("AudioBGM", initialBGMVolume);
			VolumeSFX = PlayerPrefs.GetFloat("AudioSFX", initialSFXVolume);

			var go = new GameObject("Audio System");
			DontDestroyOnLoad(go);
			bgm = go.AddComponent<AudioSource>();
			bgm.loop = true;
			for(int i = 0; i < sfxSlots.Length; i++)
				sfxSlots[i] = go.AddComponent<AudioSource>();

			OnVolumeChangedBGM += f => bgm.volume = f;
			bgm.volume = VolumeBGM;
		}

		void SFX(AudioClip clip, float pitch, float volume) {
#if DRAFTS_USE_FMOD
#else
			if(!clip) return;
			sfxSlots[current_sfxSlot].pitch = pitch < 0 ? UnityEngine.Random.Range(1 + pitch, 1 - pitch) : pitch;
			sfxSlots[current_sfxSlot].PlayOneShot(clip, volumeSFX * volume);
			current_sfxSlot = (int)Mathf.Repeat(++current_sfxSlot, sfxSlots.Length);
#endif
		}
		void BGM(AudioClip clip, float pitch, float volume) {
#if DRAFTS_USE_FMOD
#else
			if(!clip || clip == bgm.clip) return;
			VolumeBGM = volume;
			bgm.pitch = pitch;
			bgm.clip = clip;
			bgm.Play();
#endif
		}

#region Statics

		/// <summary>Master volume for all BGM.</summary>
		public static float VolumeBGM {
			get => Instance.volumeBGM; set {
				Instance.volumeBGM = value;
				OnVolumeChangedBGM?.Invoke(value);
				PlayerPrefs.SetFloat("AudioBGM", value);
			}
		}
		/// <summary>Master volume for all SFX.</summary>
		public static float VolumeSFX {
			get => Instance.volumeSFX; set {
				Instance.volumeSFX = value;
				OnVolumeChangedSFX?.Invoke(value);
				PlayerPrefs.SetFloat("AudioSFX", value);
			}
		}

		/// <summary>Replace current playing BGM AudioClip.</summary>
		/// <param name="bgm">AudioClip to play.</param>
		/// <param name="pitch">Set negative values to randomize. Ex: pitch = -.2f will result in Random(.8f, 1.2f)</param>
		/// <param name="volume">Same as VolumeBGM, ignore to leave value unchanged.</param>
		public static void PlayBGM(AudioClip bgm, float pitch = 1, float? volume = null) => Instance.BGM(bgm, pitch, volume ?? VolumeBGM);

		/// <summary>Play an AudioClip once without stopping other clips.</summary>
		/// <param name="sfx">AudioClip to play.</param>
		/// <param name="pitch">Set negative values to randomize. Ex: pitch = -.2f will result in Random(.8f, 1.2f)</param>
		/// <param name="volume">Multiply with current VolumeSFX</param>
		public static void PlaySFX(AudioClip sfx, float pitch = 1, float volume = 1) => Instance.SFX(sfx, pitch, volume);

		/// <summary>Play an AudioClip once without stopping other clips.</summary>
		/// <param name="sfx">Index of the AudioClip in sfxs array.</param>
		/// <param name="pitch">Set negative values to randomize. Ex: pitch = -.2f will result in Random(.8f, 1.2f)</param>
		/// <param name="volume">Multiply with current VolumeSFX</param>
		public static void PlaySFX(int sfx, float pitch = 1, float volume = 1) => Instance.SFX(Instance.sfxs[sfx], pitch, volume);

		/// <summary>Play an AudioClip once without stopping other clips.</summary>
		/// <param name="sfx">Name of the AudioClip in sfxs array.</param>
		/// <param name="pitch">Set negative values to randomize. Ex: pitch = -.2f will result in Random(.8f, 1.2f)</param>
		/// <param name="volume">Multiply with current VolumeSFX</param>
		public static void PlaySFX(string sfx, float pitch = 1, float volume = 1)
			=> PlaySFX(Instance.sfxs.FirstOrDefault(s => s.name == sfx), pitch, volume);

		/// <summary>Triggers after changing master volume with VolumeBGM.</summary>
		public static event Action<float> OnVolumeChangedBGM;
		/// <summary>Triggers after changing master volume with VolumeSFX.</summary>
		public static event Action<float> OnVolumeChangedSFX;

		/// <summary>In case you want to mess with bgm mixer settings.</summary>
		public static AudioSource AudioSourceBGM => Instance.bgm;

#endregion

	}
}

