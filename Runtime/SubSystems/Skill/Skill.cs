﻿using System;
using UnityEngine;

namespace Drafts {
	
	[Serializable]
	public abstract partial class Skill<T> : ScriptableObject {
		public Sprite icon;
		[Obsolete] public string castKey;
		[Obsolete] public string animationState;
		[Obsolete] public string castAnimation;

		public bool action = true;
		public bool movement = true;
		[Obsolete] public bool lockDirection = true;

		public float cooldown;
		public float castTime;

		protected virtual void OnInitialize(Caster caster, T player) { }
		protected virtual void OnPassive(Caster caster, T player) { }
		protected virtual void OnUpdate(Caster caster, T player) { }
		protected virtual void OnInterrupt(Caster caster, T player) { }

		protected virtual void OnCast(Caster caster, T player) { }
		protected virtual void WhileCasting(float percent) { }
		protected virtual void OnShoot(Caster caster, T player) { }

		//protected bool IsIdle(Caster caster) => (!action || caster.canAct) && (!movement || caster.canMove);
	}
}
