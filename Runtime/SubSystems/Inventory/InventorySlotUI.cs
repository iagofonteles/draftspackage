﻿using UnityEngine;
using Drafts.Components;
using Drafts.Extensions;

#if DRAFTS_USE_TMPRO
using TMPro;
#else
using UnityEngine.UI;
#endif

namespace Drafts.Inventory {

	public class InventorySlotUI<T, U> : DataView<Slot<T>> where U : DataView<T> {

		[SerializeField] U itemUI = default;
#if DRAFTS_USE_TMPRO
		[SerializeField] TextMeshProUGUI countText = default;
#else
		[SerializeField] Text countText = default;
#endif
		[SerializeField] GameObject favoriteIcon = default;

		public U ItemUI => itemUI;

		protected override void Subscribe(Slot<T> slot) {
			if(slot != null) itemUI.Data = slot.Item;
			slot.OnChanged += Repaint;
			slot.OnRemoved += Unsubscribe;
		}

		protected override void Unsubscribe(Slot<T> slot) {
			itemUI.Data = default;
			slot.OnChanged -= Repaint;
			slot.OnRemoved -= Unsubscribe;
		}

		protected virtual void Repaint(int delta) => Repaint();
		public override void Repaint() {
			countText.TrySetText(Data.Count);
			favoriteIcon.TrySetActive(Data.Favorite);
		}
	}
}
