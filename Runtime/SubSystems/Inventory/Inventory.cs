﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Drafts.Extensions;

namespace Drafts.Inventory {

	/// <summary>Slot containing item, count and favorite flag for IInventory.</summary>
	[SuppressMessage("", "CS0660"), SuppressMessage("", "CS0661")]
	[Serializable]
	public class Slot<TItem> {
		[SerializeField] TItem item;
		[SerializeField] int count;
		[SerializeField] internal bool favorite;

		/// <summary>Delta count. When delta = 0, the favorite property has changed.</summary>
		public event Action<int> OnChanged;
		/// <summary>Delta count.</summary>
		public event Action OnRemoved;
		/// <summary>Parent Inventory.</summary>
		public SlotInventory<TItem> Inventory { get; }
		/// <summary>Actual item on this inventory slot.</summary>
		public TItem Item { get => item; internal set => item = value; }
		/// <summary>Quanty of the item.</summary>
		public virtual int Count {
			get => count;
			set {
				if(count == value) return;
				var delta = value - count;
				count = value;
				OnChanged?.Invoke(delta);
				Inventory?.RemoveEmptySlot(this);
			}
		}
		/// <summary>Favorited itens are suposed the ocupy a slot even when Count is 0.</summary>
		public bool Favorite { get => favorite; set { if(favorite == value) return; favorite = value; OnChanged?.Invoke(0); } }
		/// <summary>Count is 0 and not favorited.</summary>
		public bool IsEmpty => Count <= 0 && !Favorite;

		public Slot(TItem item, int count = 0, bool favorite = false) : this(null, item, count, favorite) { }
		public Slot(SlotInventory<TItem> inventory, TItem item = default, int count = 0, bool favorite = false) {
			Inventory = inventory;
			this.item = item;
			this.count = count;
			this.favorite = favorite;
		}

		internal void CallOnRemoved() => OnRemoved?.Invoke();

		/// <summary>Destroy the target gameObject when this is slot is empty. useful for ListInventory UI.</summary>
		public void DestroyWhenEmpty(GameObject go) {
			void Call(int delta) {
				if(IsEmpty) GameObject.Destroy(go);
				OnChanged -= Call;
			}
			OnChanged += Call;
		}
		/// <summary>Deactivate the target gameObject when this is slot is empty. useful for ListInventory UI.</summary>
		public void DeactivateWhenEmpty(GameObject go) {
			void Call(int delta) {
				if(IsEmpty) go.SetActive(false);
				OnChanged -= Call;
			}
			OnChanged += Call;
		}

		public static bool operator ==(Slot<TItem> slot, TItem item) => slot.Item.Equals(item);
		public static bool operator !=(Slot<TItem> slot, TItem item) => !slot.Item.Equals(item);
		public override bool Equals(object obj) => base.Equals(obj);
		public override int GetHashCode() => base.GetHashCode();
		public static implicit operator TItem(Slot<TItem> slot) => slot.Item;
	}

	/// <summary>Inventory with finite slots. Can Favorite item to reserve the slot.
	/// Favorited slots will show "Item x0" instead of removing the item from the slot.</summary>
	public class SlotInventory<TItem> : IEnumerable<Slot<TItem>> {
		[SerializeField] protected List<Slot<TItem>> slots;
		public IEnumerable<Slot<TItem>> FreeSlots => slots.Where(s => s.IsEmpty);
		public IEnumerable<Slot<TItem>> SlotsWith(TItem item) => slots.Where(s => s == item);
		readonly Func<TItem, int> stackSize; // function to extract max stack size of a specific item
		protected bool removeEmptySlots;
		protected int maxSlots;

		/// <param name="slotCount">Fixed Number of slots</param>
		/// <param name="maxStack">If you dont want to specify for each item, just put "i=>99" for fixed stack size or leave it null for unlimited.</param>
		public SlotInventory(int slotCount, Func<TItem, int> maxStack = null) {
			slots = new Slot<TItem>[slotCount].ToList();
			stackSize = maxStack ?? (i => int.MaxValue);
		}

		#region IInventory Interface
		public Slot<TItem> this[int index] => slots[index];
		public int this[TItem item] { get => Count(item); set => Add(item, value - Count(item)); }
		public List<Slot<TItem>> Slots => slots;
		/// <summary>Always at the end.</summary>
		public event Action<Slot<TItem>> OnSlotAdded;
		public event Action<Slot<TItem>> OnSlotRemoved;
		public event Action<TItem, int> OnItemChanged;

		public int Add(TItem item, int count = 1) {
			if(count < 0) return Remove(item, -count);
			if(item == null) throw new ArgumentNullException("item cannot be null.");
			var initCount = count;

			int stackSize = this.stackSize(item); // max stack value

			// insert in slots current with the item before using free slots
			foreach(var slot in SlotsWith(item)) {
				if(count == 0) break;
				var v = Math.Min(stackSize - slot.Count, count);
				slot.Count += v;
				count -= v;
			}

			// try empty slots

			if(removeEmptySlots) {
				while(count > 0 && slots.Count < maxSlots) {
					var v = Math.Min(stackSize, count);
					var slot = new Slot<TItem>(this, item, v);
					slots.Add(slot);
					count -= v;
					OnSlotAdded?.Invoke(slot);
				}
			} else {
				foreach(var slot in FreeSlots) {
					if(count == 0) break;
					var v = Math.Min(stackSize, count);
					slot.Item = item;
					slot.Count = v;
					count -= v;
				}
			}

			var delta = initCount - count;
			if(delta != 0) OnItemChanged?.Invoke(item, delta);
			return count; // return the amout that could not be inserted
		}

		public int Remove(TItem item, int count = 1) {
			if(count < 0) return Add(item, -count);
			if(item == null) throw new ArgumentNullException("item cannot be null.");
			var initCount = count;

			// slots current with the item
			var with = SlotsWith(item).ToArray();
			for(int i = 0; i < with.Length; i++) {
				if(count == 0) break;
				var v = Math.Min(with[i].Count, count);
				with[i].Count -= v;
				count -= v;
			}

			var delta = initCount - count;
			if(delta != 0) OnItemChanged?.Invoke(item, -delta);
			return count; // return the amout that could not be removed
		}

		internal void RemoveEmptySlot(Slot<TItem> slot) {
			if(slot is MirrorItem<TItem>) return;
			if(removeEmptySlots && slot.IsEmpty) {
				slots.Remove(slot);
				slot.CallOnRemoved();
				OnSlotRemoved?.Invoke(slot);
			}
		}

		/// <summary>If Count >= amout, return true and remove items from inventory.</summary>
		public bool Use(TItem item, int amount = 1) { if(Count(item) >= amount) { Remove(item, amount); return true; } return false; }
		public int Count(TItem item) => slots.Sum(s => s == item ? s.Count : 0);
		public bool Contains(TItem item) => slots.Any(s => s == item);
		/// <summary>Number of slots current in inventory.</summary>
		public virtual int Length => slots.Count;
		/// <summary>Max number of slots for the inventory.</summary>
		public int MaxSlots {
			get => maxSlots;
			set {
				maxSlots = value;
				var delta = value - slots.Count;
				if(delta > 0) {
					if(!removeEmptySlots) // add empty slots if not bag
						while(delta-- > 0) {
							var slot = new Slot<TItem>(this);
							slots.Add();
							OnSlotAdded?.Invoke(slot);
						}
				} else RemoveSlots(delta);
			}
		}

		void RemoveSlots(int count) {
			// remove empty slots first
			foreach(var slot in slots.Where(s => s.IsEmpty)) {
				if(count++ == 0) break;
				slots.Remove(slot);
				slot.CallOnRemoved();
				OnSlotRemoved?.Invoke(slot);
			}
			if(count < 0) Debug.LogWarning("Slots with itens where removed.");
			while(count++ < 0) {
				var slot = slots[slots.Count - 1];
				slots.Remove(slot);
				slot.CallOnRemoved();
				OnSlotRemoved?.Invoke(slot);
			}
		}

		public void Clear(bool clearFavorites = false) {
			for(int i = slots.Count - 1; i >= 0; i--) {
				if(clearFavorites) slots[i].favorite = false;
				slots[i].Count = 0;
			}
		}

		public void Sort(Comparer<Slot<TItem>> comparer, bool executeUpdate) => throw new NotImplementedException();

		/// <summary>WARNING: this does not trigger any callbacks.</summary>
		public void LoadSlots(IEnumerable<Slot<TItem>> slots) {
			this.slots.Clear();
			this.slots.AddRange(slots);
		}

		//public void Load(Slot<TItem>[] slots, bool executeUpdate) { this.slots = slots; if(executeUpdate) UpdateAll(); }
		//void UpdateAll() { for(int i = 0; i < slots.Length; i++) OnUpdate?.Invoke(new ChangeInfo<TItem>(i, slots[i].Count, slots[i])); }
		#endregion

		public IEnumerator<Slot<TItem>> GetEnumerator() => slots.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => slots.GetEnumerator();
	}

	///// <summary>A List based Inventory, slots are added and deleted.
	///// Favorited slots will show at the top instead of removing the slot from the list.</summary>
	public class ListInventory<TItem> : SlotInventory<TItem> {
		public ListInventory(Func<TItem, int> maxStack) : this(int.MaxValue, maxStack) { }
		public ListInventory(int maxSlots = int.MaxValue, Func<TItem, int> maxStack = null) : base(0, maxStack) {
			this.maxSlots = maxSlots;
			removeEmptySlots = true;
		}
	}

	/// <summary>Reflect the current amount of a given item in on inventory. OnRemove and OnChange are called too.</summary>
	[Serializable]
	public class MirrorItem<T> : Slot<T> {
		public MirrorItem(SlotInventory<T> inventory, T item) : base(inventory, item, inventory.Count(item))
			=> Inventory.OnItemChanged += Mirror;
		~MirrorItem() => Inventory.OnItemChanged -= Mirror;
		void Mirror(T item, int delta) { if(item.Equals(Item)) base.Count += delta; }

		/// <summary>When changin this will try to add/remove the item from original inventory.</summary>
		new public int Count {
			get => base.Count;
			set => Inventory.Add(Item, value - base.Count);
		}
	}

}
