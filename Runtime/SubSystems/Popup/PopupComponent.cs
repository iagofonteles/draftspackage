using UnityEngine;
using System;
using Drafts.Patterns;

namespace Drafts.Components {

	/// <summary>Use the Value property to get/set the final output.
	/// LastValue can be checked if only one instance of the popexists at a time.
	/// callback is called on OnDisable if any.
	/// Cancel() will set the Value to default and disable the popup. Use it as a close button.</summary>
	[DisallowMultipleComponent]
	public abstract class PopupComponent<DATA, RETURN> : DataView<DATA>, IPrefabPool {

		static PopupComponent<DATA, RETURN> prefab;
		protected static PopupComponent<DATA, RETURN> Prefab => prefab ?? (prefab = PrefabPool.GetPefab<PopupComponent<DATA, RETURN>>());
		static PopupComponent<DATA, RETURN> instance;
		protected static PopupComponent<DATA, RETURN> Instance => instance ?? (instance = Instantiate(Prefab));
		[SerializeField] bool reuseObject = default;

		RETURN value;
		public Action<RETURN> callback;
		public static RETURN LastValue { get; private set; }
		public static bool LastCancelled => !LastValue.Equals(default);
		public DraftsEvent<RETURN> OnValueChanged;
		public bool Accepted => !Value.Equals(default);

		public RETURN Value {
			get => value;
			set {
				this.value = value;
				LastValue = value;
				OnValueChanged?.Invoke(value);
			}
		}
		public void Cancel() { Value = default; gameObject.SetActive(false); }
		protected virtual void OnDisable() { callback?.Invoke(Value); }

		public WaitWhile Wait => new WaitWhile(() => gameObject && gameObject.activeInHierarchy);

		public static WaitWhile Show(DATA data) => Create(data).Wait;

		public static void Show(DATA data, Action<RETURN> callback) => Create(data, callback);

		public static PopupComponent<DATA, RETURN> Create(DATA data, Action<RETURN> callback = null) {
			var popup = Prefab.reuseObject ? Instance : Instantiate(Prefab);
			popup.gameObject.SetActive(true);
			popup.Data = data;
			popup.callback = callback;
			return popup;
		}

	}
}