using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;
using Drafts.Extensions;
using Drafts.Popups;
using POP = Drafts.Components.PopupComponent<Drafts.Popups.PopupOptions, Drafts.Popups.PopupReturn>;
using System.Linq;
using System.Collections.Generic;

#if DRAFTS_USE_TMPRO
using TMPro;
#endif

namespace Drafts {

	public class Popup : POP {
		
		[Obsolete]
		public static void CicleSelectable() { }

		#region Variables

		[Header("Default Values")]
		[SerializeField] float wait = 0;
		[SerializeField] string ok = "Ok";
		[SerializeField] string yes = "Yes";
		[SerializeField] string no = "No";
		[SerializeField] string placeholder = "Type Here...";
		[SerializeField] string text = "Default Text";

		[Header("Components")]
#if DRAFTS_USE_TMPRO
		public TextMeshProUGUI message;
		public TMP_Dropdown day, month, year;
		public TMP_InputField input;
#else
		public Text message;
		public Dropdown day, month, year;
		public InputField input;
#endif
		public Button buttonClose, buttonOk, buttonYes, buttonNo;
		public Button itemTemplate;

		Func<string, bool> validation;

		#endregion

		#region Implementation

		void Start() {
			if(buttonClose) buttonClose.onClick.AddListener(Cancel);
			if(buttonNo) buttonNo.onClick.AddListener(() => Close(0));
			if(buttonYes) buttonYes.onClick.AddListener(() => Close(1));
			if(buttonOk) buttonOk.onClick.AddListener(() => Close(1));
		}

		public void Close(int choice) {
			Value = new PopupReturn() {
				choice = choice,
				input = input.text,
				date = DateTime.MinValue //new DateTime(year.value, month.value, day.value);
			};
			gameObject.SetActive(false);
		}

		void Update() {
			if(validation != null) {
				var b = validation.Invoke(input.text);
				buttonYes.interactable = b;
				buttonOk.interactable = b;
			}
		}

		static void SetButtonText(Component c, string txt, string def) {
#if DRAFTS_USE_TMPRO
			if(c) c.GetComponentInChildren<TextMeshProUGUI>().TrySetText(txt == "" ? def : txt);
#else
			if(c) c.GetComponentInChildren<Text>().TrySetText(txt == "" ? def : txt);
#endif
		}

		public override void Repaint() {
			// set texts on buttons
			message.text = Data.text == "" ? text : Data.text;
			SetButtonText(buttonOk, Data.yes, ok);
			SetButtonText(buttonYes, Data.yes, yes);
			SetButtonText(buttonNo, Data.no, no);
			SetButtonText(input?.placeholder, Data.placeholder, placeholder);

			// deactivate unessessary controls
			message.gameObject.SetActive(Data.text != null);
			if(buttonOk) buttonOk.gameObject.SetActive(Data.yes != null && Data.no == null);
			if(buttonYes) buttonYes.gameObject.SetActive(Data.yes != null && Data.no != null);
			if(buttonNo) buttonNo.gameObject.SetActive(Data.no != null);
			if(input) input.gameObject.SetActive(Data.placeholder != null);
			if(day) day.gameObject.SetActive(Data.defaultDate != null);
			if(month) month.gameObject.SetActive(Data.defaultDate != null);
			if(year) year.gameObject.SetActive(Data.defaultDate != null);
			if(buttonClose) buttonClose.gameObject.SetActive(Data.canCancel);

			// wait & validation
			var w = Data.wait < 0 ? wait : Data.wait;
			if(buttonYes) buttonYes.interactable = false;
			if(buttonOk) buttonOk.interactable = false;
			StartCoroutine(ActivateButtons(w, Data.validation));
		}

		IEnumerator ActivateButtons(float w, Func<string, bool> validation) {
			yield return new WaitForSeconds(w);
			if(buttonYes) buttonYes.interactable = true;
			if(buttonOk) buttonOk.interactable = true;
			this.validation = validation;
		}

		void SetupChoices((Sprite spr, string text)[] choices) => throw new NotImplementedException();

		#endregion

		#region Static Methods

		/// <summary>The resulting value can also be accessed in Popup.LastAccepted.</summary>
		public static POP Show(string text) => Show(text, "");

		/// <summary>The resulting value can also be accessed in Popup.LastAccepted.</summary>
		public static POP Show(string text, string yes = "", bool canCancel = false, float wait = -1, Action callback = null)
			=> Create(PopupOptions.Popup(text, yes, canCancel, wait), r => callback?.Invoke());

		/// <summary>The resulting value can also be accessed in Popup.LastAccepted.</summary>
		public static POP Choice01(string text, string yes = "", string no = "", bool canCancel = false, float wait = -1, Action<bool> callback = null)
			=> Create(PopupOptions.Choice01(text, yes, no, canCancel, wait), r => callback?.Invoke(r.confirmed));

		/// <summary>The resulting value can also be accessed in Popup.LastValue.choice.</summary>
		public static POP Choice(string[] choices, string text = null, string yes = "", bool canCancel = false, float wait = -1, Action<int> callback = null)
			=> Create(PopupOptions.Choice(text, choices.Cast<PopupChoice>(), yes, canCancel, wait), r => callback?.Invoke(r.choice));
		//choices.Select(c => ((Sprite)null, c)).ToArray(),

		/// <summary>The resulting value can also be accessed in Popup.LastValue.choice.</summary>
		public static POP Choice(PopupChoice[] choices, string text = null, string yes = "", bool canCancel = false, float wait = -1, Action<int?> callback = null)
			=> Create(PopupOptions.Choice(text, choices, yes, canCancel, wait), r => callback?.Invoke(r.choice));

		/// <summary>The resulting value can also be accessed in Popup.LastValue.input.</summary>
		public static POP Input(string text, string placeholder = "", Func<string, bool> validation = null, string yes = "", bool canCancel = false, float wait = -1, Action<string> callback = null)
			=> Create(PopupOptions.Input(text, yes, placeholder, validation, canCancel, wait), r => callback?.Invoke(r.input));

		/// <summary>The resulting value can also be accessed in Popup.LastValue.date.</summary>
		public static POP Date(string text, DateTime defaultDate = default, string yes = "", bool canCancel = false, float wait = -1, Action<DateTime?> callback = null)
			=> Create(PopupOptions.Date(text, yes, defaultDate, canCancel, wait), r => callback?.Invoke(r.date));

		#endregion
	}

}

namespace Drafts.Popups {

	public class PopupReturn {
		public bool confirmed => choice != 0;
		public int choice;
		public string input;
		public DateTime date;

		public static implicit operator bool(PopupReturn r) => r.confirmed;
		public static implicit operator int(PopupReturn r) => r.choice;
		public static implicit operator string(PopupReturn r) => r.input;
		public static implicit operator DateTime(PopupReturn r) => r.date;
		public override string ToString() => input;
	}

	public class PopupChoice {
		public string text;
		public Sprite icon;
		public PopupChoice(string txt, Sprite ico = null) { text = txt; icon = ico; }
		public static implicit operator PopupChoice(string text) => new PopupChoice(text, null);
	}

	public class PopupOptions {
		/// <summary>Main text.</summary>
		public string text = null;
		/// <summary>Show/Hide the close button.</summary>
		public bool canCancel = false;
		/// <summary>The text for the button that returns a positive response.</summary>
		public string yes = null;
		/// <summary>The text for the button that returns a negative response. Note it is not the same as cicking the close button (if available).
		/// The button will be deactivated when [no] is null.</summary>
		public string no = null;
		/// <summary>Placeholder for the input field. Also used as the text on Choice.</summary>
		public string placeholder = null;
		/// <summary>The yes button will not be interactable for [wait] seconds.</summary>
		public float wait = -1;
		/// <summary></summary>
		public DateTime? defaultDate = null;
		/// <summary>Used to prevent proceed with invalid text on Input.</summary>
		public Func<string, bool> validation = null;
		/// <summary>Describe the choices in a dialogue.</summary>
		public IEnumerable<PopupChoice> choices;

		PopupOptions() { }

		public static PopupOptions Popup(string text = "", string yes = "", bool cancel = false, float wait = -1) => new PopupOptions() { text = text, yes = yes, canCancel = cancel, wait = wait };
		public static PopupOptions Choice01(string text = "", string yes = "", string no = "", bool cancel = false, float wait = -1) => new PopupOptions() { text = text, yes = yes, no = no, canCancel = cancel, wait = wait };
		//public static PopupOptions Choice(string text = "", string[] choices = null, string yes = "", bool cancel = false, float wait = -1) => new PopupOptions() { text = text, choices = choices.Select(c => (PopupChoice)c).ToArray(), yes = yes, placeholder = text, canCancel = cancel, wait = wait };
		public static PopupOptions Choice(string text = "", IEnumerable<PopupChoice> choices = null, string yes = "", bool cancel = false, float wait = -1) => new PopupOptions() { text = text, choices = choices, yes = yes, placeholder = text, canCancel = cancel, wait = wait };
		public static PopupOptions Input(string text = "", string yes = "", string placeholder = "", Func<string, bool> validation = null, bool cancel = false, float wait = -1) => new PopupOptions() { text = text, yes = yes, placeholder = placeholder, validation = validation, canCancel = cancel, wait = wait };
		public static PopupOptions Date(string text = "", string yes = "", DateTime defaultDate = default, bool cancel = false, float wait = -1) => new PopupOptions() { text = text, yes = yes, defaultDate = defaultDate, canCancel = cancel, wait = wait };
	}

}
