using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Drafts.Extensions;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
using DraftsEditor;
#endif

namespace Drafts.Database {

	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class DatabaseNameAttribute : Attribute { }

	[CreateAssetMenu(menuName = "Drafts/Systems/Database Factory")]
	[Serializable]
	public class DatabaseFactory : ScriptableObject {

		[Tooltip("The namespace of your database.")]
		public string namespaceName = "MyNamespace";

		[Tooltip("The name of your database class.")]
		public string className = "MyDatabase";

		[Tooltip("Full Type name with namespaces"
		+ "\n\nWARNING: types where need to be declared in the executing assembly (your project)."
		+ "\nFor each type, a list is created in the database. The objects will be treated diferently depending on the parent and child types of this type."
		+ "\n\nScriptableObject with childrens: The base class is an abstract ScriptableObject. For all child classes, a instance is created in your project folder and added to the database list."
		+ "\n\nScriptableObject without childrens: The base class is a concrete ScriptableObject. Simply create itens in the specified folder on your project and they will be added to the database list."
		+ "\n\nSerializable: The base class is a concrete Serializable class. Simply add and remove itens in the list and edit its values in the inspector."
		+ "\n\nRuntime: The base class is an abstract class, all child classes will be instantiated at runtime and will not work on editor. The list will show found child classes names."
		+ "\n\nIn any case you may use the [DatabaseName] attribute to use that as the main name instead of the class name itself or Object.name in case of ScriptableObjects.")]
		public string[] types;

		[Tooltip("")]
		public bool createFilesInIndividualFolders;

#if UNITY_EDITOR
		[SerializeField, Button] bool _GenerateClass;
		internal void GenerateClass() {
			var names = types.Select(t => t.Split('.').Last());
			if(names.GroupBy(n => n).Any(g => g.Count() > 1))
				throw new Exception("Types with same name are not supported.");

			var assemblies = ReflectionUtil.GetAllAssembliesExcludingUnitys();
			var baseSOType = typeof(DatabaseItemSO);

			var lists = "";
			var staticLists = "";
			var finds = "";
			var findsId = "";
			foreach(var t in types) {
				var type = assemblies.Select(a => a.GetType(t)).FirstOrDefault(u => u != null);
				if(type == null) throw new NullReferenceException($"type \"{t}\" not found.");

				if(!baseSOType.IsAssignableFrom(type))
					throw new Exception($"{type.Name} needs to be assignable from DatabaseScriptableItem");

				lists += $"\n\t\t[SerializeField, NonReorderable, ReadOnly] List<{t}> _{type.Name};";
				staticLists += $"\n\t\tpublic static IReadOnlyList<{t}> {type.Name} => Instance._{type.Name};";
				finds += $"\n\t\tpublic static {t} Find{type.Name}(string name) => {type.Name}.FirstOrDefault(i => i.DisplayName == name);";
				findsId += $"\n\t\tpublic static {t} Find{type.Name}(int id) => {type.Name}.FirstOrDefault(i => i.Id == id);";
			}
			
			var getters = string.Format("\n{0}\n{1}\n{2}\n{3}", lists, staticLists, finds, findsId);
			var template = EditorUtil.DraftsTemplate("Database Template.cs",
				("@namespace", namespaceName),
				("@class", className),
				("@getters", getters)
			);

			EditorUtil.CreateOrReplaceFile(this, $"{className}.cs", template); // create class file
			var so = CreateInstance(className);
			so.GetType().GetField("factory", ReflectionUtil.CommonFlags).SetValue(so, this);
			EditorUtil.CreateFileOnly(this, className + ".asset", so);
		}
#endif
	}

	public abstract class DatabaseBase<T> : Patterns.ScriptableSingleton<T> where T : DatabaseBase<T> {

		[SerializeField, ReadOnly] internal DatabaseFactory factory = default;

		Type[] types;
		protected Type[] Types {
			get {
				if(types == null) {
					var a = GetType().Assembly;
					types = factory.types.Select(s => a.GetType(s)).ToArray();
				}
				return types;
			}
		}



#if UNITY_EDITOR
		public void GenerateAssets() {
			foreach(var type in Types) {
				var list = this.Reflect<IList>(type.Name); // current list of scriptables
				var all = EditorUtil.FindAssetsAtPath2(EditorUtil.RelativePath(this, type.Name), type).ToList(); // all existint itens in assets
				var subs = ReflectionUtil.FindAllDerivedTypes(GetType().Assembly, type); // subclasses of type

				// remove missing elements
				var itemRemoved = false;
				for(int i = 0; i < list.Count; i++)
					if(!(UnityEngine.Object)list[i]) {
						list.RemoveAt(i);
						itemRemoved = true;
						i--;
					}
				var itens = list.Cast<DatabaseItemSO>().ToList();

				// get existing assets
				all.RemoveAll(i => list.Contains(i));
				foreach(var item in all) {
					list.Add(item);
					itens.Add((DatabaseItemSO)item);
				}

				// re-assign ids
				if(itemRemoved)
					for(int i = 0; i < itens.Count; i++)
						itens[i].id = i;

				subs.RemoveAll(s => itens.Any(i => i.className == s.Name));

				// create a instance of missing itens scriptable
				foreach(var sub in subs) {
					var so = CreateInstance(sub) as DatabaseItemSO;
					so.name = sub.Name;
					so.displayName = sub.Name.SplitCamelCase();
					so.className = sub.Name;
					so.id = itens.Count;

					var path = factory.createFilesInIndividualFolders ? $"{type.Name}/{sub.Name}" : type.Name;
					so = EditorUtil.CreateFileOnly(this, $"{path}/{sub.Name}.asset", so, false);

					list.Add(so);
					itens.Add(so);
				}

				// update enum file
				var enumNotExist = !File.Exists($"Script/Enum{type.Name}.cs");
				if(enumNotExist || itemRemoved || subs.Count > 0 || all.Count > 0) {
					// re-assign ids
					for(int i = 0; i < itens.Count; i++) itens[i].id = i;
					GenerateEnum(type, itens.Select(i => i.name.SplitCamelCase()));
				}

			}
			AssetDatabase.Refresh();
		}

		/// <summary>Save a enum with database getter to a separate file.</summary>
		void GenerateEnum(Type type, IEnumerable<string> names) {
			names = names.Append("Length");

			var enumStr = "\n" + EditorUtil.GenerateEnum("e" + type.Name, names, 0, "\t");
			var access = $"public {type.FullName} this[e{type.Name} id] => {type.Name}[(int)id];";

			enumStr = EditorUtil.DraftsTemplate("Database.Enum Template.cs",
				("@namespace", factory.namespaceName),
				("@class", GetType().Name),
				("@access", access),
				("@enums", enumStr)
			);

			EditorUtil.CreateOrReplaceFile(this, $"Script/Enum{type.Name}.cs", enumStr, false);
		}
#endif
	}

	/// <summary>To be used on datbase as a static reference of an item.</summary>
	public abstract class DatabaseItemSO : ScriptableObject {
		[SerializeField, ReadOnly] internal string className;
		[SerializeField, ReadOnly] internal int id;
		[SerializeField] internal string displayName;

		public int Id => id;
		public string DisplayName => displayName;
	}

	public abstract class DatabaseItemSO<SO, I> : DatabaseItemSO where SO : DatabaseItemSO where I : DatabaseItemInstance<SO> {

		public virtual I CreateType => default;

		public I NewInstance {
			get {
				var ret = CreateType;
				ret.Info = this;
				return ret;
			}
		}
	}

	public abstract class DatabaseItemInstance<T> where T : DatabaseItemSO {
		public DatabaseItemSO Info { get; internal set; }
	}

}

