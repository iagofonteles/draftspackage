﻿using UnityEngine;

namespace Drafts.CustomUISkin {
	
	public class UISkinTextConfig : UISkinElementConfig {
		public enum ColorMode { Text, Image, Override }
		public ColorMode colorMode;
		
		[Conditional("colorMode", (int)ColorMode.Override)]
		public Color textColorOverride = Color.white;
		
		[Conditional("colorMode", (int)ColorMode.Image)]
		public UISkinImageSO getColorFrom;
		
		public bool overrideTextSize;
	}
}
