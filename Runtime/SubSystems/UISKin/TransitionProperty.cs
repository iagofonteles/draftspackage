﻿using UnityEngine;
using UnityEngine.UI;

namespace Drafts.CustomUISkin {
	[System.Serializable]
	public class TransitionProperty {
		public Selectable.Transition mode;
		public AnimationTriggers triggers;
		public SpriteState sprites;
		public ColorBlock colors = new ColorBlock() {
			normalColor = Color.white,
			highlightedColor = Color.white,
			selectedColor = Color.white,
			pressedColor = new Color(.8f, .8f, .8f, 1f),
			disabledColor = new Color(.8f, .8f, .8f, .5f),
			colorMultiplier = 1,
			fadeDuration = .1f
		};
		[HideInInspector, SerializeField]
		internal bool isMain = false;
	}
}
