﻿using UnityEngine;
using UnityEngine.UI;
using Drafts.Extensions;
using System;

namespace Drafts.CustomUISkin {

	public enum MaterialPropertyType { Int, Float, Color, Texture }

	[CreateAssetMenu(menuName = "Drafts/UI SKin/Image Element")]
	public class UISkinImageSO : UISkinElementSO {
		//[Preview]
		[SerializeField] Sprite sprite;
		[SerializeField] Color color = Color.white;
		[SerializeField] bool preserveAspect;
		[SerializeField] TransitionProperty transition;
		[Tooltip("Only used when transition mode is not color tint.")]
		[SerializeField] Color textColor = Color.black;
		[SerializeField] RectOffset borders;
		[SerializeField] Material material;

		public TransitionProperty Transition => transition;
		public RectOffset Borders => borders;
		public Color TextColor => textColor;

		public override Type ConfigType => typeof(UISkinImageConfig);

		public override void UpdateUI(GameObject go) {
			var conf = go.GetComponent<UISkinImageConfig>();
			if(!conf) Debug.Log("missing element config in " + go.transform.parent?.name + " - " + go, go);

			if(conf.applyBorder) {
				if(conf.applyBorder.TryGetComponent<LayoutGroup>(out var lg))
					lg.padding = Borders;
				else ApplyOnRect(conf.applyBorder, Borders);
			}
		}

		public override void OnSKinChange(GameObject go) {
			var config = go.GetComponent<UISkinImageConfig>();
			if(!config) return;

			// if it has an Image component
			if(config.TryGetComponent<Image>(out var image)) {
				if(!config.overrideMaterial) image.material = material;
#if UNITY_EDITOR
				image.sprite = sprite;
#else
				image.overrideSprite = sprite;
#endif
				if(image.type != Image.Type.Filled)
					image.type = preserveAspect ? Image.Type.Simple : Image.Type.Sliced;

				image.preserveAspect = preserveAspect;

				if(transition.mode != Selectable.Transition.ColorTint)
					if(!config.overrideColor) image.color = color;
			}

			if(config.TryGetComponent<Selectable>(out var selectable)) {
				selectable.transition = transition.mode;
				selectable.colors = transition.colors;
				selectable.spriteState = transition.sprites;
				selectable.animationTriggers = transition.triggers;
			}
		}

		// offset & borders
		void ApplyOnRect(RectTransform rect, RectOffset offset) {
			var changeX = rect.anchorMin.x != rect.anchorMax.x;
			var changeY = rect.anchorMin.y != rect.anchorMax.y;

			rect.offsetMin = new Vector2(
				changeX ? offset.left : rect.offsetMin.x,
				changeY ? offset.bottom : rect.offsetMin.y);
			rect.offsetMax = new Vector2(
				changeX ? -offset.right : rect.offsetMax.x,
				changeY ? -offset.top : rect.offsetMax.y);
		}
	}

}
