﻿using System;
using UnityEngine;

namespace Drafts.CustomUISkin {

	[DisallowMultipleComponent]
	public abstract class UISkinElementConfig : MonoBehaviour { }

	public abstract class UISkinElementSO : ScriptableObject {

		public abstract Type ConfigType { get; }

		public abstract void OnSKinChange(GameObject go);

		public virtual void UpdateUI(GameObject go) { }
	}
}
