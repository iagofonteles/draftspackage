﻿using System;
using UnityEngine;
using UnityEngine.UI;

#if DRAFTS_USE_TMPRO
using TMPro;
#endif

namespace Drafts.CustomUISkin {

	/// <summary>Allow easy change of gui elements sprites.</summary>
	[AddComponentMenu("MyEngine/UI Skin")]
	[DisallowMultipleComponent, ExecuteInEditMode]
	public class UISkinElement : MonoBehaviour {

		[StaticDropdown(typeof(UISkin), "GetElementNames", false)]
		[Tooltip("Element name from Default UISkin.")]
		public string element;

		[Editable("ForceUpdate")]
		[SerializeField] UISkinElementSO preset;
		public UISkinElementSO Preset => preset;

		void Start() {
			UISkin.OnSkinChange += GetPreset;
			GetPreset();
		}

		void OnDestroy() => UISkin.OnSkinChange -= GetPreset;

		void GetPreset(UISkinSO so) => GetPreset(false);

		/// <summary>Try to find the preset again and update the UI if needed.</summary>
		[ContextMenu("Get Preset")]
		public void GetPreset() => GetPreset(false, true);
		/// <summary>Try to find the preset again and update the UI if needed.</summary>
		public void GetPreset(bool debug) => GetPreset(debug, true);
		/// <summary>Try to find the preset again and update the UI if needed.</summary>
		public void GetPreset(bool debug, bool update) {
			if(string.IsNullOrEmpty(element)) return;
			var _new = UISkin.GetElement(element.Replace('/', '_'));
			//if(preset == _new) return; // no need to change
			preset = _new;

			if(!preset) {
				if(debug) Debug.LogError($"Skin element {element} not found.", gameObject);
				return;
			}
			if(update) ForceUpdate();
		}

		public void ForceUpdate() {
			if(!preset) return;
			preset.OnSKinChange(gameObject);
			preset.UpdateUI(gameObject);
		}

		private void OnValidate() { if(UISkin.Instance) GetPreset(); }

#if UNITY_EDITOR
		public void ChangeElement() {
			GetPreset(false, false);
			if(!preset) return;
			var conf = GetComponent(preset.ConfigType);
			if(!conf) {
				DestroyImmediate(GetComponent<UISkinElementConfig>());
				gameObject.AddComponent(preset.ConfigType);
			}
			ForceUpdate();
			UnityEditor.EditorUtility.SetDirty(this);
		}
#endif
	}

}
