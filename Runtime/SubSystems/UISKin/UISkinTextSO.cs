﻿using Drafts.Extensions;
using UnityEngine;
using System;

#if DRAFTS_USE_TMPRO
using TMPro;
#else
using UnityEngine.UI;
#endif

namespace Drafts.CustomUISkin {

	[CreateAssetMenu(menuName = "Drafts/UI SKin/Text Element")]
	public class UISkinTextSO : UISkinElementSO {

		public Color color = Color.white;
		public bool fixedFontSize;

		[Conditional("fixedFontSize")]
		public int size;

		[Conditional("fixedFontSize", true)]
		public Vector2Int range;

#if DRAFTS_USE_TMPRO
		public TMP_FontAsset font;
#else
		public Font font;
		public Color shadowColor;
		public Vector2 shadowDistance;
#endif

		public override Type ConfigType => typeof(UISkinTextConfig);

		public override void OnSKinChange(GameObject go) {
			var config = go.GetComponent<UISkinTextConfig>();
			if(!config) return;

			// get text color
			var finalColor = Color.white;
			switch(config.colorMode) {
				case UISkinTextConfig.ColorMode.Text: finalColor = color; break;
				case UISkinTextConfig.ColorMode.Image: finalColor = config.getColorFrom.TextColor; break;
				case UISkinTextConfig.ColorMode.Override: finalColor = config.textColorOverride; break;
			}

#if DRAFTS_USE_TMPRO
			var text = config.gameObject.GetOrAdd<TextMeshProUGUI>();
			text.font = font;
			text.color = finalColor;

			if(!config.overrideTextSize) {
				text.fontSize = size;
				text.fontSizeMin = range.x;
				text.fontSizeMax = range.y;
				text.autoSizeTextContainer = !fixedFontSize;
			}
#else
			var text = config.gameObject.GetOrAdd<Text>();
			text.font = font;
			text.color = finalColor;

			if(!config.overrideTextSize) {
				text.fontSize = size;
				text.resizeTextMinSize = 0;
				text.resizeTextMaxSize = 300;
				text.resizeTextForBestFit = !fixedFontSize;
			}

			var shadow = config.gameObject.GetOrAdd<Shadow>();
			shadow.effectDistance = shadowDistance;
			shadow.effectColor = shadowColor;
#endif
		}

	}
}
