﻿using UnityEngine;

namespace Drafts.CustomUISkin {
	public class UISkinImageConfig : UISkinElementConfig {
		public RectTransform applyBorder = null;
		public bool overrideColor = false;
		public bool overrideMaterial = false;
	}
}
