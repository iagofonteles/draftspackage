﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using DraftsEditor;
using UnityEditor;
#endif

namespace Drafts.CustomUISkin {

	[CreateAssetMenu(fileName = "new UI Skin", menuName = "Drafts/UI SKin/New Skin")]
	public class UISkinSO : ScriptableObject {
		public string displayName;

		public List<UISkinElementSO> elements;
#if UNITY_EDITOR
		[SerializeField, Button] bool _FindElementsInThisFolder;
#endif
		[Header("Default Transitions")]
		public TransitionProperty transition = new TransitionProperty() { isMain = true };

		[ContextMenu("Set Requeriments")]
		protected virtual void SetRequeriments() { }

		public UISkinElementSO GetElement(string name) {
#if UNITY_EDITOR
			elements.RemoveAll(e => !e); // erease missing element
#endif
			return elements.FirstOrDefault(e => e.name == name);
		}

#if UNITY_EDITOR
		[SerializeField, Button] bool _SetThisSkin;
		void SetThisSkin() {
			UISkin.ChangeSkin(this);
			UISkin.ApplyCurrentSkin();
		}

		private void OnValidate() {
			// set color/triggers of elements
			foreach(var a in elements) {
				var e = a as UISkinImageSO;
				if(e) {
					e.Transition.colors = transition.colors;
					e.Transition.triggers = transition.triggers;
				}
			}

			if(baseSkin) notOverridenElements = baseSkin.elements
				.Where(a => elements.All(b => b.name != a.name)).Select(e => e.name).ToArray();
			else notOverridenElements = null;
		}

		[Header("Is Skin Overrider")]
		[SerializeField] UISkinSO baseSkin = default;
		[SerializeField, ReadOnly] string[] notOverridenElements = default;

		[SerializeField] Sprite[] createFrom;
		[SerializeField, Button] bool _CreateElements;
		protected void CreateElements() {
			foreach(var s in createFrom)
				if(elements.All(e => e.name != s.name)) {
					var img = CreateInstance<UISkinImageSO>();
					img.ReflectSet("sprite", s);
					EditorUtil.CreateOrReplaceFile(s, s.name + ".asset", img, false);
					elements.Add(img);
				}
			AssetDatabase.Refresh();
			createFrom = null;
		}
		protected void FindElementsInThisFolder() {
			elements = EditorUtil.FindAssetsAtPath<UISkinElementSO>(EditorUtil.RelativePath(this, "")).ToList();
			OnValidate();
		}
#endif
	}
}
