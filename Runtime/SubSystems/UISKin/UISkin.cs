﻿using Drafts.Extensions;
using System;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Drafts.CustomUISkin {

	[CreateAssetMenu(menuName = "MyEngine/UISkin Manager", fileName = "Config UISkin")]
	public class UISkin : Drafts.Patterns.ScriptableSingleton<UISkin> {
		protected override int LoadPriority => 100;

		[SerializeField] UISkinSO defaultSkin = default;
		[SerializeField] UISkinSO currentSkin = default;
		public UISkinSO[] skins;

		public static string[] GetAllElementsNames() => Instance.defaultSkin.elements
			.OrderBy(e => e.name).Select(e => e.name.Replace('_', '/')).ToArray();

		public static string[] GetImageElementsNames() => Instance.defaultSkin.elements
			.Where(e => e is UISkinImageSO).OrderBy(e => e.name).Select(e => e.name.Replace('_', '/')).ToArray();

		public static string[] GetTextElementsNames() => Instance.defaultSkin.elements
			.Where(e => e is UISkinTextSO).OrderBy(e => e.name).Select(e => e.name.Replace('_', '/')).ToArray();

		public static UISkinSO Default => Instance.defaultSkin;

		public static UISkinSO Current => Instance.currentSkin;

		public static event Action<UISkinSO> OnSkinChange;

		public static void ChangeSkin(string name) {
			var skin = Instance.skins.FirstOrDefault(s => s.name == name);
			if(skin) ChangeSkin(skin);
			else Debug.LogWarning($"Skin {name} does not exist.");
		}

		public static void ChangeSkin(UISkinSO skin) {
			Instance.currentSkin = skin;
			OnSkinChange?.Invoke(skin);
		}

		public static UISkinElementSO GetElement(string name) {
			if(!Instance) throw new Exception("wrong time");

			if(Instance.currentSkin)
				return Instance.currentSkin.GetElement(name) ?? Instance.defaultSkin.GetElement(name);
			else return Instance.defaultSkin.GetElement(name);
		}

#if UNITY_EDITOR
		public static void ApplyCurrentSkin() {
			Resources.FindObjectsOfTypeAll<UISkinElement>().ForEach(s => s.GetPreset(true));
		}
#endif

	}
}
