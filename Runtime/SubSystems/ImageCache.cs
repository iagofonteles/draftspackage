﻿using System.Collections;
using System;
using UnityEngine;
using System.Collections.Generic;
using Drafts.Patterns;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.IO;
using Drafts.Extensions;

namespace Drafts {

	public class ImageCache : ScriptableSingleton<ImageCache> {

		[SerializeField] string folder = "CachedImages";
		[SerializeField] Texture2D defaultTexture;
		[SerializeField] Sprite defaultSprite;

		public static string Folder => Path.Combine(Application.persistentDataPath, Instance.folder);
		public static Texture2D DefaultTexture => Instance.defaultTexture;
		public static Sprite DefaultSprite => Instance.defaultSprite;

		Dictionary<string, CachedImage> cachedImages = new Dictionary<string, CachedImage>();

		/// <summary>Download image from url. Execute callback as soon as image is downloaded.</summary>
		/// <param name="saveToDisk">First try loading the texture from disk.</param>
		public static CachedImage UseTexture(string url, Action<Texture2D> callback, bool saveToDisk = false, bool base64 = false) {
			if(!Instance.cachedImages.TryGetValue(url, out var img)) {
				img = new CachedImage(url, saveToDisk, base64);
				Instance.cachedImages.Add(url, img);
			}
			img.UseTexture(callback);
			return img;
		}

		/// <summary>Download image from url. Execute callback as soon as image is downloaded.</summary>
		/// <param name="saveToDisk">First try loading the texture from disk.</param>
		public static CachedImage UseSprite(string url, Action<Sprite> callback, bool saveToDisk = false, bool base64 = false) {
			if(!Instance.cachedImages.TryGetValue(url, out var img)) {
				img = new CachedImage(url, saveToDisk, base64);
				Instance.cachedImages.Add(url, img);
			}
			img.UseSprite(callback);
			return img;
		}
	}

	/// <summary>Starts the download on constructor, when ready, all callbacks are called.</summary>
	[Serializable]
	public class CachedImage {
		[SerializeField, ReadOnly] string url;
		[SerializeField, ReadOnly] Texture2D texture;
		[SerializeField, ReadOnly] Sprite sprite;
		[SerializeField, ReadOnly] bool saveToDisk;
		[SerializeField, ReadOnly] bool ready;
		bool base64;

		Action<Texture2D> callbackTex;
		Action<Sprite> callbackSpr;

		public string Url => url;
		public bool Ready => ready;
		public DateTime Date { get; private set; }

		public CachedImage(string url, bool saveToDisk = false, bool base64 = false) {
			this.url = url;
			this.saveToDisk = saveToDisk;
			this.base64 = base64;
			ImageCache.StartCoroutine(Download());
		}

		IEnumerator Download() {
			if(saveToDisk) {
				var fname = Path.Combine(ImageCache.Folder, UnityWebRequest.EscapeURL(url));
				if(File.Exists(fname)) {
					var texture = new Texture2D(0, 0);
					texture.LoadImage(File.ReadAllBytes(fname));
					Debug.Log($"file {fname} loaded");
				}
			}

			if(!texture) {
				if(base64) {
					var www = UnityWebRequest.Get(url);
					Debug.Log($"downloading base64 {url}");
					yield return www.SendWebRequest();
					texture = www.downloadHandler.text.Base64Texture2D();
				} else {
					var www = UnityWebRequestTexture.GetTexture(url);
					Debug.Log($"downloading {url}");
					yield return www.SendWebRequest();
					texture = DownloadHandlerTexture.GetContent(www);
				}
			}

			if(!texture) {
				Debug.LogError($"failed to convert texture in {url}");
				yield break;
			}

			sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one / 2);
			callbackTex?.Invoke(texture);
			callbackSpr?.Invoke(sprite);
			callbackTex = null;
			callbackSpr = null;
			Date = DateTime.Now;
			ready = true;

			if(saveToDisk) {
				var fname = Path.Combine(ImageCache.Folder, UnityWebRequest.EscapeURL(url));
				File.WriteAllBytes(fname, texture.EncodeToPNG());
				Debug.Log($"texture saved as {fname}");
			}
		}

		/// <summary>Download image from url. Execute callback as soon as image is downloaded.</summary>
		public void UseTexture(Action<Texture2D> callback) {
			callback?.Invoke(texture ? texture : ImageCache.DefaultTexture);
			if(!texture) callbackTex += callback;
		}

		/// <summary>Download image from url. Execute callback as soon as image is downloaded.</summary>
		public void UseSprite(Action<Sprite> callback) {
			callback?.Invoke(sprite ? sprite : ImageCache.DefaultSprite);
			if(!sprite) callbackSpr += callback;
		}
	}

	/// <summary>Shortcut for ImageCache sub system.</summary>
	public static class ImageCacheExtensions {
		/// <summary>Use ImageCache sub system to download and set sprite.</summary>
		public static void CachedOverrideSprite(this Image image, string url, bool saveToDisk = false) {
			if(!image) return;
			ImageCache.UseSprite(url, spr => image.overrideSprite = spr, saveToDisk);
		}
		/// <summary>Use ImageCache sub system to download and set sprite.</summary>
		public static void CachedSprite(this Image image, string url, bool saveToDisk = false) {
			if(!image) return;
			ImageCache.UseSprite(url, spr => image.sprite = spr, saveToDisk);
		}
		/// <summary>Use ImageCache sub system to download and set sprite.</summary>
		public static void CachedSprite(this SpriteRenderer renderer, string url, bool saveToDisk = false) {
			if(!renderer) return;
			ImageCache.UseSprite(url, spr => renderer.sprite = spr, saveToDisk);
		}
	}
}
