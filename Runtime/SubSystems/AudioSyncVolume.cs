﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using Drafts;

namespace HiddenClasses {
	/// <summary>Sync Audio Sources with Audio.Volume.</summary>
	[AddComponentMenu("MyEngine/Audio Sync Volume")]
	public class AudioSyncVolume : MonoBehaviour {
		/// <summary></summary>
		enum AudioType { BGM, SFX }
		/// <summary></summary>
		[SerializeField] AudioType type = AudioType.SFX;
		[Tooltip("Multiplies VolumeSFX."), Range(0, 1)]
		[SerializeField] float localVolume = 1;

		/// <summary>If empty, all Audio Sources in the GameObject will be synced</summary>
		[Tooltip("If empty, all Audio Sources in the GameObject will be synced.")]
		public AudioSource[] sources;
		void OnVolume(float f) { foreach(var s in sources) s.volume = f * localVolume; }

		[SuppressMessage("", "IDE0051")]
		void Start() {
			if(sources.Length == 0) sources = GetComponents<AudioSource>();
			OnVolume(type == AudioType.BGM ? Audio.VolumeBGM : Audio.VolumeSFX);

			if(type == AudioType.BGM) Audio.OnVolumeChangedBGM += OnVolume;
			else Audio.OnVolumeChangedSFX += OnVolume;
		}
		[SuppressMessage("", "IDE0051")]
		void OnDestroy() {
			if(type == AudioType.BGM) Audio.OnVolumeChangedBGM -= OnVolume;
			else Audio.OnVolumeChangedSFX -= OnVolume;
		}
	}
}