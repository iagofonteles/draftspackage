﻿using UnityEngine;

namespace HiddenClasses
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class SpriteGizmo : MonoBehaviour {
		void Start() => GetComponent<SpriteRenderer>().enabled = false;
	}
}
