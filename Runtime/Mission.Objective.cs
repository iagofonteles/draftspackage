﻿using System;
using static UnityEngine.Mathf;

namespace Drafts.Mission {

	/// <summary>Mission objectives.</summary>
	[Serializable]
	public abstract class Objective {
		/// <summary>Mission reference.</summary>
		public Mission Mission { get; internal set; }
		/// <summary>Short description.</summary>
		public string Name { get; protected set; }
		/// <summary>Goal for the counter.</summary>
		public int Goal { get; private set; }
		/// <summary>Normalized progress towards goal.</summary>
		public float Progress { get; private set; } = 0;
		/// <summary>Wheter goal is met.</summary>
		public bool Completed { get; private set; } = false;
		[NonSerialized]
		private bool enabled = true;
		/// <summary>Disabled objectives cannot change its current counter.</summary>
		public bool Enabled { get => enabled & Mission.Enabled & !Failed; set => enabled = value; }
		/// <summary>A failed objective cannot increase progress any further. Can also be used to temporary disable the task.</summary>
		private bool failed = false;
		/// <summary>A failed objective cannot increase progress any further. Can also be used to temporary disable the task.</summary>
		public bool Failed { get => failed; protected set { if (failed != value) OnFailure(value); failed = value; } }
		/// <summary>Current counter.</summary>
		private int current = 0;
		/// <summary>Current counter.</summary>
		public int Current {
			get => current; set {
				if (!Enabled || current == value) return;
				var _last = Completed;
				current = DontClampCounter ? value : Clamp(value, 0, Goal);
				if (AutoFailure) Failed = Limit ? value > Goal : value < 0;
				Progress = Limit ? value <= Goal ? 1 : 0 : Clamp01(current / (float)Goal);
				Completed = Progress == 1;
				if (Completed != _last) OnCompletion(Completed);
				Mission.UpdateProgress();
			}
		}

		#region Aditional Options
		/// <summary>Set Aditional options</summary>
		public Objective Set(bool? optional = null, int? weight = null, bool? limit = null, bool? autoFailure = null, bool? dontCapCounter = null) {
			Optional = optional ?? Optional;
			Weight = weight ?? Weight;
			Limit = limit ?? Limit;
			AutoFailure = autoFailure ?? AutoFailure;
			DontClampCounter = dontCapCounter ?? DontClampCounter;
			return this;
		}
		/// <summary>Optional objectives does not count towards mission progress.</summary>
		public bool Optional { get; protected set; } = false;
		/// <summary>Weight of individual objective in the total progress of the mission.</summary>
		public int Weight { get; protected set; } = 1;
		/// <summary>The goal is a limit for this objective. It is completed while current &lt;= goal.</summary>
		public bool Limit { get; protected set; } = false;
		/// <summary>Objective fails if it goes below 0 (or above goal if Inverse is true).</summary>
		public bool AutoFailure { get; protected set; } = false;
		/// <summary>Clamp current counter to 0~Goal. AutoFailure will not work in this case.</summary>
		public bool DontClampCounter { get; protected set; } = false;
		#endregion

		/// <summary>Objective status code (failed, disabled, enabled, completed).</summary>
		public int Status => Failed ? 0 : Completed ? 3 : Enabled ? 2 : 1;
		/// <summary>"Name (current/goal)" color coded.</summary>
		public string Display => string.Format("<color={3}>{0} ({1}/{2})</color>", Name, Current, Goal, statusColor[Status]);

		public Objective(string name, int goal) { Name = name; Goal = goal; }

		/// <summary>You can use the bool in case you want the objective to "discomlpete" at some point.</summary>
		protected virtual void OnCompletion(bool completed) { }
		/// <summary>You can use the bool in case you want the objective to "desfailure" at some point.</summary>
		protected virtual void OnFailure(bool failed) { }
		public virtual void Update() { }
		/// <summary>Use this if you want the task to listen to an event handler. it is only triggered when created and again </summary>
		public virtual void OnEnable(bool register) { }
		protected void Register(bool b, Action<int> update, ref Action<int> _event) {
			_event -= update;
			if (b) _event += update;
		}

		/// <summary>Rich text color code.</summary>
		public static string[] statusColor = new string[] { "red", "grey", "black", "green" };
	}
}
