﻿using System;
using System.Collections.Generic;
using Drafts.Extensions;

namespace Drafts {

	/// <summary>Create a pool of objects T to reuse, avoiding overhead of instantiation.</summary>
	[Obsolete]
	public class ObjectPool<T> where T : class {
		protected List<T> pool = new List<T>(); // object pool
		/// <summary>Used to create new instances.</summary>
		Func<T> New;
		/// <summary>Execute after pulling or pushing from pool.</summary>
		Action<T, bool> SetState;

		/// <summary></summary>
		/// <param name="New"></param>
		/// <param name="setState">Triggered after Recycle() and Trash()</param>
		public ObjectPool(Func<T> New = null, Action<T, bool> setState = null) { this.New = New; SetState = setState; }

		/// <summary>Get from the pool or create new if the pool is empty, then do activation action.</summary>
		public T Recycle() {
			var obj = pool.Count == 0 ? New?.Invoke() ?? default : pool.Pop();
			SetState?.Invoke(obj, true);
			return obj;
		}
		/// <summary>Execute deactivation action then return the obj to the pool.</summary>
		public void Trash(T obj) {
			SetState?.Invoke(obj, false);
			pool.Push(obj);
		}
		/// <summary>Instantiate a batch of objects to add to the pool. Use in loading times.</summary>
		public void FillPool(int number) { for (int i = 0; i < number; i++) Trash(New()); }
		/// <summary>Clear pool itens.</summary>
		public void Clear() => pool.Clear();
	}

}