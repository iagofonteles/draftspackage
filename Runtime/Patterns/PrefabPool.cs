using System;
using UnityEngine;
using System.Collections.Generic;

namespace Drafts.Patterns {

	/// <summary>Use it the show your script on PrefabPool.</summary>
	public interface IPrefabPool  {	}

	/// <summary>Used by MonoPool and LazyMonoSingleton.</summary>
	public class PrefabPool : ScriptableSingleton<PrefabPool> {
		public List<GameObject> prefabs;
		public string searchFolder = "Assets/Prefabs";

		/// <summary>Return a new duplicate of the found prefab.</summary>
		public static T Instantiate<T>(Transform parent = null) where T : Component => Instantiate(GetPefab<T>(), parent);
		/// <summary>Store result if you pretend to call multiple times.</summary>
		public static T GetPefab<T>() where T : Component => Instance.prefabs[GetPrefabIndex<T>()].GetComponent<T>();

		static int GetPrefabIndex<T>() where T : Component {
			var index = Instance.prefabs.FindIndex(go => go.GetComponent<T>());
			if(index < 0) Debug.LogException(new NullReferenceException($"No prefab of type {typeof(T).Name} found."), Instance);
			return index;
		}

	}

}
