﻿using UnityEngine;
namespace Drafts.Hidden {
	public abstract class LazyMonoSingleton : MonoBehaviour { }
}

namespace Drafts.Patterns {

	/// <summary>
	/// A Monobehaviour Lazy Singleton Solution
	/// GameObject is only instantiated when Instance is called.
	/// Not persistent by default but IsPersistant can be overriden.
	/// </summary>
	public class LazyMonoSingleton<T> : Hidden.LazyMonoSingleton, IPrefabPool where T : LazyMonoSingleton<T> {

		/// <summary>Not destroyed on scene change.</summary>
		protected virtual bool IsPersistent => false;

		/// <summary>Wheter an instance is currently in scene.</summary>
		public static bool IsInstantiated => instance;

		static T instance;
		public static T Instance {
			get {
				if(!instance) {
					instance = Instantiate(PrefabPool.GetPefab<T>());
					if(instance.IsPersistent) DontDestroyOnLoad(instance.gameObject);
				}
				return instance;
			}
		}
	}
}
