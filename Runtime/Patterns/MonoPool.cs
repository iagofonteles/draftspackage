﻿using UnityEngine;
using System.Collections.Generic;
using Drafts.Extensions;
using Drafts.Patterns;

namespace Drafts.Hidden {
	public abstract class MonoPool : MonoBehaviour, IPrefabPool {
		static Transform trashParent;
		protected static Transform TrashParent => trashParent ?? (trashParent = new GameObject("Mono Pool Trash").transform);
	}
}

namespace Drafts.Patterns {

	/// <summary>
	/// WARNING: Start is called on every Recycle(), use Awake if you want to call only once.
	/// WARNING: OnDestroy is called on every Trash() but te gameobject is not really destroyed.
	/// Destroying an instance using Destroy(obj) will not return the gameobject to the pool but has no other side effects.
	/// </summary>
	public abstract class MonoPool<T> : Hidden.MonoPool where T : MonoPool<T> {

		static T prefab;
		static T Prefab => prefab ?? (prefab = PrefabPool.GetPefab<T>());
		static readonly List<T> pool = new List<T>();

		/// <summary>All non trashed instances.</summary>
		public static List<T> Instances { get; } = new List<T>();

		/// <summary>Get object from pool or instantiate a new one if none, calls Start().</summary>
		public static T Recycle(Transform parent = null) {
			//if(!prefab) prefab = PrefabPool.GetPefab<T>();
			var obj = pool.Count > 0 ? pool.Pop() : Instantiate(Prefab);
			pool.Remove(obj);
			obj.Start();
			obj.transform.SetParent(parent);
			obj.gameObject.SetActive(true);
			return obj;
		}

		/// <summary>Deactivate the game object for later use, calls OnDestroy().</summary>
		public virtual void Trash(T obj) {
			obj.gameObject.SetActive(false);
			obj.OnDestroy();
			Instances.Remove(obj);
			pool.Add(obj);
			obj.transform.SetParent(TrashParent);
		}

		/// <summary>Instantiate up to [count] instances if there is less in the pool. Start nor OnDestroy are called.</summary>
		public static void PrePool(int count) {
			//if(!prefab) prefab = PrefabPool.GetPefab<T>();
			while(pool.Count < count) Instantiate(Prefab).gameObject.SetActive(false);
		}
		/// <summary>Destroy all gameobjects in the pool. WARNING: OnDestroy is called again.</summary>
		public static void ClearPool() { foreach(var obj in pool) Destroy(obj.gameObject); }

		/// <summary>Add to active instances.</summary>
		protected virtual void Start() => Instances.Add((T)this);

		/// <summary>Empty</summary>
		protected virtual void OnDestroy() { }
	}

}
