﻿using System;
using UnityEngine;
using Drafts.Hidden;
using UnityEngine.Events;

namespace Drafts.Hidden {
	public abstract class ValueWatcher { }
	public abstract class ValueWatcherBase<T> : ValueWatcher {
		[SerializeField] protected T _value;
		public static implicit operator T(ValueWatcherBase<T> w) => w._value;
		/// <summary>Remove all non persistent listeners.</summary>
		public abstract void ClearAllEvents();
		/// <summary>Set value without triggering OnChange callback.</summary>
		public void SilentlySet(T newValue) => _value = newValue;
		public override string ToString() => _value.ToString();
	}
}

namespace Drafts.Patterns {

	#region Implementation

	/// <summary>Fires an event with the new value when changed.</summary>
	public class ValueWatcher<T> : ValueWatcherBase<T> {
		/// <summary>OnChanged is only called if new value is different.</summary>
		public T Value {
			get => _value; set {
				if(value.Equals(_value)) return;
				_value = value;
				OnChanged?.Invoke(value);
			}
		}
		/// <summary>Remove all non persistent listeners.</summary>
		public override void ClearAllEvents() => OnChanged.RemoveAllListeners();
		/// <summary>OnChanged(newValue)</summary>
		public DraftsEvent<T> OnChanged = new DraftsEvent<T>();
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static ValueWatcher<T> operator +(ValueWatcher<T> w, Action<T> a) { w.OnChanged.AddListener(new UnityAction<T>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static ValueWatcher<T> operator -(ValueWatcher<T> w, Action<T> a) { w.OnChanged.RemoveListener(new UnityAction<T>(a)); return w; }
	}

	/// <summary>Fires an event with the new and old values when changed.</summary>
	public class ValueWatcher2<T> : ValueWatcherBase<T> {
		/// <summary>OnChanged is only called if new value is different.</summary>
		public T Value {
			get => _value; set {
				if(value.Equals(_value)) return;
				var old = _value;
				_value = value;
				OnChanged.Invoke(value, old);
			}
		}
		/// <summary>Remove all non persistent listeners.</summary>
		public override void ClearAllEvents() => OnChanged.RemoveAllListeners();
		/// <summary>OnChanged(newValue, oldValue)</summary>
		public DraftsEvent<T, T> OnChanged = new DraftsEvent<T, T>();
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static ValueWatcher2<T> operator +(ValueWatcher2<T> w, Action<T, T> a) { w.OnChanged.AddListener(new UnityAction<T, T>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static ValueWatcher2<T> operator -(ValueWatcher2<T> w, Action<T, T> a) { w.OnChanged.RemoveListener(new UnityAction<T, T>(a)); return w; }
	}

	/// <summary>Fires an event with the new value and the delta from previous value.</summary>
	[Serializable]
	public class DeltaWatcher<T> : ValueWatcherBase<T> {
		protected Func<T, T, T> getDelta;
		public DeltaWatcher(Func<T, T, T> getDelta) => this.getDelta = getDelta;

		/// <summary>OnChanged is only called if new value is different.</summary>
		public T Value {
			get => _value; set {
				if(value.Equals(_value)) return;
				var delta = getDelta(_value, value);
				_value = value;
				OnChanged?.Invoke(value, delta);
			}
		}
		/// <summary>Remove all non persistent listeners.</summary>
		public override void ClearAllEvents() => OnChanged = null;
		/// <summary>OnChanged(newValue, delta)</summary>
		public DraftsEvent<T, T> OnChanged = new DraftsEvent<T, T>();
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static DeltaWatcher<T> operator +(DeltaWatcher<T> w, Action<T, T> a) { w.OnChanged.AddListener(new UnityAction<T, T>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static DeltaWatcher<T> operator -(DeltaWatcher<T> w, Action<T, T> a) { w.OnChanged.RemoveListener(new UnityAction<T, T>(a)); return w; }
	}

	#endregion

	#region Watchers

	/// <summary>Callback(newValue).</summary>
	[Serializable]
	public class BoolWatcher : ValueWatcher<bool> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static BoolWatcher operator +(BoolWatcher w, Action<bool> a) { w.OnChanged.AddListener(new UnityAction<bool>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static BoolWatcher operator -(BoolWatcher w, Action<bool> a) { w.OnChanged.RemoveListener(new UnityAction<bool>(a)); return w; }
	}

	/// <summary>Callback(newValue). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class StringWatcher : ValueWatcher<string> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static StringWatcher operator +(StringWatcher w, Action<string> a) { w.OnChanged.AddListener(new UnityAction<string>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static StringWatcher operator -(StringWatcher w, Action<string> a) { w.OnChanged.RemoveListener(new UnityAction<string>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static StringWatcher operator +(StringWatcher w, string value) { w.Value += value; return w; }
	}

	/// <summary>Callback(newValue). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class IntWatcher : ValueWatcher<int> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static IntWatcher operator +(IntWatcher w, Action<int> a) { w.OnChanged.AddListener(new UnityAction<int>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static IntWatcher operator -(IntWatcher w, Action<int> a) { w.OnChanged.RemoveListener(new UnityAction<int>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher operator +(IntWatcher w, int value) { w.Value += value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher operator -(IntWatcher w, int value) { w.Value -= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher operator *(IntWatcher w, int value) { w.Value *= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher operator /(IntWatcher w, int value) { w.Value /= value; return w; }
	}

	/// <summary>Callback(newValue). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class FloatWatcher : ValueWatcher<float> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static FloatWatcher operator +(FloatWatcher w, Action<float> a) { w.OnChanged.AddListener(new UnityAction<float>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static FloatWatcher operator -(FloatWatcher w, Action<float> a) { w.OnChanged.RemoveListener(new UnityAction<float>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher operator +(FloatWatcher w, int value) { w.Value += value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher operator -(FloatWatcher w, int value) { w.Value -= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher operator *(FloatWatcher w, int value) { w.Value *= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher operator /(FloatWatcher w, int value) { w.Value /= value; return w; }
	}

	/// <summary>Callback(newValue, oldValue). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class IntWatcher2 : ValueWatcher2<int> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static IntWatcher2 operator +(IntWatcher2 w, Action<int, int> a) { w.OnChanged.AddListener(new UnityAction<int, int>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static IntWatcher2 operator -(IntWatcher2 w, Action<int, int> a) { w.OnChanged.RemoveListener(new UnityAction<int, int>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher2 operator +(IntWatcher2 w, int value) { w.Value += value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher2 operator -(IntWatcher2 w, int value) { w.Value -= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher2 operator *(IntWatcher2 w, int value) { w.Value *= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntWatcher2 operator /(IntWatcher2 w, int value) { w.Value /= value; return w; }
	}

	/// <summary>Callback(newValue, oldValue). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class FloatWatcher2 : ValueWatcher2<float> {
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static FloatWatcher2 operator +(FloatWatcher2 w, Action<float, float> a) { w.OnChanged.AddListener(new UnityAction<float, float>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static FloatWatcher2 operator -(FloatWatcher2 w, Action<float, float> a) { w.OnChanged.RemoveListener(new UnityAction<float, float>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher2 operator +(FloatWatcher2 w, int value) { w.Value += value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher2 operator -(FloatWatcher2 w, int value) { w.Value -= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher2 operator *(FloatWatcher2 w, int value) { w.Value *= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static FloatWatcher2 operator /(FloatWatcher2 w, int value) { w.Value /= value; return w; }
	}

	/// <summary>Callback(newValue, delta). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class IntDeltaWatcher : DeltaWatcher<int> {
		public IntDeltaWatcher() : base((a, b) => b - a) { }
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static IntDeltaWatcher operator +(IntDeltaWatcher w, Action<int, int> a) { w.OnChanged.AddListener(new UnityAction<int, int>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static IntDeltaWatcher operator -(IntDeltaWatcher w, Action<int, int> a) { w.OnChanged.RemoveListener(new UnityAction<int, int>(a)); return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntDeltaWatcher operator +(IntDeltaWatcher w, int value) { w.Value += value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntDeltaWatcher operator -(IntDeltaWatcher w, int value) { w.Value -= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntDeltaWatcher operator *(IntDeltaWatcher w, int value) { w.Value *= value; return w; }
		/// <summary>Aritimetic operators always trigger OnChanged, even when not stored.</summary>
		public static IntDeltaWatcher operator /(IntDeltaWatcher w, int value) { w.Value /= value; return w; }
	}

	/// <summary>Callback(newValue, delta). Aritimetic operators always modify left operand.</summary>
	[Serializable]
	public class FloatDeltaWatcher : DeltaWatcher<float> {
		public FloatDeltaWatcher() : base((a, b) => b - a) { }
		/// <summary>MODIFY LEFT OPERAND. Same as AddListener.</summary>
		public static FloatDeltaWatcher operator +(FloatDeltaWatcher w, Action<float, float> a) { w.OnChanged.AddListener(new UnityAction<float, float>(a)); return w; }
		/// <summary>MODIFY LEFT OPERAND. Same as RemoveListener.</summary>
		public static FloatDeltaWatcher operator -(FloatDeltaWatcher w, Action<float, float> a) { w.OnChanged.RemoveListener(new UnityAction<float, float>(a)); return w; }
		public static FloatDeltaWatcher operator +(FloatDeltaWatcher w, float value) { w.Value += value; return w; }
		public static FloatDeltaWatcher operator -(FloatDeltaWatcher w, float value) { w.Value -= value; return w; }
		public static FloatDeltaWatcher operator *(FloatDeltaWatcher w, float value) { w.Value *= value; return w; }
		public static FloatDeltaWatcher operator /(FloatDeltaWatcher w, float value) { w.Value /= value; return w; }
	}

	//	public static IntValue operator ++(IntValue v) { v.Value++; return v; }
	//	public static IntValue operator --(IntValue v) { v.Value--; return v; }
	//	public static int operator +(IntValue v) => v._value;
	//	public static int operator -(IntValue v) => -v._value;
	#endregion

	#region Events

	[Serializable]
	public class DraftsEvent : UnityEvent {
		public static DraftsEvent operator +(DraftsEvent w, Action a) { w.AddListener(new UnityAction(a)); return w; }
		public static DraftsEvent operator -(DraftsEvent w, Action a) { w.RemoveListener(new UnityAction(a)); return w; }
	}
	[Serializable]
	public class DraftsEvent<T> : UnityEvent<T> {
		public static DraftsEvent<T> operator +(DraftsEvent<T> w, Action<T> a) { w.AddListener(new UnityAction<T>(a)); return w; }
		public static DraftsEvent<T> operator -(DraftsEvent<T> w, Action<T> a) { w.RemoveListener(new UnityAction<T>(a)); return w; }
	}
	[Serializable]
	public class DraftsEvent<T, U> : UnityEvent<T, U> {
		public static DraftsEvent<T, U> operator +(DraftsEvent<T, U> w, Action<T, U> a) { w.AddListener(new UnityAction<T, U>(a)); return w; }
		public static DraftsEvent<T, U> operator -(DraftsEvent<T, U> w, Action<T, U> a) { w.RemoveListener(new UnityAction<T, U>(a)); return w; }
	}
	[Serializable]
	public class DraftsEvent<T, U, V> : UnityEvent<T, U, V> {
		public static DraftsEvent<T, U, V> operator +(DraftsEvent<T, U, V> w, Action<T, U, V> a) { w.AddListener(new UnityAction<T, U, V>(a)); return w; }
		public static DraftsEvent<T, U, V> operator -(DraftsEvent<T, U, V> w, Action<T, U, V> a) { w.RemoveListener(new UnityAction<T, U, V>(a)); return w; }
	}
	[Serializable]
	public class DraftsEvent<T, U, V, W> : UnityEvent<T, U, V, W> {
		public static DraftsEvent<T, U, V, W> operator +(DraftsEvent<T, U, V, W> w, Action<T, U, V, W> a) { w.AddListener(new UnityAction<T, U, V, W>(a)); return w; }
		public static DraftsEvent<T, U, V, W> operator -(DraftsEvent<T, U, V, W> w, Action<T, U, V, W> a) { w.RemoveListener(new UnityAction<T, U, V, W>(a)); return w; }
	}
	#endregion
}
