﻿using Drafts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UObj = UnityEngine.Object;

namespace Drafts.Patterns {

	[Serializable, Obsolete("Incomplete Feature")]
	public class LocalObjectPool<T> where T : Component {
		public Transform parentTransform;
		public T template;

		public T Recycle() {
			var o = parentTransform.GetComponentsImmediate<T>().FirstOrDefault(c => !c.gameObject.activeSelf);
			if(!o) o = UObj.Instantiate(template, parentTransform);
			return o;
		}
		public virtual void OnRecycle(T obj) { }
		public void Trash(T c) => c.gameObject.SetActive(false);
	}

	[Serializable, Obsolete("Incomplete Feature")]
	public class LocalObjectPool<T, Data> where T : Component {
		public Transform parentTransform;
		public T template;

		public T Recycle(Data data) {
			var o = parentTransform.GetComponentsImmediate<T>().FirstOrDefault(c => !c.gameObject.activeSelf);
			if(!o) o = UObj.Instantiate(template, parentTransform);
			OnRecycle(o, data);
			return o;
		}
		public virtual void OnRecycle(T obj, Data data) { }
		public void Trash(T c) => c.gameObject.SetActive(false);
	}

	public static class ObjectPoolUtil {

		static Transform trashParent;
		static Transform TrashParent => trashParent ?? (trashParent = new GameObject("Pool Trash").transform);

		static Dictionary<Type, List<object>> allPools = new Dictionary<Type, List<object>>();

		public static void DontDestroyOnLoad() => UObj.DontDestroyOnLoad(TrashParent.gameObject);

		/// <summary>Serach for the first inactive children with that component, when not found, a copy of the first active found is returned.</summary>
		public static T PooledChildren<T>(this Transform parent) where T : Component {
			T template = null;
			for(int i = 0; i < parent.childCount; i++) {
				if(parent.GetChild(i).TryGetComponent<T>(out var n)) {
					template = n;
					if(!n.gameObject.activeSelf) {
						n.gameObject.SetActive(true);
						return n;
					}
				}
			}
			var obj = UObj.Instantiate(template, parent);
			obj.gameObject.SetActive(true);
			return obj;
		}

		/// <summary>Create up to [count] copies of the first T child and deactivate them all.</summary>
		public static void PrePoolChild<T>(this Transform parent, int childCount) where T : Component {
			var all = parent.GetComponentsImmediate<T>();
			var template = all[0];
			template.gameObject.SetActive(false);
			for(int i = all.Length; i < childCount; i++) UObj.Instantiate(template, parent);
		}
		/// <summary>Create up to [count] copies of the first child and deactivate them all.</summary>
		public static void PrePoolChild(this Transform parent, int childCount) {
			var template = parent.GetChild(0);
			template.gameObject.SetActive(false);
			for(int i = template.transform.childCount; i < childCount; i++) UObj.Instantiate(template, parent);
		}

		/// <summary></summary>
		/// <param name="prepare">Only called once when instantiating new copies.</param>
		public static T RecycleType<T>(this T prefab, Transform parent = null, Action<T> prepare = null) where T : Component {
			Debug.Log($"recycling {typeof(T)}");
			T obj;
			if(allPools.TryGetValue(typeof(T), out var pool) && pool.Count > 0) {
				obj = (T)pool.Pop();
			} else {
				obj = UObj.Instantiate(prefab, parent);
				prepare?.Invoke(obj);
			}
			obj.gameObject.SetActive(true);
			return obj;
		}

		public static void TrashType<T>(this T instance, Transform parent = null) where T : Component {
			instance.gameObject.SetActive(false);
			instance.transform.parent = parent ?? TrashParent;
			if(allPools.TryGetValue(typeof(T), out var pool)) pool.Add(instance); // add to pool
			else allPools.Add(typeof(T), new List<object>() { instance }); // create pool for that type
		}

		/// <summary>Create a copy on the same parent and activate it.</summary>
		public static T Duplicate<T>(this T template) where T : Component {
			var obj = UObj.Instantiate(template, template.transform.parent);
			obj.gameObject.SetActive(true);
			return obj;
		}

		static Dictionary<GameObject, List<object>> allPrefabPools = new Dictionary<GameObject, List<object>>();

		/// <summary></summary>
		/// <param name="prepare">Only called once when instantiating new copies.</param>
		public static T RecyclePrefab<T>(this T prefab, Transform parent = null, Action<T> prepare = null) where T : Component {
			T obj;
			if(allPrefabPools.TryGetValue(prefab.gameObject, out var pool) && pool.Count > 0) {
				obj = (T)pool.Pop();
			} else {
				obj = UObj.Instantiate(prefab, parent);
				prepare?.Invoke(obj);
			}
			obj.gameObject.SetActive(true);
			return obj;
		}

		public static void TrashPrefab<T>(this T prefab, T instance, Transform parent = null) where T : Component {
			instance.gameObject.SetActive(false);
			instance.transform.parent = parent ?? TrashParent;
			if(allPrefabPools.TryGetValue(prefab.gameObject, out var pool)) pool.Add(instance); // add to pool
			else allPrefabPools.Add(prefab.gameObject, new List<object>() { instance }); // create pool for that type
		}
	}

}
