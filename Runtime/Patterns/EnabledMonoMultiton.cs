﻿using UnityEngine;
using System.Collections.Generic;

namespace Drafts.Patterns {

	/// <summary>
	/// A Monobehaviour Multiton Solution
	/// Store only enabled instances.
	/// </summary>
	public abstract class EnabledMonoMultiton<T> : MonoBehaviour where T : EnabledMonoMultiton<T> {
		/// <summary>All created instances.</summary>
		public static List<T> Instances { get; } = new List<T>();
		/// <summary>Add to active instances.</summary>
		protected virtual void OnEnable() => Instances.Add((T)this);
		/// <summary>Empty</summary>
		protected virtual void OnDisable() => Instances.Remove((T)this);
	}
}
