﻿using UnityEngine;
using System.Collections.Generic;

namespace Drafts.Patterns {

	/// <summary>
	/// A Monobehaviour Multiton Solution
	/// Store every created instance on Awake and remove on OnDestroy.
	/// </summary>
	public abstract class MonoMultiton<T> : MonoBehaviour where T : MonoMultiton<T> {
		/// <summary>All created instances.</summary>
		public static List<T> Instances { get; } = new List<T>();
		/// <summary>Add to active instances.</summary>
		protected virtual void Awake() => Instances.Add((T)this);
		/// <summary>Empty</summary>
		protected virtual void OnDestroy() => Instances.Remove((T)this);
	}
}
