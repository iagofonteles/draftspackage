﻿using UnityEditor;
using UnityEngine;
using System.Diagnostics.CodeAnalysis;
using Drafts.Extensions;
using ALP = Drafts.Components.AlternativeLayoutPosition;

namespace DraftsEditor {

	//[CustomEditor(typeof(ALP), true)]
	public class AlternativeLayoutPositionEditor : Editor {

		SerializedProperty _positions;
		string[] _names;

		[SuppressMessage("", "IDE0051")]
		private void OnEnable() {
			serializedObject.Update();
			_positions = serializedObject.FindProperty("positions");
			_names = (target as ALP).Names;
			_positions.arraySize = _names.Length;

			for(int i = 0; i < _positions.arraySize; i++) {
				var p = _positions.GetArrayElementAtIndex(i);
				if(p.objectReferenceValue == null)
					p.objectReferenceValue = CreateNewPosition(i);
			}
			serializedObject.ApplyModifiedProperties();
		}

		RectTransform CreateNewPosition(int index) {
			var rect = (target as ALP).GetComponent<RectTransform>();
			var n = _names.Length == 0 ? $"Layout {index}" : _names[index];
			var p = new GameObject($"{target.name} [{n}]", typeof(RectTransform)).GetComponent<RectTransform>();
			p.SetParent(rect.parent, false);
			p.SetSiblingIndex(rect.GetSiblingIndex() + 1 + index);
			p.CopyPosition(rect);
			return p;
		}

		public override void OnInspectorGUI() {
			ALP.Layout = EditorGUILayout.IntSlider(ALP.Layout, 0, _positions.arraySize - 1);

			if(_names?.Length == 0) {
				base.OnInspectorGUI();
				return;
			}

			serializedObject.Update();
			for(int i = 0; i < _positions.arraySize; i++)
				EditorGUILayout.PropertyField(_positions.GetArrayElementAtIndex(i),
					new GUIContent(_names[i]));
			serializedObject.ApplyModifiedProperties();
		}
	}
}
