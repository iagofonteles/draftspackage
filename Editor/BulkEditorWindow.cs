using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System;
using Drafts;
using Drafts.Extensions;

namespace DraftsEditor {

	public class BulkEditorWindow : EditorWindow {

		string[] allNames = new string[0];
		SerializedObject[] objects = new SerializedObject[0];
		
		[MenuItem("Drafts/Bulk Edit SO")]
		static void Init() {
			// Get existing open window or if none, make a new one:
			var window = (BulkEditorWindow)GetWindow(typeof(BulkEditorWindow));
			window.Show();
		}

		private void OnEnable() => Selection.selectionChanged += Redraw;
		private void OnDisable() => Selection.selectionChanged -= Redraw;

		private void Redraw() {
			var itens = Selection.objects.Where(o => o is ScriptableObject);
			IEnumerable<FieldInfo> allFields = new List<FieldInfo>();

			foreach(var i in itens) allFields = allFields.Concat(ElegibleFields(i)); // all serialized fields
			allFields = allFields.GroupBy(f => f.Name).Where(g => g.Count() == itens.Count()).Select(g => g.First()); // filter common fields

			allNames = allFields.Select(f => f.Name).ToArray();
			if(allNames.Length > 0) objects = itens.Select(i => new SerializedObject(i)).ToArray();
		}

		IEnumerable<FieldInfo> ElegibleFields(UnityEngine.Object t) => t.GetType().FindSerializableFields<ScriptableObject>();

		void OnGUI() {
			EditorGUILayout.LabelField("Select objects ob the same base type to edit shared fields.");

			if(allNames.Length == 0 || objects.Length == 0) {
				EditorGUILayout.LabelField("No ScriptableObjects selected.");
				return;
			}

			foreach(var n in allNames) {
				EditorGUI.BeginChangeCheck();
				EditorGUILayout.PropertyField(objects[0].FindProperty(n));

				try {
					if(EditorGUI.EndChangeCheck()) {
						objects[0].ApplyModifiedProperties();

						var newValue = ElegibleFields(objects[0].targetObject).First(f => f.Name == n)
							.GetValue(objects[0].targetObject);

						foreach(var o in objects) {
							ElegibleFields(o.targetObject).First(f => f.Name == n)
								.SetValue(o.targetObject, newValue);
							o.Update();
						}
					}
				} catch { Debug.Log("Set Value failed, maybe objects are of different sub types"); }
			}
		}

	}
}
