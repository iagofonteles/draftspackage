﻿using Drafts;
using Drafts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DraftsEditor {

	[CreateAssetMenu(menuName = "Drafts/Editor/Texture observer")]
	public class TextureObserver : ScriptableObject {
		[SerializeField] string folder = "Assets";

		[Button, SerializeField] bool _FindWrongSized;
		public Texture[] wrongSized;

		[Button, SerializeField] bool _FindMaximumSize;
		public List<Texture> maximumSize;
		[Button, SerializeField] bool _OptimizeMaximumSize;

		public void FindWrongSized() {
			var all = new List<(long, Texture)>();

			DoWithAllTextures(folder, (p, t, i) => {
				if(((t.width % 4) != 0) || ((t.height % 4) != 0))
					all.Add((t.width * t.height, t));
			});

			all.Sort((a, b) => b.Item1.CompareTo(a.Item1));
			wrongSized = all.Select(a => a.Item2).ToArray();
			Debug.Log(wrongSized.Length + " Not multiple of 4 textures.");
		}

		public void FindMaximumSize() {
			var sizes = new int[] { 33, 65, 129, 257, 513, 1025, 2049, 4097 };
			maximumSize.Clear();

			var count = new int[sizes.Length];

			DoWithAllTextures(folder, (p, t, importer) => {
				var rank = sizes.RankOf(Mathf.Max(t.width, t.height));
				if(importer && importer.maxTextureSize != sizes[rank] - 1) {
					maximumSize.Add(t);
					count[rank]++;
				}
			});

			var str = $"Can be optimized: {count.Sum()}\n";
			for(int i = 0; i < sizes.Length; i++)
				str += $"{sizes[i] - 1}: {count[i]}\n";
			Debug.Log(str);
		}

		public void OptimizeMaximumSize() {
			var sizes = new int[] { 33, 65, 129, 257, 513, 1025, 2049, 4097 };
			var count = new int[sizes.Length];

			DoWithAllTextures(folder, (path, t, importer) => {
				var rank = sizes.RankOf(Mathf.Max(t.width, t.height));
				if(importer && importer.maxTextureSize != sizes[rank] - 1) {
					importer.maxTextureSize = sizes[rank] - 1;
					AssetDatabase.ImportAsset(path);
					count[rank]++;
				}
			});
			Debug.Log(count.Sum() + " Textures modified");
		}

		static void DoWithAllTextures(string folder, Action<string, Texture, TextureImporter> action) {
			foreach(string guid in AssetDatabase.FindAssets("t:texture", new[] { folder })) {
				string path = AssetDatabase.GUIDToAssetPath(guid);
				var importer = AssetImporter.GetAtPath(path) as TextureImporter;
				var texture = AssetDatabase.LoadAssetAtPath<Texture>(path);
				action(path, texture, importer);
			}
		}

		[MenuItem("Assets/Crunch All Textures")]
		static void CrunchAllTextures() {
			int quality = 50, count = 0;

			DoWithAllTextures("Assets", (path, t, importer) => {
				if(importer
				&& (!importer.crunchedCompression || importer.compressionQuality != quality)) {
					importer.crunchedCompression = true;
					importer.compressionQuality = quality;
					AssetDatabase.ImportAsset(path);
					count++;
				}
			});

			Debug.Log(count + " textures crunched");
		}
	}
}
