﻿using System.Diagnostics.CodeAnalysis;
using UnityEditor;
using UnityEngine;
using Drafts;
using DraftsEditor;
using System.Collections.Generic;

namespace MyEngineEditor {
	[CustomEditor(typeof(Audio))]
	class AudioEditor : Editor {
		SerializedProperty _sfx;
		int removePrefixFromNames = 0;
		
		[SuppressMessage("", "IDE0051")]
		private void OnEnable() => _sfx = serializedObject.FindProperty("sfxs");

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();
			removePrefixFromNames = EditorGUILayout.IntField("Remove Prefix from SFX Names?", removePrefixFromNames);
			if(GUILayout.Button("Generate Enum File")) GenerateEnumFile();
		}

		void GenerateEnumFile() {
			var str = new List<string>();
			for(int i = 0; i < _sfx.arraySize; i++)
				str.Add(_sfx.GetArrayElementAtIndex(i).objectReferenceValue.name);

			var enu = EditorUtil.GenerateEnum("SFX", str, removePrefixFromNames, "\t");

			// replace file
			EditorUtil.CreateOrReplaceFile(target, "AudioEnum.cs",
				EditorUtil.DraftsTemplate("AudioEnum Template", enu));
		}
	}
}
