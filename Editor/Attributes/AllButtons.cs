using UnityEngine;
using UnityEditor;
using Drafts;

namespace DraftsEditor
{
    public class AllButtonsEditor : DraftsPropertyDrawer<AllButtonsEditor>
    {
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			return base.GetPropertyHeight(property, label) + 1;
		}
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			var rect = position;
			position.height= GetPropertyHeight(property, label);
			var after = (attribute as AllButtonsAttribute).after;
			if(!after) EditorGUI.PropertyField(NextY(ref rect), property, true);

			//var allBtn = property.propertyPath;
			Debug.Log(property.propertyPath);
		}
	}
}
