﻿using UnityEditor;
using UnityEngine;
using Drafts;
using System.Linq;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(DropdownAttribute), true)]
	public class DropdownDrawer : DraftsPropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
			=> Draw(attribute as DropdownAttribute, position, property, label);

		public static void Draw(DropdownAttribute dropdown, Rect position, SerializedProperty property, GUIContent label) {
			var all = dropdown.GetStrings(property.GetParentObject()) ?? new string[0];

			var r = position;
			var w = r.width / 10;

			// string field
			EditorGUI.LabelField(NextX(ref r, w * 4), label);
			if(dropdown.AllowTyping) {
				if(dropdown.FilterDropdown)
					all = all.Where(s => s.Contains(property.stringValue)).ToArray();
				EditorGUI.PropertyField(NextX(ref r, w * 4), property, GUIContent.none);
			}
			// dropdown
			EditorGUI.BeginChangeCheck();
			var id = System.Array.IndexOf(all, property.stringValue);
			//id = EditorGUI.Popup(NextX(ref r, w * 1.5f), "", Mathf.Clamp(id, 0, all.Length - 1), all);
			id = EditorGUI.Popup(NextX(ref r, w * (dropdown.AllowTyping ? 1.5f : 6)), "", id, all);
			if(EditorGUI.EndChangeCheck() && id >= 0 && id < all.Length) {
				property.stringValue = all[id];
				property.serializedObject.ApplyModifiedProperties();
			}

			// checkmark
			if(dropdown.AllowTyping) {
				var check = all.Contains(property.stringValue);
				EditorGUI.BeginDisabledGroup(true);
				EditorGUI.Toggle(NextX(ref r, w / 2), GUIContent.none, check);
				EditorGUI.EndDisabledGroup();
			}
		}
	}
}
