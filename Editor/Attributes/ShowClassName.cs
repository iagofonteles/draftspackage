using UnityEditor;
using UnityEngine;
using Drafts;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(ShowClassNameAttribute))]
	public class ShowClassNameEditor : PropertyDrawer {
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> base.GetPropertyHeight(property, label);

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginDisabledGroup(true);

			object o = property.serializedObject.targetObject;
			var t = o.GetType().UnderlyingSystemType.Name;

			EditorGUI.LabelField(position, "Class", t);
			EditorGUI.EndDisabledGroup();
		}
	}
}
