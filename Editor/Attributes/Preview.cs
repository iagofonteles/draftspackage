﻿using UnityEditor;
using UnityEngine;
using Drafts;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(PreviewAttribute))]
	public class PreviewAttributeDrawer : DraftsPropertyDrawer {
		Vector2 GetPreviewSize(SerializedProperty property) {
			return Vector2.one * (attribute as PreviewAttribute).maxHeight;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			return base.GetPropertyHeight(property, label) + GetPreviewSize(property).y;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			var h = base.GetPropertyHeight(property, label);
			EditorGUI.PropertyField(NextY(ref position, h), property);

			var obj = property.objectReferenceValue;
			if(obj is Texture)
				EditorGUI.DrawPreviewTexture(position, (Texture)property.objectReferenceValue, null, ScaleMode.ScaleToFit);
			else if(obj is Sprite)
				EditorGUI.DrawPreviewTexture(position, ((Sprite)property.objectReferenceValue).texture, null, ScaleMode.ScaleToFit);
			else
				Debug.LogError($"Property {property.name} is not a Texture or Sprite.");
		}
	}

}