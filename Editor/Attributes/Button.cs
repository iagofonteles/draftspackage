﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using System.Linq;
using Drafts;
using static DraftsEditor.DraftsPropertyDrawer;
using Drafts.Extensions;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(ButtonAttribute), true)]
	public class ButoPropertyDrawer : PropertyDrawer {

		bool initialized;
		string name;
		Action invoke;
		Func<bool> enabled;
		parameter[] parameters = new parameter[0];
		float height;

		class parameter {
			public object value;
			Func<Rect, object, object> gui = null;

			public parameter(string label, Type type) {
				value = type.IsValueType ? Activator.CreateInstance(type) : null;
				if(type == typeof(int))
					gui = (r, v) => EditorGUI.IntField(r, label, (int)v);
				if(type == typeof(float))
					gui = (r, v) => EditorGUI.FloatField(r, label, (float)v);
				if(type == typeof(string))
					gui = (r, v) => EditorGUI.TextField(r, label, (string)v);
				if(type.IsSubclassOf(typeof(UnityEngine.Object)))
					gui = (r, v) => EditorGUI.ObjectField(r, label, (UnityEngine.Object)v, type, true);
				if(type.IsEnum)
					gui = (r, v) => EditorGUI.EnumPopup(r, label, (Enum)Enum.ToObject(type, v));

				if(gui == null) Debug.LogError($"type: {type.Name} not suported in button methods.");
			}
			public void Draw(Rect pos) => value = gui(pos, value);
		}

		void Init(SerializedProperty property, GUIContent label) {
			if(initialized) return;
			initialized = true;

			var methodName = (attribute as ButtonAttribute).methodName ?? property.name.Substring(1);
			var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
			var o = property.serializedObject.targetObject;
			var m = o.GetType().GetMethod(methodName, flags);

			if(m == null) { invoke = () => Debug.Log($"No Method {methodName} Found"); return; }

			// get parametes
			var p = m.GetParameters();
			parameters = new parameter[p.Length];
			for(int i = 0; i < p.Length; i++)
				parameters[i] = new parameter(p[i].Name, p[i].ParameterType);

			height = GetPropertyHeight(property, label) / (p.Length + 1);
			invoke = () => m.Invoke(o, parameters.Select(a => a.value).ToArray());

			var btnAtr = attribute as ButtonAttribute;
			name = btnAtr.text ?? property.name.Substring(1).SplitCamelCase();
			enabled = btnAtr.enabled;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			Init(property, label);
			position.height = height;

			//GUI.enabled = enabled();
			if(GUI.Button(NextY(ref position), name)) invoke();
			foreach(var p in parameters) p.Draw(NextY(ref position));
			//GUI.enabled = true;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> base.GetPropertyHeight(property, label) * (parameters.Length + 1) + 8;
	}
}