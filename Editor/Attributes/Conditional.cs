﻿using UnityEditor;
using UnityEngine;
using Drafts;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(ConditionalAttribute))]
	public class ConditionalAttributeEditor : DraftsPropertyDrawer {

		bool GetValue(SerializedProperty property) {
			var cond = attribute as ConditionalAttribute;
			var p = property.SiblingProperty(cond.field);
			if(p == null) {
				Debug.LogError($"field {cond.field} not found", property.serializedObject.targetObject);
				return false;
			}

			switch(p.propertyType) {
				case SerializedPropertyType.Boolean: return cond.Validate(p.boolValue);
				case SerializedPropertyType.Enum: return cond.Validate(p.enumValueIndex);
				case SerializedPropertyType.Integer: return cond.Validate(p.intValue);
				case SerializedPropertyType.Float: return cond.Validate(p.floatValue);
				case SerializedPropertyType.String: return cond.Validate(p.stringValue);
				case SerializedPropertyType.ObjectReference: return cond.Validate(p.objectReferenceValue);
				default: throw new System.Exception("Invalid Type:" + p.propertyType);
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> GetValue(property) ? Height(property) : 0;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			if(GetValue(property)) EditorGUI.PropertyField(position, property, label, true);
		}
	}
}

