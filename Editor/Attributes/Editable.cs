﻿using UnityEngine;
using UnityEditor;
using Drafts;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(EditableAttribute))]
	public class EditableAttributeEditor : DraftsPropertyDrawer {

		Dictionary<UnityEngine.Object, (Editor editor, Action validate)> editors = new Dictionary<UnityEngine.Object, (Editor editor, Action validate)>();

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

			EditorGUILayout.PropertyField(property);
			property.isExpanded = EditorGUI.Foldout(GUILayoutUtility.GetLastRect(), property.isExpanded, GUIContent.none, true);

			if(property.objectReferenceValue && property.isExpanded) {
				EditorGUI.indentLevel++;

				// cached editor
				if(!editors.TryGetValue(property.objectReferenceValue, out var match)) {
					Editor.CreateCachedEditor(property.objectReferenceValue, null, ref match.editor);

					// validation method
					var mName = (attribute as EditableAttribute).validationMethod;
					if(!string.IsNullOrEmpty(mName)) {
						//var method = property.ParentType(out var obj).GetMethod(mName, BindingFlags.InvokeMethod | ReflectionUtil.CommonFlags);
						var method = property.ParentType(out var obj).GetMethod(mName, ReflectionUtil.CommonFlags);
						if(method != null) match.validate = () => {
							method.Invoke(obj, null);
							EditorUtility.SetDirty(property.serializedObject.targetObject);
						};
					}
				}
				editors[property.objectReferenceValue] = match;

				EditorGUI.BeginChangeCheck();
				match.editor.OnInspectorGUI();
				if(EditorGUI.EndChangeCheck())
					match.validate?.Invoke();

				EditorGUI.indentLevel--;
				EditorGUILayout.Separator();
			}
		}
	}

}
