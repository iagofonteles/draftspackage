using Drafts;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(PropertyFieldAttribute))]
	public class PropertyFieldAttributeEditor : PropertyDrawer {
		
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginChangeCheck();
			EditorGUI.PropertyField(position, property, label);
			if(EditorGUI.EndChangeCheck()) {
				var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

				var type = property.ParentType(out var obj);
				var field = type.GetField(property.name, flags);

				var pname = (attribute as PropertyFieldAttribute).name
					?? property.name[0].ToString().ToUpper() + property.name.Substring(1);
				var prop = type.GetProperty(pname, flags);

				if(prop != null) prop.SetMethod.Invoke(obj, new[] { field.GetValue(obj) });
				else Debug.Log($"Property {pname} not found in {obj}");
				
				EditorUtility.SetDirty(property.serializedObject.targetObject);
			}
		}
	}
}
