#if ENABLE_INPUT_SYSTEM
using UnityEngine;
using UnityEditor;
using Drafts;
using UnityEngine.InputSystem;
using System.Linq;
using System;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(InputActionAttribute))]
	public class InputActionAttributeEditor : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			IInputActionCollection collection;

			var field = (attribute as InputActionAttribute).asset;
			if(field != null) collection = (InputActionAsset)property.serializedObject.FindProperty(field).objectReferenceValue;
			else collection = (IInputActionCollection)Activator.CreateInstance((attribute as InputActionAttribute).type);
			
			var names = collection.Select(a => a.actionMap.name + "/" + a.name).ToArray();
			var i = Array.IndexOf(names, property.stringValue);
			if(i < 0) i = 0;
			property.stringValue = names[EditorGUI.Popup(position, "Action", i, names)];
		}
	}
}
#endif
