using UnityEditor;
using Drafts;
using Drafts.Hidden;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine;
using Drafts.Patterns;

namespace DraftsEditor {
	[CustomEditor(typeof(PrefabPool))]
	public class PrefabPoolEditor : Editor {
		
		List<GameObject> Prefabs => (target as PrefabPool).prefabs;
		IEnumerable<Type> allTypes;
		List<Type> missingTypes = new List<Type>();

		private void OnEnable() {
			var assemblies = ReflectionUtil.GetAllAssembliesExcludingUnitys();
			allTypes = ReflectionUtil.FindAllDerivedTypes<IPrefabPool>(assemblies);

			Validate();
		}

		public override void OnInspectorGUI() {
			serializedObject.Update();

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("prefabs"));
			if(EditorGUI.EndChangeCheck()) Validate();

			EditorGUILayout.PropertyField(serializedObject.FindProperty("searchFolder"));
			serializedObject.ApplyModifiedProperties();

			Type tFind = null;
			foreach(var t in missingTypes) {
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField(t.Name);
				if(GUILayout.Button("Find")) tFind = t;
				EditorGUILayout.EndHorizontal();
			}
			if(tFind != null) FindType(tFind);
		}

		void FindType(Type type) {
			var folder = serializedObject.FindProperty("searchFolder").stringValue;
			EditorUtility.DisplayProgressBar("Searching", null, 1);
			var found = EditorUtil.FindPrefabsOfType(type, folder).FirstOrDefault();
			EditorUtility.ClearProgressBar();
			if(!found) throw new Exception($"No prefab of type {type.Name} found.");
			Prefabs.Add(found.gameObject);
			missingTypes.Remove(type);
			EditorUtility.SetDirty(this);
		}

		// check if all and only one prefab is assigned for each MonoPool and LazyMonoSingleton
		// doenst handle other classes for now
		void Validate() {
			RemoveListDuplicates(Prefabs); // remove duplicates
			Prefabs.RemoveAll(go => !go); // remove null

			missingTypes.Clear(); // check missing types
			foreach(var t in allTypes) if(Prefabs.All(go => !go || !go.GetComponent(t))) missingTypes.Add(t);
		}

		void RemoveListDuplicates<T>(List<T> list) {
			var set = new HashSet<T>();
			for(int i = list.Count-1; i >= 0; i--) {
				if(set.Contains(list[i]))
					list.RemoveAt(i);
				else set.Add(list[i]);
			}
		}
	}
}
