﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Drafts.yaSingleton.Internal;
using DraftsEditor.yaSingleton;

namespace DraftsEditor {
	[CustomEditor(typeof(SingletonUpdater), true)]
	public class SingletonUpdaterEditor2 : Editor {

		//private Dictionary<int, Editor> _editors;
		//private Dictionary<int, bool> _foldouts;
		private static GUIStyle BoldLabel = new GUIStyle("BoldLabel");
		SingletonUtil drawer;

		//public void OnEnable() {
		//	_editors = new Dictionary<int, Editor>();
		//	_foldouts = new Dictionary<int, bool>();
		//}

		public override void OnInspectorGUI() {
			EditorGUILayout.LabelField("Singletons", BoldLabel);
			if(drawer == null) drawer = new SingletonUtil();
			drawer.DrawAll();
		}
		//public override void OnInspectorGUI() {
		//	base.OnInspectorGUI();

		//	EditorGUILayout.LabelField("Singletons", BoldLabel);

		//	for(int i = 0; i < BaseSingleton.AllSingletons.Count; ++i) {

		//		if(!_foldouts.ContainsKey(i)) _foldouts.Add(i, false);

		//		var singleton = BaseSingleton.AllSingletons[i];
		//		_foldouts[i] = EditorGUILayout.InspectorTitlebar(_foldouts[i], singleton);

		//		if(_foldouts[i]) {
		//			if(!_editors.TryGetValue(i, out var editor))
		//				_editors.Add(i, null);

		//			CreateCachedEditor(singleton, null, ref editor);
		//			_editors[i] = editor;

		//			EditorGUI.indentLevel += 1;
		//			editor.OnInspectorGUI();
		//			EditorGUILayout.Space();

		//			if(AssetDatabase.Contains(singleton))
		//				EditorGUILayout.ObjectField("Reference", singleton, singleton.GetType(), false);
		//			else EditorGUILayout.HelpBox("Runtime generated", MessageType.Info);

		//			EditorGUI.indentLevel -= 1;
		//		}
		//	}
		//}

	}

}
