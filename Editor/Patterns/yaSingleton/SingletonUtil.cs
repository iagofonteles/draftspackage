﻿using Drafts.yaSingleton.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using System.IO;
using Drafts;

namespace DraftsEditor.yaSingleton {

	public class SingletonUtil {
		public static string defaultSingletonFolder = "Assets/Settings";

		[Serializable]
		public class SingleInfo {
			public Type type;
			public BaseSingleton instance;
			public Editor editor;
			public bool foldout;
		}
		public List<SingleInfo> Singletons { get; private set; }

		public SingletonUtil() {

			var all = ReflectionUtil.GetAllAssembliesExcludingUnitys();
			var types = ReflectionUtil.FindAllDerivedTypes<BaseSingleton>(all);

			Singletons = types.Select(t => {
				var i = (BaseSingleton)Resources.FindObjectsOfTypeAll(t).FirstOrDefault();
				if(i) i.AddToPreloadedAssets();
				return new SingleInfo() { type = t, instance = i };
			}).ToList();
			BaseSingleton.AllSingletons.Clear();
			BaseSingleton.AllSingletons.AddRange(Singletons.Where(s => s.instance).Select(s => s.instance).ToList());
		}

		public void FindInstances() {
			foreach(var s in Singletons)
				s.instance = (BaseSingleton)EditorUtil.FindAssetsAtPath2("", s.type).FirstOrDefault();
				//s.instance = (BaseSingleton)Resources.FindObjectsOfTypeAll(s.type).FirstOrDefault();
		}

		public void DrawAll() {
			defaultSingletonFolder = EditorGUILayout.TextField("Default Folder", defaultSingletonFolder);

			for(int i = 0; i < Singletons.Count; ++i) {
				var single = Singletons[i];

				EditorGUILayout.BeginHorizontal();
				// header foldout
				if(single.instance)
					single.foldout = EditorGUILayout.Foldout(single.foldout, single.type.Name, true);
				else EditorGUILayout.LabelField(single.type.Name);

				// create button
				if(single.instance) {
					EditorGUI.BeginDisabledGroup(true);
					EditorGUILayout.ObjectField(GUIContent.none, single.instance, single.type, allowSceneObjects: false);
					EditorGUI.EndDisabledGroup();
				} else
					if(!Application.isPlaying && GUILayout.Button("Create Instance"))
					single.instance = CreateSingletonSO(single.type, defaultSingletonFolder);

				EditorGUILayout.EndHorizontal();
				if(!single.foldout || !single.instance) continue;

				// editor
				EditorGUI.indentLevel++;
				Editor.CreateCachedEditor(single.instance, null, ref single.editor);
				single.editor.OnInspectorGUI();
				//EditorGUILayout.ObjectField("Reference", single.instance, single.type, false);
				EditorGUILayout.Space();
				EditorGUI.indentLevel--;
			}
		}

		BaseSingleton CreateSingletonSO(Type type, string path) {
			if(!Directory.Exists(path)) Directory.CreateDirectory(path);
			path = $"{path}/{type.Name}.asset";

			var o = Editor.CreateInstance(type);
			o.AddToPreloadedAssets();
			AssetDatabase.CreateAsset(o, path);
			AssetDatabase.Refresh();
			return (BaseSingleton)o;
		}

	}
}