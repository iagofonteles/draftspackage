﻿#if USE_AUTOTILE
using UnityEngine;
using UnityEditor;

#pragma warning disable IDE0051 // Remover membros privados não utilizados

class AutotileAutomationWindow : EditorWindow
{
    [MenuItem("Window/Autotile Automation")]
    public static void ShowWindow() => GetWindow<AutotileAutomationWindow>();

    string outputFolder = "Assets/Tilesets";
    Texture texture;

	void OnGUI() {

		outputFolder = EditorGUILayout.TextField("Output Folder", outputFolder);
        texture = EditorGUILayout.ObjectField("Texture", texture, typeof(Texture), false) as Texture;
        EditorGUILayout.LabelField("Input Path: " + (texture ? AssetDatabase.GetAssetPath(texture) : ""));
        if (GUILayout.Button("Create RuleTile")) CreateRuleTile(texture,
            outputFolder + "/new_rule_tile " + System.DateTime.Now.Ticks + ".asset");
    }

    static void CreateRuleTile(Texture texture, string outputPath) {
        var sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(texture));
        var tile = ScriptableObject.CreateInstance<RuleTile>();
        tile.m_DefaultSprite = sprites[0] as Sprite;

		RuleTile.TilingRule newRule(Object s, int[] i) =>
			new RuleTile.TilingRule() {
				m_ColliderType = UnityEngine.Tilemaps.Tile.ColliderType.Grid,
				m_Sprites = new[] { (Sprite)s },
				m_Neighbors = i
			};

		tile.m_TilingRules = new System.Collections.Generic.List<RuleTile.TilingRule>() {
            // corners
            newRule(sprites[16], new[] { 2, 1, 0,   1, 1,   0, 1, 0 }),
            newRule(sprites[17], new[] { 0, 1, 2,   1, 1,   0, 1, 0 }),
            newRule(sprites[18], new[] { 0, 1, 0,   1, 1,   2, 1, 0 }),
            newRule(sprites[19], new[] { 0, 1, 0,   1, 1,   0, 1, 2 }),

            // left side
            newRule(sprites[ 5], new[] { 0, 2, 0,   2, 1,   0, 1, 0 }),
            newRule(sprites[ 9], new[] { 0, 1, 0,   2, 1,   0, 1, 0 }),
            newRule(sprites[13], new[] { 0, 1, 0,   2, 1,   0, 2, 0 }),

            // middle colum
            newRule(sprites[ 6], new[] { 0, 2, 0,   1, 1,   0, 1, 0 }),
            newRule(sprites[10], new[] { 0, 1, 0,   1, 1,   0, 1, 0 }),
            newRule(sprites[14], new[] { 0, 1, 0,   1, 1,   0, 2, 0 }),

            // right side
            newRule(sprites[ 7], new[] { 0, 2, 0,   1, 2,   0, 1, 0 }),
            newRule(sprites[11], new[] { 0, 1, 0,   1, 2,   0, 1, 0 }),
            newRule(sprites[15], new[] { 0, 1, 0,   1, 2,   0, 2, 0 }),

            // horizontal strip
            newRule(sprites[1], new[] { 0, 2, 0,   2, 1,   0, 2, 0 }),
            newRule(sprites[2], new[] { 0, 2, 0,   1, 1,   0, 2, 0 }),
            newRule(sprites[3], new[] { 0, 2, 0,   1, 2,   0, 2, 0 }),

            // vertical strip
            newRule(sprites[ 4], new[] { 0, 2, 0,   2, 2,   0, 1, 0 }),
            newRule(sprites[ 8], new[] { 0, 1, 0,   2, 2,   0, 1, 0 }),
            newRule(sprites[12], new[] { 0, 1, 0,   2, 2,   0, 2, 0 }),
        };

        AssetDatabase.CreateAsset(tile, outputPath);
    }
}
#endif