﻿/*
using UnityEngine;
using MyEngine;
using UnityEditor;
using static MyEditor.Custom;
namespace MyEditor {

	[CustomEditor(typeof(InteractableObject))]
	class InteractableObjectEditor : Editor {

		public override void OnInspectorGUI() {
			serializedObject.Update();
			var target = this.target as InteractableObject;
			var i = serializedObject.FindProperty("instance");


			Edit(i, "name");
			var mode = Enum(ref target.position, "Position Mode");
			
			if(mode == InteractableObject.PositionMode.Other)
				Edit(i, "transform");
			if(mode == InteractableObject.PositionMode.Fixed)
				EditLabeled(i, "fixed_position", "Position");

			Edit(i, "range", "automatic");

			Edit(i, "icon");
			if((Sprite)i.FindPropertyRelative("icon").objectReferenceValue)
				Edit(i, "iconOffset");
			
			Edit(i, "action");

			serializedObject.ApplyModifiedProperties();
			if(GUI.changed) target.OnValidate();

			if(Application.isPlaying)
				if(GUILayout.Button("Enable Again"))
					Interactable.Add(target.instance);
		}
	}
}
*/