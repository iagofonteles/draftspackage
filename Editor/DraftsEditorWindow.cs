using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Drafts.Extensions;
using System;
using UObj = UnityEngine.Object;
using DraftsEditor.yaSingleton;
using Drafts.yaSingleton.Internal;
using System.Linq;
using System.Reflection;

namespace DraftsEditor {

	public class DraftsEditorWindow : EditorWindow {

		// singletons
		static bool expandSingletons = true;
		static SingletonUtil singletonDrawer;
		static Vector2 scrollPosition;

		[MenuItem("Drafts/Overview Window")]
		static void Init() {
			// Get existing open window or if none, make a new one:
			DraftsEditorWindow window = (DraftsEditorWindow)GetWindow(typeof(DraftsEditorWindow));
			window.Show();
		}

		void OnGUI() {
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
			GUILayout.Label("Drafts Settings", EditorStyles.boldLabel);
			DrawSingletons();
			DrawActions();
			EditorGUILayout.EndScrollView();
		}

		void DrawSingletons() {
			// header
			if(expandSingletons) EditorGUILayout.Separator();
			EditorGUILayout.BeginHorizontal();
			expandSingletons = EditorGUILayout.Foldout(expandSingletons, "Singletons", true);
			if(!expandSingletons) {
				EditorGUILayout.LabelField($"({BaseSingleton.AllSingletons.Count} used)");
				EditorGUILayout.EndHorizontal();
				return;
			}

			var find = GUILayout.Button("Find Singletons");
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;

			// find all
			if(singletonDrawer == null || find)
				singletonDrawer = new SingletonUtil();

			// draw
			singletonDrawer.DrawAll();

			EditorGUI.indentLevel--;
			if(expandSingletons) EditorGUILayout.Separator();

		}

		void DrawActions() {
			if(Selection.objects.Length == 0) return;

			EditorGUILayout.Space();
			GUILayout.Label("Object Actions:", EditorStyles.boldLabel);
			EditorGUI.indentLevel++;
			foreach(var t in typeActions)
				if(Selection.objects.All(o=> t.Evaluate(o))) t.DrawGUI();
			EditorGUI.indentLevel--;
		}

		static TypeAction[] typeActions = new TypeAction[] {
			new ta_DivideOnGrid(),
			new ta_CreateMaterials(),
			new ta_ChangeScriptIcon(),
		};

		public void SelectionRoundPosition() {
			foreach(var o in Selection.gameObjects)
				o.transform.localPosition = o.transform.localPosition.Rounded();
		}

		#region Actions

		abstract class TypeAction {
			protected abstract string Title { get; }
			protected abstract void Execute();
			protected abstract void Draw();

			protected void Parameters(params (string name, object value)[] param) {
				for(int i = 0; i < param.Length; i++)
					param[i].value = defaultEditor[param[i].value.GetType()](param[i].name, param[i].value);
			}

			public abstract bool Evaluate(UObj obj);
			protected bool HasComponent<T>(UObj obj) where T : Component => (obj as GameObject)?.GetComponent<T>() ?? false;

			public void DrawGUI() {
				if(GUILayout.Button(Title)) Execute();
				EditorGUI.indentLevel += 2;
				Draw();
				EditorGUI.indentLevel -= 2;
			}

			Dictionary<Type, Func<string, object, object>> defaultEditor = new Dictionary<Type, Func<string, object, object>>() {
				{ typeof(int), (n,v)=> EditorGUILayout.IntField(n,(int)v) },
				{ typeof(float), (n,v)=> EditorGUILayout.FloatField(n,(float)v) },
				{ typeof(string), (n,v)=> EditorGUILayout.TextField(n,(string)v) },
				{ typeof(bool), (n,v)=> EditorGUILayout.Toggle(n,(bool)v) },
			};
		}

		class ta_DivideOnGrid : TypeAction {
			int width, height, skip;

			protected override string Title => "Divide On Grid";
			public override bool Evaluate(UObj obj) => HasComponent<RectTransform>(obj);
			protected override void Draw() {
				EditorGUILayout.BeginHorizontal();
				width = EditorGUILayout.IntField("Colums", width);
				height = EditorGUILayout.IntField("Rows", height);
				skip = EditorGUILayout.IntField("Skip", skip);
				EditorGUILayout.EndHorizontal();
			}

			protected override void Execute() {
				var t = Selection.activeGameObject.GetComponentsImmediate<RectTransform>();
				var h = height == 0 ? ((t.Length - 1) / width) + 1 : height;
				Selection.activeTransform.SortChildrenInGrid(width, h, skip);
			}
		}

		class ta_CreateMaterials : TypeAction {
			protected override string Title => "Create Material";

			string textureName = "_MainTex";
			string prefix = "mat_", suffix = "";
			Material baseMaterial;

			public override bool Evaluate(UObj obj) => obj is Texture || obj is Texture2D;
			protected override void Draw() {
				EditorGUILayout.BeginHorizontal();
				textureName = EditorGUILayout.TextField("Texture", textureName);
				prefix = EditorGUILayout.TextField("Prefix", prefix);
				suffix = EditorGUILayout.TextField("Sufifx", suffix);
				EditorGUILayout.EndHorizontal();
				baseMaterial = (Material)EditorGUILayout.ObjectField("Base Material", baseMaterial, typeof(Material), false);
			}

			protected override void Execute() {
				if(!baseMaterial) return;
				foreach(var o in Selection.objects) {
					var mat = new Material(baseMaterial);
					mat.SetTexture(textureName, o as Texture2D);
					EditorUtil.CreateOrReplaceFile(o, prefix + o.name + suffix + ".asset", mat, false);
				}
				AssetDatabase.Refresh();
			}
		}

		class ta_ChangeScriptIcon : TypeAction {
			protected override string Title => "Change Script Icon";

			static MethodInfo SetIconForObject = typeof(EditorGUIUtility).GetMethod("SetIconForObject", BindingFlags.Static | BindingFlags.NonPublic);
			static MethodInfo CopyMonoScriptIconToImporters = typeof(MonoImporter).GetMethod("CopyMonoScriptIconToImporters", BindingFlags.Static | BindingFlags.NonPublic);

			Texture2D icon;

			public override bool Evaluate(UObj obj) => obj is MonoScript;

			protected override void Draw() => icon = icon.Edit();

			protected override void Execute() {
				foreach(var scr in Selection.objects) {
					SetIconForObject.Invoke(null, new object[] { scr, icon });
					CopyMonoScriptIconToImporters.Invoke(null, new object[] { scr });
				}
				AssetDatabase.Refresh();
			}
		}

		#endregion
	}
}