#if DRAFTS_USE_FMOD
using UnityEngine;
using UnityEditor;
using Drafts.FMOD;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(FMODEvent))]
	[CustomPropertyDrawer(typeof(FMODSound))]
	public class FMODSoundEditor : DraftsPropertyDrawer {

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> Height(property.FindPropertyRelative("sound"));

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.PropertyField(position, property.FindPropertyRelative("sound"),
				new GUIContent(property.displayName), true);
		}
	}
}
#endif