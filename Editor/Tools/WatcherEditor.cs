using UnityEngine;
using UnityEditor;
using Drafts.Hidden;

namespace DraftsEditor {

	[CustomPropertyDrawer(typeof(ValueWatcher), true)]
	public class ValueWatcherEditor : DraftsPropertyDrawer {

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> base.GetPropertyHeight(property, label) + (!property.isExpanded ? 0
			: Height(property.FindPropertyRelative("OnChanged")));

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			position.height = base.GetPropertyHeight(property, label);
			
			// foldout
			var foldoutPos = new Rect(position.position, new Vector2(position.width * .4f, position.height));
			property.isExpanded = EditorGUI.Foldout(foldoutPos, property.isExpanded, (string)null, true);
			
			// value
			EditorGUI.PropertyField(position, property.FindPropertyRelative("_value"), new GUIContent(property.displayName), false);
			
			// on changed
			if(property.isExpanded) {
				position.y += position.height;
				var _changed = property.FindPropertyRelative("OnChanged");
				position.height = Height(_changed);
				EditorGUI.PropertyField(position, _changed, true);
			}
		}
	}

	public class DeltaWatcherEditor : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
			=> EditorGUI.PropertyField(position, property.FindPropertyRelative("_value"),
				new GUIContent(property.displayName), false);
	}

}
