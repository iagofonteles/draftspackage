﻿using UnityEngine;
using UnityEditor;

//[CustomPropertyDrawer(typeof(ScriptableObject), true)]
public class ScriptableObjectDrawer : PropertyDrawer {
    // Cached scriptable object editor
    private Editor editor = null;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        if(property.objectReferenceValue == null)
            // Draw label
            EditorGUI.PropertyField(position, property, label, true);
        else {
            var p = position;
            p.x += EditorGUIUtility.labelWidth;
            p.width -= EditorGUIUtility.labelWidth;
            EditorGUI.PropertyField(p, property, GUIContent.none, true);

            p = position;
            p.width = EditorGUIUtility.labelWidth;
            property.isExpanded = EditorGUI.Foldout(p, property.isExpanded, label, true);

            // Draw foldout properties
            if(property.isExpanded) {
                EditorGUI.indentLevel++;

                // Draw object properties
                if(!editor) Editor.CreateCachedEditor(property.objectReferenceValue, null, ref editor);
                editor.OnInspectorGUI();

                EditorGUI.indentLevel--;
            }
        }
    }
}