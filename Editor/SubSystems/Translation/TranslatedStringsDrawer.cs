using Drafts;
using Drafts.Translations;
using System;
using UnityEditor;
using UnityEngine;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(TranslatedStrings))]
	public class TranslatedStringsDrawer : DraftsPropertyDrawer {

		//public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		//	=> base.GetPropertyHeight(property, label) * (property.isExpanded && !property.FindPropertyRelative("lockKey").boolValue ? 3 : 1);

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			var obj = property.GetObject<TranslatedStrings>();

			void Header(Rect pos) {
				property.isExpanded = EditorGUI.Foldout(NextX2(ref pos, .3f), property.isExpanded, label, true);

				if(obj.Reflect<bool>("IsValid")) return;
				if(GUI.Button(pos, $"Create ALl KeySets and Keys")) obj.Reflect<object>("CreateAllKeySetsAndKeys");
			}

			if(!property.isExpanded) {
				Header(position);
				return;
			}
			EditorGUI.PropertyField(position, property, label, true);
		}

	}
}
