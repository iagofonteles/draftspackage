﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Drafts.Translations;
using System.IO;
using System.Linq;

namespace DraftsEditor {
	[CustomEditor(typeof(TranslationKeySet))]
	class TranslationKeySetEditor : Editor {
		static bool fileEditMode = true;
		static TranslationKeySet scriptable;
		static string filter = "";
		static List<int> idSort = new List<int>();
		static TextAsset file;
		static bool sort = true;
		string newKey = "";

		void OnEnable() {
			scriptable = target as TranslationKeySet;
			idSort = scriptable.Filter(filter, false);
		}

		public override void OnInspectorGUI() {
			scriptable.alias = EditorGUILayout.TextField("Alias", scriptable.alias);
			if(GUILayout.Button(fileEditMode ? "Key Edit Mode" : "File Edit Mode"))
				fileEditMode = !fileEditMode;

			if(fileEditMode) {
				// auto load file
				//EditorGUI.BeginChangeCheck();
				file = (TextAsset)EditorGUILayout.ObjectField("File", file, typeof(TextAsset), false);
				//if(EditorGUI.EndChangeCheck() && file) scriptable.LoadTexts(file.text);

				EditorGUILayout.BeginHorizontal();
				EditorGUI.BeginDisabledGroup(!file);
				if(GUILayout.Button("Save")) SaveFile();
				if(GUILayout.Button("Load")) scriptable.LoadTexts(file.text);
				if(GUILayout.Button("Load Keys")) LoadKeys();
				EditorGUI.EndDisabledGroup();
				if(GUILayout.Button("Create Empty File")) CreateEmptyFile();
				EditorGUILayout.EndHorizontal();
			}

			// filter
			EditorGUILayout.Space(20);
			EditorGUILayout.BeginHorizontal();
			filter = EditorGUILayout.TextField("", filter);
			if(GUILayout.Button("Filter Keys")) idSort = scriptable.Filter(filter, sort);
			if(GUILayout.Button($"Sort: {sort}", GUILayout.MaxWidth(80))) sort = !sort;
			EditorGUILayout.EndHorizontal();

			if(fileEditMode) { // text editing only
				var sort = idSort;
				var lines = scriptable.ToArray();

				EditorGUIUtility.labelWidth = EditorGUIUtility.currentViewWidth / 3;
				for(int j = 0; j < sort.Count; j++) {
					var label = new GUIContent(lines[sort[j]].alias);
					lines[sort[j]].text = EditorGUILayout.TextField(label, lines[sort[j]].text);
				}
			} else { // key editing only
				EditorGUI.indentLevel++;
				EditorGUILayout.BeginHorizontal();
				newKey = EditorGUILayout.TextField("", newKey);
				if(GUILayout.Button("Add Key")) AddKey(newKey);
				EditorGUILayout.EndHorizontal();

				for(int i = 0; i < idSort.Count; i++)
					scriptable[idSort[i]].alias
						= EditorGUILayout.TextField(scriptable[idSort[i]].alias);
				EditorGUI.indentLevel--;
			}
			serializedObject.ApplyModifiedProperties();
		}

		void SaveFile() {
			var path = AssetDatabase.GetAssetPath(file);
			File.WriteAllText(path, scriptable.GenerateFile());
			AssetDatabase.Refresh();
			Debug.Log("File Saved", file);
		}
		void LoadKeys() { scriptable.LoadKeys(file.text); idSort = scriptable.Filter("", sort); }
		void CreateEmptyFile() {
			var path = AssetDatabase.GetAssetPath(scriptable);
			Debug.Log(path);
			path = Path.GetDirectoryName(path) + $"/{scriptable.alias}_lang.txt";
			Debug.Log(path);
			File.Create(path);
			File.WriteAllText(path, scriptable.GenerateEmptyKeys());
			AssetDatabase.Refresh();
		}
		void AddKey(string key) {
			if(scriptable.AddKey(key)) {
				idSort.Add(scriptable.Count - 1);
				//newKey = "";
			} else
				Debug.LogError("Key already exists");
		}
	}
}