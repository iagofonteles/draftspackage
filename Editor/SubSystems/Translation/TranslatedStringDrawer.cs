using Drafts;
using Drafts.Translations;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DraftsEditor {
	[CustomPropertyDrawer(typeof(TranslatedString))]
	public class TranslatedStringDrawer : DraftsPropertyDrawer {

		//public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		//	=> base.GetPropertyHeight(property, label) * (property.isExpanded && !property.FindPropertyRelative("lockKey").boolValue ? 3 : 1);

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

			var _keyWithSet = property.FindPropertyRelative("keyWithSet");
			var _set = property.FindPropertyRelative("set");
			var _key = property.FindPropertyRelative("key");
			var _keySet = property.FindPropertyRelative("keySet");
			var _keyId = property.FindPropertyRelative("keyId");
			var _def = property.FindPropertyRelative("defaultText");
			var _lockKey = property.FindPropertyRelative("lockKey");

			var allKeys = Translation.GetAllKeysWithSet();//.Where(s => s.Contains(_keyWithSet.stringValue)).ToArray();
			var id = Array.IndexOf(allKeys, _keyWithSet.stringValue);

			EditorGUI.LabelField(NextX2(ref position, .3f), label, GUIContent.none); // name

			EditorGUI.BeginDisabledGroup(_lockKey.boolValue);

			// text field
			EditorGUI.BeginChangeCheck();
			EditorGUI.PropertyField(NextX2(ref position, .6f), _keyWithSet, GUIContent.none);
			if(EditorGUI.EndChangeCheck()) Validate(_keyWithSet, _set, _key, _keySet, _keyId);

			// dropdown
			EditorGUI.BeginChangeCheck();
			id = EditorGUI.Popup(NextX(ref position, 18), id, allKeys);
			if(EditorGUI.EndChangeCheck() && id >= 0 && id < allKeys.Length) {
				_keyWithSet.stringValue = allKeys[id];
				Validate(_keyWithSet, _set, _key, _keySet, _keyId);
			}

			EditorGUI.EndDisabledGroup();

			if(_set.stringValue == "" || _key.stringValue == "") return;

			if(!_keySet.objectReferenceValue) { // invalid set
				//if(GUI.Button(position, $"Create KeySet: {_set.stringValue}/{_key.stringValue}")) {
				if(GUI.Button(position, $"+Create")) {
					_keySet.objectReferenceValue = TranslationEditor.CreateKeySet(_set.stringValue, _key.stringValue, _def.stringValue);
					Validate(_keyWithSet, _set, _key, _keySet, _keyId);
				}
			} else {
				if(_keyId.intValue < 0) { // invalid key
					//if(GUI.Button(pos, $"+Add Key: {_set.stringValue}/{_key.stringValue}")) {
					if(GUI.Button(position, $"+Add")) {
						((TranslationKeySet)_keySet.objectReferenceValue).AddKey(_key.stringValue, _def.stringValue);
						Validate(_keyWithSet, _set, _key, _keySet, _keyId);
					}
				} else { // all valid
					//EditorGUI.LabelField(NextX2(ref pos, .8f), $"{_set.stringValue}/{_key.stringValue}", GUIStyle.none);
					EditorGUI.PropertyField(position, _keySet, GUIContent.none);
					//EditorGUI.PropertyField(pos, _keyId, GUIContent.none);
				}
			}


		}

		static bool Validate(SerializedProperty keyWithSet, SerializedProperty set, SerializedProperty key, SerializedProperty keySet, SerializedProperty keyId) {
			if(!Translation.Instance) throw new Exception("Translation system not found.");

			try {
				set.stringValue = keyWithSet.stringValue.Split('/')[0];
				key.stringValue = keyWithSet.stringValue.Substring(set.stringValue.Length + 1);
			} catch {
				set.stringValue = "";
				key.stringValue = "";
			}

			keySet.objectReferenceValue = Translation.Instance.GetKeySet(set.stringValue);
			keyId.intValue = keySet.objectReferenceValue ? ((TranslationKeySet)keySet.objectReferenceValue).GetKeyId(key.stringValue) : -2;
			return keySet.objectReferenceValue && keyId.intValue >= 0;
		}

	}
}
