﻿using Drafts;
using Drafts.Translations;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DraftsEditor {

	[CustomEditor(typeof(Translation))]
	class TranslationEditor : Editor {

		public override void OnInspectorGUI() {
			serializedObject.Update();
			base.OnInspectorGUI();

			if(GUILayout.Button("Create Key Set"))
				throw new System.NotImplementedException();

			serializedObject.ApplyModifiedProperties();
		}

		[MenuItem("Drafts/Translation: Validate Scene & Project")]
		internal static void ValidateKeysIdInScene() {
			bool Validate(Object comp) {
				var strings = comp.GetType().GetFields(ReflectionUtil.CommonFlags)
					.Where(f => typeof(IValidateTranslation).IsAssignableFrom(f.FieldType))
					.Select(f => (IValidateTranslation)f.GetValue(comp));
				return strings.Count(s => !s.Validate()) == 0;
			}

			var all = Resources.FindObjectsOfTypeAll<Object>().Where(o => o is ITranslatedComponent);
			foreach(var t in all) if(!Validate(t)) Debug.LogError($"Invalid Translation on {t.name}", t);
		}

		public static TranslationKeySet CreateKeySet(string alias, string key, string text) {
			var set = Translation.Instance.CreateKeySet(alias);
			set.AddKey(key, text);
			EditorUtil.CreateFileOnly(Translation.Instance, alias + ".asset", set, true);
			ValidateKeysIdInScene();
			return set;
		}
	}
}