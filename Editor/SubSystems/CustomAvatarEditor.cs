﻿using UnityEngine;
using UnityEditor;
using Drafts.Avatar;

namespace DraftsEditor {
	[CustomEditor(typeof(CustomAvatar)), CanEditMultipleObjects]
	public class CustomAvatarEditor : Editor {

		SerializedProperty _parts;
		SerializedProperty _preset;

		int partsLength;

		bool anyNull, anyOk;

		private void OnEnable() {
			_parts = serializedObject.FindProperty("parts");
			_preset = serializedObject.FindProperty("preset");
			partsLength = (target as CustomAvatar).preset.Length;

			// check if any parts are missing
			anyNull = false;
			anyOk = false;
			for(int i = 0; i < _parts.arraySize; i++) {
				if(!_parts.GetArrayElementAtIndex(i).objectReferenceValue) anyNull = true;
				if(_parts.GetArrayElementAtIndex(i).objectReferenceValue) anyOk = true;
			}

		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if(targets.Length > 1) return;

			if((anyNull || _parts.arraySize != partsLength)
			&& GUILayout.Button($"{(anyOk ? "Fix" : "Create")} Avatar"))
				FixAvatar();
		}

		void FixAvatar() {
			for(int i = 0; i < _parts.arraySize; i++) {

			}
		}
	}
}