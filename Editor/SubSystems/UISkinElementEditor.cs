﻿using UnityEditor;
using Drafts.CustomUISkin;
using Drafts.Extensions;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

#if DRAFTS_USE_TMPRO
using TMPro;
#endif

namespace DraftsEditor {

	[CustomEditor(typeof(UISkinElement), true), CanEditMultipleObjects]
	class UISkinElementEditor : Editor {

		string[] elements;
		int id;
		SerializedProperty _element;
		SerializedProperty _preset;

		private void OnEnable() {
			_element = serializedObject.FindProperty("element");
			_preset = serializedObject.FindProperty("preset");

			elements = null;
			var go = (target as UISkinElement).gameObject;
			var comp = go.GetComponent(typeof(Image))
#if DRAFTS_USE_TMPRO
				?? go.GetComponent(typeof(TextMeshProUGUI));
#else
				?? go.GetComponent(typeof(Text));
#endif
			if(!comp || targets.Any(t => !(t as UISkinElement).gameObject.GetComponent(comp.GetType()))) return;
			elements = comp.GetType() == typeof(Image) ? UISkin.GetImageElementsNames() : UISkin.GetTextElementsNames();
			id = Array.IndexOf(elements, _element.stringValue);
		}

		public override void OnInspectorGUI() {

			if(elements == null) {
				EditorGUILayout.HelpBox("Diferent element types selected or Image/Text component missing.", MessageType.Warning, true);
				return;
			}

			serializedObject.Update();
			EditorGUI.BeginChangeCheck();
			id = EditorGUILayout.Popup("Element", id, elements);
			if(EditorGUI.EndChangeCheck()) {
				_element.stringValue = elements[id];
				serializedObject.ApplyModifiedProperties();
				foreach(var t in targets) (t as UISkinElement).ChangeElement();
			}
			EditorGUILayout.PropertyField(_preset, new GUIContent("Preset"));
			serializedObject.ApplyModifiedProperties();

			//if(GUILayout.Button("all"))
			//	FindObjectsOfType<UISkinElement>().ForEach(e => e.ChangeElement());
		}

		[MenuItem("Drafts/UISkin: Validate Scene")]
		public static void ValidateScene() {
			foreach(var e in Resources.FindObjectsOfTypeAll<UISkinElement>())
				e.GetPreset(true, true);
		}
	}
}
