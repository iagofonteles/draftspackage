using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Drafts/Systems/Stat Generator")]
public class StatsGenerator : ScriptableObject {
	public string @namespace = "Drafts.Battle";
	public string className;
	public List<StatInfo> stats = new List<StatInfo>();
	[Serializable]
	public class StatInfo {
		public int sortOrder;
		public string name;
		public string DisplayName;
		public bool roundToInt = false;
	}
}
