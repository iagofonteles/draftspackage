﻿using UnityEngine;
using UnityEditor;
using System;
using Drafts.Extensions;
using System.Linq;
using UObj = UnityEngine.Object;
using Drafts;
using System.Reflection;
using System.Collections;

public static class EditorExtensions {
	public static bool ChildSelected(this Transform t) => Selection.activeTransform != null && (Selection.activeTransform == t || Selection.activeTransform.parent == t);
}

namespace DraftsEditor {

	public static class EditorExtension {

		public static T Edit<T>(this T obj, bool allowSceneObjects = false) where T : UObj
			=> (T)EditorGUILayout.ObjectField(obj, typeof(T), allowSceneObjects);

		public static Type GetRootType(this SerializedProperty property, out UObj obj) {
			obj = property.GetEndProperty().serializedObject.targetObject;
			return obj.GetType();
		}

		[Obsolete("does not work with arrays")]
		public static SerializedProperty SiblingProperty(this SerializedProperty property, string name) {
			var path = property.propertyPath;
			path = path.Remove(path.LastIndexOf(property.name)); // remove self
			if(path != "") path += ".";
			return property.serializedObject.FindProperty(path + name);
		}


		public static T GetObject<T>(this SerializedProperty property) => (T)GetObject(property);
		public static T GetParentObject<T>(this SerializedProperty property) => (T)GetParentObject(property);

		public static object GetObject(this SerializedProperty property) {
			object obj = property.serializedObject.targetObject;
			var path = property.propertyPath.Replace(".Array.data[", "[").Split('.');

			foreach(var p in path.Take(path.Length))
				obj = GetValue(obj, p);

			return obj;
		}

		public static object GetParentObject(this SerializedProperty property) {
			object obj = property.serializedObject.targetObject;
			var path = property.propertyPath.Replace(".Array.data[", "[").Split('.');
			if(path.Length<2) Debug.Log(path.Length);

			foreach(var p in path.Take(path.Length - 1)) {
				obj = GetValue(obj, p);
				if(path.Length < 2)  Debug.Log("cagous");
			}
			if(path.Length < 2) Debug.Log(obj);

			return obj;
		}

		static object GetValue(object source, string path) {
			if(path.Contains("[")) {
				var member = path.Substring(0, path.IndexOf("["));
				var index = Convert.ToInt32(path.Substring(path.IndexOf("[") + 1).Replace("]", ""));
				return _GetValue(source, member, index);
			}
			return _GetValue(source, path);
		}

		static object _GetValue(object source, string member) {
			try {
				return source.GetType().GetMember(member, ReflectionUtil.CommonFlags)[0].GetValue(source);
			} catch {
				throw new Exception($"member {member} not found in {source.GetType().Name}.");
			}
		}

		static object _GetValue(object source, string member, int index) {
			var enm = (_GetValue(source, member) as IEnumerable).GetEnumerator();
			while(index-- >= 0) enm.MoveNext();
			return enm.Current;
		}

		public static Type ParentType(this SerializedProperty property, out object obj) {
			var parent = GetRootType(property, out var obj2);
			obj = obj2;

			string[] path = property.propertyPath.Split('.');
			path = path.Take(path.Length - 1).ToArray();

			foreach(var s in path) {
				var m = parent.GetMember(s)[0];
				obj = m.GetValue(obj);
				parent = parent.GetMember(s)[0].ReturnType();
			}

			return parent;
		}
	}

	public class DraftsPropertyDrawer : PropertyDrawer {
		protected SerializedProperty property;

		public static T GetTarget<T>(PropertyDrawer drawer, SerializedProperty property) where T : class
			=> drawer.fieldInfo.GetValue(property.serializedObject.targetObject) as T;

		public static class Style {
			public static GUIStyle Center = new GUIStyle(EditorStyles.numberField) { alignment = TextAnchor.MiddleCenter };
		}

		public static Rect NextX(ref Rect r, float width) {
			var ret = r;
			ret.width = width;
			r.x += width;
			r.width -= width;
			return ret;
		}
		public static Rect NextX(ref Rect r) { var ret = r; r.x += r.width; return ret; }

		public static Rect NextX2(ref Rect r, float currentWidthMultiplier) {
			var ret = r;
			ret.width = ret.width * currentWidthMultiplier;
			r.x += ret.width;
			r.width -= ret.width;
			return ret;
		}

		public static Rect NextY(ref Rect r, float height) {
			var ret = r;
			ret.height = height;
			r.y += height;
			r.height -= height;
			return ret;
		}
		public static Rect NextY(ref Rect r) { var ret = r; r.y += r.height; return ret; }

		public static void PropertyArray(Rect rect, SerializedProperty property, int index, GUIContent content = null)
			=> EditorGUI.PropertyField(rect, property.GetArrayElementAtIndex(index), content ?? GUIContent.none);

		public static void ArrayLabel(Rect rect, SerializedProperty prop, int index, Func<SerializedProperty, object> getValue)
			=> EditorGUI.LabelField(rect, getValue(prop.GetArrayElementAtIndex(index)).ToString());
		public static void LabelArray(Rect rect, SerializedProperty prop, int index, Func<SerializedProperty, object> getValue, GUIStyle style)
			=> EditorGUI.LabelField(rect, getValue(prop.GetArrayElementAtIndex(index)).ToString(), style);

		public static float Height(SerializedProperty property) => EditorGUI.GetPropertyHeight(property);

		public static void DrawProperties(ref Rect rect, SerializedProperty prop, params string[] properties) {
			var heightLeft = rect.height;
			foreach(var name in properties) {
				var p = prop.FindPropertyRelative(name);
				rect.height = Height(p);
				heightLeft -= rect.height;
				EditorGUI.PropertyField(NextY(ref rect), p, true);
			}
			rect.height = heightLeft;
		}
	}

	public class DraftsPropertyDrawer<T> : DraftsPropertyDrawer where T : class {
		protected T Target { get; private set; }
		protected void GetTarget(SerializedProperty property) => Target = GetTarget<T>(this, property);
	}

	public static class Menus {

		[MenuItem("Drafts/Update Drafts Package")]
		public static void UpdateDraftsPackage() {
			UnityEditor.PackageManager.Client.Add("https://bitbucket.org/iagofonteles/draftspackage.git");
		}

		[MenuItem("Drafts/Misc/All World Canvas To Main Camera")]
		public static void AllWorldCanvasToMainCamera() {
			foreach(var c in GameObject.FindObjectsOfType<Canvas>())
				c.worldCamera = Camera.main;
		}

		[MenuItem("Drafts/Misc/Transform Round Local Position")]
		public static void SelectionRoundPosition() {
			foreach(var o in Selection.gameObjects)
				o.transform.localPosition = o.transform.localPosition.Rounded();
		}

		[MenuItem("Drafts/Misc/Create Scriptable Instance")]
		public static void CreateScriptableInstance() {
			if(Application.isPlaying) return;

			foreach(var o in Selection.objects) {
				try {
					var asset = ScriptableObject.CreateInstance(o.name);
					asset.name = o.name;
					var path = AssetDatabase.GetAssetPath(o.GetInstanceID());
					AssetDatabase.CreateAsset(asset, path.Remove(path.Length - 3, 3) + ".asset");
					Selection.activeObject = asset;
				} catch { }
			}
		}
	}
}
